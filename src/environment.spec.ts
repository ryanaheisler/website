import * as local from './environments/environment';
import * as prod from './environments/environment.prod';
describe('environments', () => {
  describe('local', () => {
    it('should have correct values', () => {
      expect(local.environment.production).toBeFalsy();
      expect(local.environment.backendHost).toEqual('localhost');
      expect(local.environment.backendPort).toEqual(3000);
      expect(local.environment.debug).toBe(true);
    });
  });

  describe('prod', () => {
    it('should have correct values', () => {
      expect(prod.environment.production).toBeTruthy();
      expect(prod.environment.backendHost).toEqual('server.ryanheisler.com');
      expect(prod.environment.backendPort).toEqual(80);
      expect(prod.environment.debug).toBe(false);
    });
  });
});