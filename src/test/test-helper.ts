import { Scorecard } from 'src/app/heisldice/models/scorecard.model';
import { DEFAULT_PLAYER, Player } from './../app/heisldice/models/player.model';
export class TestHelper {
    static buildPlayer(fromFields: {
        name?: string,
        rollsRemaining?: number,
        turnsRemaining?: number,
        previousDice?: number[],
        myTurn?: boolean,
        scorecard?: Scorecard,
        diceColor?: string
    }): Player {
        const defaultPlayer = DEFAULT_PLAYER();
        return new Player(
            fromFields.name || defaultPlayer.name,
            fromFields.rollsRemaining || defaultPlayer.rollsRemaining,
            fromFields.turnsRemaining || defaultPlayer.turnsRemaining,
            fromFields.previousDice || defaultPlayer.previousDice,
            fromFields.myTurn || defaultPlayer.myTurn,
            fromFields.scorecard || defaultPlayer.scorecard,
            fromFields.diceColor || defaultPlayer.diceColor
        )
    }

    static constructPlayer(
        name?: string,
        rollsRemaining?: number,
        turnsRemaining?: number,
        previousDice?: number[],
        myTurn?: boolean,
        scorecard?: Scorecard,
        diceColor?: string
    ): Player {
        const defaultPlayer = DEFAULT_PLAYER();
        return new Player(
            name || defaultPlayer.name,
            rollsRemaining || defaultPlayer.rollsRemaining,
            turnsRemaining || defaultPlayer.turnsRemaining,
            previousDice || defaultPlayer.previousDice,
            myTurn || defaultPlayer.myTurn,
            scorecard || defaultPlayer.scorecard,
            diceColor || defaultPlayer.diceColor
        );
    }

    static assertAlmostEqual(expected: number, actual: number, margin: number, message?: string): void {
        let errorMessage = `expected ${actual} to be almost equal to ${expected}, ~${margin}`;
        if (message) errorMessage = `${errorMessage}: ${message}`;

        if (actual > expected + margin) throw new Error(errorMessage);
        if (actual < expected - margin) throw new Error(errorMessage);
    }

    static expectNothing() {
        expect(true).toBe(true);
    }
}