import { MainPhotoComponent } from './main-photo/main-photo.component';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        MainPhotoComponent
      ],
    }).compileComponents();
  }));

  it('should have only router outlet', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();

    const routerOutlet = fixture.debugElement.nativeElement.querySelector('router-outlet');

    expect(routerOutlet).toBeTruthy();

  });
});
