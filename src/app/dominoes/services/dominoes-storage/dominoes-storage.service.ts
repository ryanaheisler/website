import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { SessionStorageWrapperService } from 'src/app/services/session-storage-wrapper/session-storage-wrapper.service';

@Injectable({
  providedIn: 'root'
})
export class DominoesStorageService {

  private LOBBY_ID_KEY: string = "dominoes-lobby-id";
  private PLAYER_NAME_KEY: string = "dominoes-player-name";
  
  private _lobbyIDSubject: Subject<string>;
  private get lobbyIDSubject(): Subject<string> {
    if (!this._lobbyIDSubject) this._lobbyIDSubject = new ReplaySubject();
    return this._lobbyIDSubject;
  };

  constructor(private sessionStorageWrapperService: SessionStorageWrapperService) { }

  // Lobby ID
  public storeCurrentLobbyID(lobbyID: string) {
    this.sessionStorageWrapperService.setItem(this.LOBBY_ID_KEY, lobbyID);
    this.lobbyIDSubject.next(lobbyID);
  }

  public subscribeToCurrentLobbyID(): Observable<string> {
    const currentLobbyID = this.sessionStorageWrapperService.getItem(this.LOBBY_ID_KEY);
    this.lobbyIDSubject.next(currentLobbyID);
    return this.lobbyIDSubject;
  }

  public unsetCurrentLobbyID(): void {
    this.sessionStorageWrapperService.removeItem(this.LOBBY_ID_KEY);
    this.lobbyIDSubject.next(undefined);
  }

  // Player Name
  public storeCurrentPlayerName(playerName: string) {
    this.sessionStorageWrapperService.setItem(this.PLAYER_NAME_KEY, playerName);
  }
  public getCurrentPlayerName(): string {
    return this.sessionStorageWrapperService.getItem(this.PLAYER_NAME_KEY);
  }
  public unsetCurrentPlayerName(): void {
    this.sessionStorageWrapperService.removeItem(this.PLAYER_NAME_KEY);
  }
}
