import { TestBed } from '@angular/core/testing';
import { SessionStorageWrapperService } from 'src/app/services/session-storage-wrapper/session-storage-wrapper.service';
import { TestHelper } from 'src/test/test-helper';
import * as td from 'testdouble';

import { DominoesStorageService } from './dominoes-storage.service';

describe('DominoesStorageService', () => {
  let service: DominoesStorageService;
  let mockSessionStorageWrapper: SessionStorageWrapperService;
  const playerName = "Jurmy";
  const lobbyID = "9EF1";

  beforeEach(() => {
    mockSessionStorageWrapper = td.object(SessionStorageWrapperService.prototype);

    TestBed.configureTestingModule({
      providers: [
        { provide: SessionStorageWrapperService, useValue: mockSessionStorageWrapper },
      ]
    });

    service = TestBed.get(DominoesStorageService);
  });

  describe('lobby ID', () => {
    it('should store lobby ID in Session Storage', () => {
      service.storeCurrentLobbyID("anything");
      td.verify(mockSessionStorageWrapper.setItem('dominoes-lobby-id', "anything"));
      TestHelper.expectNothing();
    });
    it('should get the lobby ID in session storage and send to subscribers', () => {
      td.when(mockSessionStorageWrapper.getItem("dominoes-lobby-id")).thenReturn(lobbyID);

      let nextLobbyID: string;

      service.subscribeToCurrentLobbyID().subscribe(value => nextLobbyID = value);
      expect(nextLobbyID).toEqual(lobbyID);

      const newLobbyID = "7772";
      service.storeCurrentLobbyID(newLobbyID);

      expect(nextLobbyID).toEqual(newLobbyID);
    });
    it('should clear the current lobby ID and send undefined to subscribers', () => {
      let nextLobbyID: string = "not undefined";
      td.when(mockSessionStorageWrapper.getItem('dominoes-lobby-id')).thenReturn(lobbyID);
      service.subscribeToCurrentLobbyID().subscribe(value => nextLobbyID = value);

      service.unsetCurrentLobbyID();
      td.verify(mockSessionStorageWrapper.removeItem('dominoes-lobby-id'));
      expect(nextLobbyID).toBeUndefined();
    });
  });
  describe('player name', () => {
    it('should store player name in Session Storage', () => {
      service.storeCurrentPlayerName("anything");
      td.verify(mockSessionStorageWrapper.setItem('dominoes-player-name', "anything"));
      TestHelper.expectNothing();
    });
    it('should get the player name in session storage ', () => {
      td.when(mockSessionStorageWrapper.getItem("dominoes-player-name")).thenReturn(playerName);

      expect(service.getCurrentPlayerName()).toEqual(playerName);
    });
    it('should clear the current player name', () => {
      service.unsetCurrentPlayerName();
      td.verify(mockSessionStorageWrapper.removeItem('dominoes-player-name'));
      TestHelper.expectNothing();
    });
  });
});
