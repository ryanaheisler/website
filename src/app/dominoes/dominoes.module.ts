import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DominoesContainerComponent } from './components/dominoes-container/dominoes-container.component';
import { DominoesLandingPageComponent } from './components/dominoes-landing-page/dominoes-landing-page.component';
import { DominoesStorageService } from './services/dominoes-storage/dominoes-storage.service';

export const DOMINOES_ROUTES = [
  {
    path: 'dominoes',
    component: DominoesContainerComponent,
    children: [
      {
        path: '',
        component: DominoesLandingPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(DOMINOES_ROUTES),
    FormsModule,
  ],
  declarations: [DominoesContainerComponent, DominoesLandingPageComponent],
  providers: [
    DominoesStorageService,
  ],
  entryComponents: [],
})
export class DominoesModule { }
