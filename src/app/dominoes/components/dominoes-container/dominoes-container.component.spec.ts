import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DominoesContainerComponent } from './dominoes-container.component';

describe('DominoesContainerComponent', () => {
  let component: DominoesContainerComponent;
  let fixture: ComponentFixture<DominoesContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DominoesContainerComponent, MockRouterOutlet ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DominoesContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show/hide the router outlet when the settings are toggled', () => {
    let outlet = fixture.nativeElement.querySelector('router-outlet');
    expect(outlet).toBeTruthy();
});

@Component({
selector: 'router-outlet',
template: ''
})
class MockRouterOutlet { }
});
