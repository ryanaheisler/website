import { TestHelper } from 'src/test/test-helper';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import * as td from 'testdouble';
import { DominoesLandingPageComponent } from './dominoes-landing-page.component';
import { DominoesStorageService } from '../../services/dominoes-storage/dominoes-storage.service';

describe('DominoesLandingPageComponent', () => {
  let fixture: ComponentFixture<DominoesLandingPageComponent>;
  let mockModal: any;
  let mockRouter: any;
  let mockActivatedRoute: any;
  let mockToastrService: ToastrService;
  let mockStorageService: DominoesStorageService;

  beforeEach(async(() => {
    mockModal = td.object(['load']);
    mockModal.stateEvents = { subscribe: td.function() };
    mockRouter = td.object(['navigate']);
    mockActivatedRoute = { parent: "parent route" };
    mockToastrService = td.object(ToastrService.prototype);
    mockStorageService = td.object(DominoesStorageService.prototype);
    TestBed.configureTestingModule({
      declarations: [DominoesLandingPageComponent],
      imports: [FormsModule, ToastrModule],
      providers: [
        { provide: Router, useValue: mockRouter },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: ToastrService, useValue: mockToastrService },
        { provide: DominoesStorageService, useValue: mockStorageService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DominoesLandingPageComponent);
    fixture.detectChanges();
  });

  describe('component', () => {
    it('should clear the current game ID and player name from session storage', () => {
      td.verify(mockStorageService.unsetCurrentLobbyID());
      td.verify(mockStorageService.unsetCurrentPlayerName());
      TestHelper.expectNothing();
    });
  });

  describe('new lobby button', () => {
    let button: HTMLButtonElement;

    beforeEach(() => {
      button = fixture.nativeElement.querySelectorAll('button')[0];
    });

    it('should exist', () => {
      expect(button.innerText).toEqual('New Lobby');
    });

    it('should navigate to the game page when new game', () => {
      // const game = new GameObjectFromServer("J3SD");
      // td.when(mockGameService.newGame()).thenReturn(of(game));

      // td.when(mockCookieService.check(Cookies.hasDismissedIntroModal)).thenReturn(true);
      // td.when(mockCookieService.get(Cookies.hasDismissedIntroModal)).thenReturn("true");

      button.click()

      // td.verify(mockModal.load(td.matchers.anything()), { times: 0 });
      // td.verify(mockModal.stateEvents.subscribe(td.matchers.anything()), { times: 0 });
      // td.verify(mockRouter.navigate([`${game.gameID}`], { relativeTo: mockActivatedRoute }));
      // expect().nothing();
      expect().nothing();
    });
  });

  describe('join lobby button', () => {

    let button: HTMLButtonElement;
    let label: HTMLLabelElement;
    let input: HTMLInputElement;

    beforeEach(() => {
      button = fixture.nativeElement.querySelectorAll('button')[1];
      label = fixture.nativeElement.querySelector('label');
      input = fixture.nativeElement.querySelector('#lobby-input');
    });

    it('should exist with a text input', () => {
      expect(label.innerText).toEqual("Enter a Lobby ID");
      expect(label.htmlFor).toEqual("lobby-input");
      expect(input.type).toEqual('text');
      expect(button.innerText).toEqual('Join Lobby');
    });

    it('should navigate to the game page if player enters a valid game ID and clicks Join Game', () => {
      // const game = new GameObjectFromServer("j3sd");

      // td.when(mockGameService.getExistingGame("J3SD")).thenReturn(of(game));

      // input.value = game.gameID;
      // input.dispatchEvent(new Event('input'));

      // button.click();

      // td.verify(mockRouter.navigate([`J3SD`], { relativeTo: mockActivatedRoute }));

      expect().nothing();
    });

    it('should show toast and not navigate if game ID entered is not valid', () => {
      // const gameID = "J3SD";

      // td.when(mockGameService.getExistingGame(gameID)).thenReturn(of(null));

      // input.value = gameID;
      // input.dispatchEvent(new Event('input'));

      // button.click();

      // td.verify(mockRouter.navigate(td.matchers.anything()), { times: 0 });
      // td.verify(mockToastrService.error("The game ID you entered does not match a current game.", "GAME NOT FOUND", { positionClass: "toast-center-center" }));
      expect().nothing();
    });
  });
});
