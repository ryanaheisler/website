import { Component, OnInit } from '@angular/core';
import { DominoesStorageService } from '../../services/dominoes-storage/dominoes-storage.service';

@Component({
  selector: 'dominoes-landing-page',
  templateUrl: './dominoes-landing-page.component.html',
  styleUrls: ['./dominoes-landing-page.component.css']
})
export class DominoesLandingPageComponent implements OnInit {

  constructor(private storageService: DominoesStorageService) { }

  ngOnInit() {
    this.storageService.unsetCurrentLobbyID();
    this.storageService.unsetCurrentPlayerName();
  }

}
