import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MainPhotoComponent } from '../main-photo/main-photo.component';
import { MainTextComponent } from './../main-text/main-text.component';
import { HomePageComponent } from './home-page.component';


describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePageComponent, MainPhotoComponent, MainTextComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have main photo component and main text component', () => {
    const fixture = TestBed.createComponent(HomePageComponent);
    fixture.detectChanges();

    const photo = fixture.debugElement.nativeElement.querySelector('main-photo');
    const text = fixture.debugElement.nativeElement.querySelector('main-text');

    expect(photo).toBeTruthy();
    expect(text).toBeTruthy();

  });
});
