import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeisldiceModule } from './heisldice/heisldice.module';
import { HomePageComponent } from './home-page/home-page.component';
import { MainPhotoComponent } from './main-photo/main-photo.component';
import { MainTextComponent } from './main-text/main-text.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DominoesModule } from './dominoes/dominoes.module';

@NgModule({
  declarations: [
    AppComponent,
    MainPhotoComponent,
    HomePageComponent,
    MainTextComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HeisldiceModule,
    DominoesModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
