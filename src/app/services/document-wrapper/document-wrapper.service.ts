import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DocumentWrapperService {

  constructor() { }

  public getDocument() {
    return document;
  }
}
