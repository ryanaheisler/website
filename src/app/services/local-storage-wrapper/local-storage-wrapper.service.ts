import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageWrapperService {

  constructor() { }

  public getItem(key: string): string {
    return window.localStorage.getItem(key);
  }

  public setItem(key: string, value: string): void {
    return window.localStorage.setItem(key, value);
  }

  public removeItem(key: string): void {
    return window.localStorage.removeItem(key);
  }

  public clear(): void {
    return window.localStorage.clear();
  }
}
