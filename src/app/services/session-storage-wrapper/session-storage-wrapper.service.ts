import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageWrapperService {

  constructor() { }

  public getItem(key: string): string {
    return window.sessionStorage.getItem(key);
  }

  public setItem(key: string, value: string): void {
    return window.sessionStorage.setItem(key, value);
  }

  public removeItem(key: string): void {
    return window.sessionStorage.removeItem(key);
  }

  public clear(): void {
    return window.sessionStorage.clear();
  }
}
