import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPhotoComponent } from './main-photo.component';

describe('MainPhotoComponent', () => {
  let component: MainPhotoComponent;
  let fixture: ComponentFixture<MainPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainPhotoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have an image from assets', () => {
    const imageTag: HTMLImageElement = fixture.debugElement.nativeElement.querySelector('div > img');

    expect(imageTag.src).toContain('assets/main.jpg')
    expect(imageTag.alt).toEqual('Ryan Heisler dressed nicely and smiling, sitting on a wicker bench in a sunlit enclosed porch.');
  });
});
