import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MainTextComponent } from './main-text.component';

describe('MainTextComponent', () => {
  let component: MainTextComponent;
  let fixture: ComponentFixture<MainTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainTextComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have a heading', () => {
    const heading = fixture.debugElement.nativeElement.querySelector('h1');
    expect(heading.innerText).toEqual('Ryan Heisler');
  });

  it('should have paragraphs', () => {
    const paragraphs = fixture.debugElement.nativeElement.querySelectorAll('p');
    expect(paragraphs.length).toEqual(5);
  });

  it('should have anchor tags with hrefs', () => {
    const anchors: NodeList = fixture.debugElement.nativeElement.querySelectorAll('a');
    expect(anchors.length).toEqual(5);
    anchors.forEach((anchor: HTMLAnchorElement) => {
      expect(anchor.href).toBeDefined();
    });
  });
});
