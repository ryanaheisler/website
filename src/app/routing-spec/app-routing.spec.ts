import { APP_ROUTES } from '../app-routing.module';
import { HomePageComponent } from '../home-page/home-page.component';

describe('App Routing', () => {
  it('should have the correct routes', () => {
    expect(APP_ROUTES).toContain({ path: '', component: HomePageComponent });
  });
});