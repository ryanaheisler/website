import { HeisldiceContainerComponent } from './../heisldice/components/heisldice-container/heisldice-container.component';
import { HeisldiceLandingPageComponent } from './../heisldice/components/heisldice-landing-page/heisldice-landing-page.component';
import { HeisldicePageComponent } from './../heisldice/components/heisldice-page/heisldice-page.component';
import { HEISLDICE_ROUTES } from './../heisldice/heisldice.module';
import { ScorecardResolver } from './../heisldice/resolvers/scorecard/scorecard-resolver';
describe('Heisldice Module Routes', () => {
  it('should have correct routes', () => {
    expect(HEISLDICE_ROUTES).toContain(
      {
        path: 'heisldice',
        component: HeisldiceContainerComponent,
        children: [
          {
            path: '',
            component: HeisldiceLandingPageComponent
          },
          {
            path: ':gameID',
            component: HeisldicePageComponent,
            resolve: { scorecard: ScorecardResolver }
          }
        ]
      });
  });
});