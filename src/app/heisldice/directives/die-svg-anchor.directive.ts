import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[dieSvgAnchor]'
})
export class DieSvgAnchorDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
