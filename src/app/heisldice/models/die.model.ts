export class DieModel {
  constructor(public value: number, public isReserved: boolean) {
  }

  toggleReserved() {
    this.isReserved = !this.isReserved;
  }
}