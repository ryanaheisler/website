import { Cookies } from "./cookies";

describe('Cookies', () => {
    it('should contain correct cookie names', () => {
        expect(Cookies.hasDismissedIntroModal).toEqual('dismissed-modal');
    });
});