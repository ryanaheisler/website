export class Category {

  public score: number;
  public selected: boolean;
  public fitsCurrentDice: boolean;
  public description: string;
  public section: 0 | 1;
  public inDangerZeroScore: boolean;

  constructor(description: string, score?: number, selected?: boolean, fitsCurrentDice?: boolean, section?: 0 | 1) {
    this.description = description;
    this.selected = selected || false;
    this.fitsCurrentDice = fitsCurrentDice || false;
    this.section = section || 0;
    this.inDangerZeroScore = false;

    if (score === undefined) {
      this.score = null;
    } else {
      this.score = score;
    }
  }

  public get scoreAsString() {
    if (this.score === null) {
      return '';
    } else {
      return this.score.toString();
    }
  }

  public scoreIsNull() {
    if (this.score === null) {
      return true;
    }
    return false;
  }
}