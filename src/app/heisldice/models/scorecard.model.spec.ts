import { Category } from './category.model';
import { Scorecard } from './scorecard.model';
describe('Scorecard', () => {
  let scorecard: Scorecard
  beforeEach(() => {
    scorecard = new Scorecard();
    scorecard.categories = [
      new Category('1'),
      new Category('2'),
      new Category('3'),
      new Category('4'),
      new Category('5'),
      new Category('6')
    ];

    scorecard.categories[0].section = 0;
    scorecard.categories[1].section = 1;
    scorecard.categories[2].section = 0;
    scorecard.categories[3].section = 0;
    scorecard.categories[4].section = 1;
    scorecard.categories[5].section = 0;
  });
  it('should return categories from top section', () => {
    expect(scorecard.getTopSection()).toEqual([scorecard.categories[0], scorecard.categories[2], scorecard.categories[3], scorecard.categories[5]])
  });
  it('should return categories from bottom section', () => {
    expect(scorecard.getBottomSection()).toEqual([scorecard.categories[1], scorecard.categories[4]])
  });
});