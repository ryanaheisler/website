import { Color } from "./color.model";

describe('Color', () => {
    it('should return the hex string for its parts', () => {
        expect(new Color(0, 0, 0).toHexString()).toEqual('#000000');
        expect(new Color(255, 0, 0).toHexString()).toEqual('#ff0000');
        expect(new Color(0, 255, 0).toHexString()).toEqual('#00ff00');
        expect(new Color(0, 0, 255).toHexString()).toEqual('#0000ff');
        expect(new Color(128, 64, 32).toHexString()).toEqual('#804020');
        expect(new Color(201, 13, 77).toHexString()).toEqual('#c90d4d');
        expect(new Color(0.0000001, 0, 0).toHexString()).toEqual('#000000');
        expect(new Color(97.5, 0, 0).toHexString()).toEqual('#620000');
        expect(new Color(241.3, 0, 0).toHexString()).toEqual('#f10000');
        expect(new Color(48.7, 0, 0).toHexString()).toEqual('#310000');
    });
    it('should return the rgb string for its parts', () => {
        expect(new Color(0, 0, 0).toRGBString()).toEqual('rgb(0, 0, 0)');
        expect(new Color(255, 0, 0).toRGBString()).toEqual('rgb(255, 0, 0)');
        expect(new Color(0, 255, 0).toRGBString()).toEqual('rgb(0, 255, 0)');
        expect(new Color(0, 0, 255).toRGBString()).toEqual('rgb(0, 0, 255)');
        expect(new Color(128, 64, 32).toRGBString()).toEqual('rgb(128, 64, 32)');
        expect(new Color(201, 13, 77).toRGBString()).toEqual('rgb(201, 13, 77)');
        expect(new Color(0.0000001, 0, 0).toRGBString()).toEqual('rgb(0, 0, 0)');
        expect(new Color(97.5, 0, 0).toRGBString()).toEqual('rgb(98, 0, 0)');
        expect(new Color(241.3, 0, 0).toRGBString()).toEqual('rgb(241, 0, 0)');
        expect(new Color(48.7, 0, 0).toRGBString()).toEqual('rgb(49, 0, 0)');
    });

    it('should have constructor for building from a hex code', () => {
        expect(Color.fromHexString('#000000')).toEqual(new Color(0, 0, 0));
        expect(Color.fromHexString('#ff0000')).toEqual(new Color(255, 0, 0));
        expect(Color.fromHexString('#00ff00')).toEqual(new Color(0, 255, 0));
        expect(Color.fromHexString('#0000ff')).toEqual(new Color(0, 0, 255));

        expect(Color.fromHexString('000000')).toEqual(new Color(0, 0, 0));
        expect(Color.fromHexString('ff0000')).toEqual(new Color(255, 0, 0));
        expect(Color.fromHexString('00ff00')).toEqual(new Color(0, 255, 0));
        expect(Color.fromHexString('0000ff')).toEqual(new Color(0, 0, 255));

        expect(Color.fromHexString('#804020')).toEqual(new Color(128, 64, 32));
        expect(Color.fromHexString('#c90d4d')).toEqual(new Color(201, 13, 77));
        expect(Color.fromHexString('#620000')).toEqual(new Color(98, 0, 0));
        expect(Color.fromHexString('#0000f1')).toEqual(new Color(0, 0, 241));
        expect(Color.fromHexString('#003100')).toEqual(new Color(0, 49, 0));
    });

    const hslTestCases = [
        { r: 0, g: 0, b: 0, h: 0, s_l: 0, l: 0 },
        { r: 255, g: 0, b: 0, h: 0, s_l: 1, l: 0.5 },
        { r: 0, g: 255, b: 0, h: 120, s_l: 1, l: 0.5 },
        { r: 0, g: 0, b: 255, h: 240, s_l: 1, l: 0.5 },
        { r: 255, g: 255, b: 0, h: 60, s_l: 1, l: 0.5 },
        { r: 0, g: 255, b: 255, h: 180, s_l: 1, l: 0.5 },
        { r: 255, g: 0, b: 255, h: 300, s_l: 1, l: 0.5 },
        { r: 255, g: 255, b: 255, h: 0, s_l: 0, l: 1 },
        { r: 128, g: 64, b: 32, h: 20, s_l: 0.6, l: 0.3137254901960784 },
        { r: 253, g: 255, b: 109, h: 61, s_l: 1, l: .7137254901960784 },
        { r: 1, g: 94, b: 13, h: 128, s_l: .9789473684210527, l: .18627450980392157 },
    ]
    hslTestCases.forEach(testCase => {
        it('should have the HSL values', () => {
            const color = new Color(testCase.r, testCase.g, testCase.b);
            expect(color.hue).toEqual(testCase.h);
            expect(color.saturation_L).toEqual(testCase.s_l);
            expect(color.lightness).toEqual(testCase.l);
        });

        it('should have constructor for building from HSL values code', () => {
            const color: Color = Color.fromHSL(testCase.h, testCase.s_l, testCase.l);

            expect(color.red).toEqual(testCase.r);
            expect(color.green).toEqual(testCase.g);
            expect(color.blue).toEqual(testCase.b);
        });
    });

    const hsvTestCases = [
        { r: 0, g: 0, b: 0, h: 0, s_v: 0, v: 0 },
        { r: 62, g: 89, b: 89, h: 180, s_v: 0.3, v: 0.35 },
        { r: 127, g: 67, b: 204, h: 266, s_v: 0.67, v: 0.8 },
        { r: 162, g: 144, b: 166, h: 289, s_v: 0.13, v: 0.65 },
        { r: 43, g: 23, b: 17, h: 14, s_v: 0.6, v: 0.17 },
        { r: 0, g: 91, b: 166, h: 207, s_v: 1, v: 0.65 },
        { r: 99, g: 99, b: 99, h: 0, s_v: 0, v: 0.39 },
        { r: 143, g: 99, b: 11, h: 40, s_v: 0.92, v: 0.56 },
        { r: 73, g: 189, b: 47, h: 109, s_v: 0.75, v: 0.74 },
        { r: 26, g: 25, b: 28, h: 260, s_v: 0.11, v: 0.11 },
        { r: 26, g: 25, b: 28, h: 620, s_v: 0.11, v: 0.11 },
        { r: 26, g: 25, b: 28, h: -100, s_v: 0.11, v: 0.11 },
        { r: 128, g: 64, b: 64, h: 0, s_v: .5, v: .5 },
        { r: 255, g: 128, b: 128, h: 0, s_v: .5, v: 1 }
    ]
    hsvTestCases.forEach(testCase => {
        it(`should have the HSV values rgb(${testCase.r}, ${testCase.g}, ${testCase.b}) -> hsv(${testCase.h}, ${testCase.s_v}, ${testCase.v})`, () => {
            const color = new Color(testCase.r, testCase.g, testCase.b);
            let h = testCase.h;
            if (h >= 360) h -= 360;
            if (h < 0) h += 360;

            expect(color.hue).toEqual(h);
            expect(Math.round(color.saturation_V * 100)).toEqual(Math.round(testCase.s_v * 100));
            expect(Math.round(color.value * 100)).toEqual(Math.round(testCase.v * 100));
        });

        it('should have constructor for building from HSL values code', () => {
            const color: Color = Color.fromHSV(testCase.h, testCase.s_v, testCase.v);

            expect(color.red).toEqual(testCase.r);
            expect(color.green).toEqual(testCase.g);
            expect(color.blue).toEqual(testCase.b);
        });
    });

    const hslExtraDegreeTestCases = [
        { r: 255, g: 0, b: 0, h: 360, s: 1, l: 0.5 },
        { r: 255, g: 0, b: 0, h: 359.999, s: 1, l: 0.5 },
        { r: 0, g: 255, b: 0, h: 480, s: 1, l: 0.5 },
        { r: 0, g: 0, b: 255, h: -120, s: 1, l: 0.5 },
        { r: 255, g: 255, b: 0, h: -300, s: 1, l: 0.5 },
        { r: 0, g: 255, b: 255, h: 540, s: 1, l: 0.5 },
        { r: 255, g: 0, b: 255, h: -60, s: 1, l: 0.5 },
        { r: 255, g: 255, b: 255, h: 360, s: 0, l: 1 },
        { r: 128, g: 64, b: 32, h: -340, s: 0.6, l: 0.3137254901960784 },
        { r: 253, g: 255, b: 109, h: 421, s: 1, l: .7137254901960784 },
        { r: 1, g: 94, b: 13, h: -232, s: .9789473684210527, l: .18627450980392157 },
    ]
    hslExtraDegreeTestCases.forEach(testCase => {
        it(`should build from HSL values outside 0-359.999 correctly - HSL(${testCase.h}, ${testCase.s}, ${testCase.l})`, () => {
            const color: Color = Color.fromHSL(testCase.h, testCase.s, testCase.l);

            expect(color.red).toEqual(testCase.r);
            expect(color.green).toEqual(testCase.g);
            expect(color.blue).toEqual(testCase.b);
        });
    });

    const nearestColorTestCases = [
        { color: new Color(128, 24, 201), expectedNearest: new Color(150, 0, 255) },
        { color: new Color(59, 71, 23), expectedNearest: new Color(191, 255, 0) },
        { color: new Color(150, 176, 166), expectedNearest: new Color(0, 255, 157) },
        { color: new Color(69, 47, 38), expectedNearest: new Color(255, 74, 0) },
        { color: new Color(36, 1, 12), expectedNearest: new Color(255, 0, 85) },
        { color: new Color(133, 173, 255), expectedNearest: new Color(0, 84, 255) },
        { color: new Color(199, 169, 169), expectedNearest: new Color(255, 0, 0) },
    ]
    nearestColorTestCases.forEach(testCase => {
        it(`should get the nearest color with full saturation and half lightness - (${testCase.color.red}, ${testCase.color.green}, ${testCase.color.blue})`, () => {
            const nearest = Color.getNearestFullySaturatedColor(testCase.color);

            const margin = 4;

            expect(nearest.red).toBeGreaterThanOrEqual(testCase.expectedNearest.red - margin);
            expect(nearest.red).toBeLessThanOrEqual(testCase.expectedNearest.red + margin);

            expect(nearest.green).toBeGreaterThanOrEqual(testCase.expectedNearest.green - margin);
            expect(nearest.green).toBeLessThanOrEqual(testCase.expectedNearest.green + margin);

            expect(nearest.blue).toBeGreaterThanOrEqual(testCase.expectedNearest.blue - margin);
            expect(nearest.blue).toBeLessThanOrEqual(testCase.expectedNearest.blue + margin);
        });
    });
});