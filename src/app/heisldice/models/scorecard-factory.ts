import { Injectable } from '@angular/core';
import { Category } from "./category.model";
import { Scorecard } from "./scorecard.model";

@Injectable({
  providedIn: 'root'
})
export class ScorecardFactory {

  build(json): Scorecard {
    const scorecard = new Scorecard();

    Object.keys(json.categories).forEach(key => {
      const currentCategory = json.categories[key];

      const category = new Category(
        key,
        currentCategory.score,
        currentCategory.selected,
        currentCategory.fitsCurrentDice,
        currentCategory.section
      );

      scorecard.categories.push(category);
    });

    scorecard.topSectionBonus = json.topSectionBonus;
    scorecard.topSectionTotal = json.topSectionTotal;
    scorecard.extraYahtzeeBonusTotal = json.extraYahtzeeBonusTotal;
    scorecard.bottomSectionTotal = json.bottomSectionTotal;
    scorecard.total = json.total;

    return scorecard;
  }
}