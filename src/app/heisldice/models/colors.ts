export const COLORS = {
    red: '#ff0000',
    yellow: '#ffff00',
    green: '#00ff00',
    cyan: '#00ffff',
    blue: '#0000ff',
    magenta: '#ff00ff',
}