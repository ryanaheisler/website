import { CookieBoolean } from './cookie-boolean';
describe('CookieBoolean', () => {
    it('should take a boolean and return a string version', () => {
        const trueBool = CookieBoolean.fromBoolean(true);
        const falseBool = CookieBoolean.fromBoolean(false);

        expect(trueBool.string).toEqual('true');
        expect(falseBool.string).toEqual('false');
        expect(trueBool.bool).toEqual(true);
        expect(falseBool.bool).toEqual(false);
    });

    it('should take a string and return a boolean version', () => {
        const trueBool = CookieBoolean.fromString('true');
        const falseBool = CookieBoolean.fromString('false');
        const falseAlso = CookieBoolean.fromString('typo');

        expect(trueBool.string).toEqual('true');
        expect(falseBool.string).toEqual('false');
        expect(trueBool.bool).toEqual(true);
        expect(falseBool.bool).toEqual(false);

        expect(falseAlso.string).toEqual('false');
        expect(falseAlso.bool).toEqual(false);
    });
});