import { Player } from './player.model';
export class GameObjectFromServer {
  public players: Player[]
  constructor(public gameID: string, players: Player[] = []) {
    this.players = players;
  }
}