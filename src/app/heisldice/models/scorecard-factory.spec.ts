import { Category } from './category.model';
import { ScorecardFactory } from './scorecard-factory';
import { Scorecard } from './scorecard.model';
describe('Scorecard', () => {
  it('should have correct attributes', () => {
    const jsonFromAPI = {
      topSectionBonus: 0,
      topSectionTotal: 0,
      extraYahtzeeBonusTotal: 0,
      bottomSectionTotal: 0,
      total: 0,
      categories: {
        'Ones': {
          score: null,
          selected: false,
          fitsCurrentDice: false,
          section: 1
        },
        'Sevens': {
          score: 21,
          selected: false,
          fitsCurrentDice: true,
          section: 0
        },
        'Big ol\' stret': {
          score: 0,
          selected: true,
          fitsCurrentDice: true,
          section: 1
        }
      }
    };
    const factory = new ScorecardFactory();
    const scorecard: Scorecard = factory.build(jsonFromAPI);

    const expectedCategory1 = new Category('Ones', null, false, false, 1);
    const expectedCategory2 = new Category('Sevens', 21, false, true, 0);
    const expectedCategory3 = new Category('Big ol\' stret', 0, true, true, 1);

    expect(scorecard.categories.length).toEqual(3);
    checkCategory(scorecard.categories[0], expectedCategory1);
    checkCategory(scorecard.categories[1], expectedCategory2);
    checkCategory(scorecard.categories[2], expectedCategory3);

    expect(scorecard.topSectionBonus).toEqual(0);
    expect(scorecard.topSectionTotal).toEqual(0);
    expect(scorecard.extraYahtzeeBonusTotal).toEqual(0);
    expect(scorecard.bottomSectionTotal).toEqual(0);
    expect(scorecard.total).toEqual(0);
  });

});
function checkCategory(actual: Category, expected: Category) {
  expect(actual.score).toEqual(expected.score);
  expect(actual.description).toEqual(expected.description);
  expect(actual.selected).toEqual(expected.selected);
  expect(actual.fitsCurrentDice).toEqual(expected.fitsCurrentDice);
  expect(actual.section).toEqual(expected.section);
}