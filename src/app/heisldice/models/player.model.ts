import { Scorecard } from './scorecard.model';
export class Player {
  constructor(
    public name: string,
    public rollsRemaining: number,
    public turnsRemaining: number,
    public previousDice: number[],
    public myTurn: boolean,
    public scorecard: Scorecard,
    public diceColor?: string
  ) { }
}

export const DEFAULT_PLAYER = () => {
  return new Player('', 3, 0, [1, 2, 3, 4, 5], false, new Scorecard(), "#ff0000");
}