export class CookieBoolean {

    public static fromBoolean(value: boolean) {
        const cookieBoolean = new CookieBoolean()
        cookieBoolean.bool = value;

        switch (value) {
            case true:
                cookieBoolean.string = 'true';
                break;
            case false:
                cookieBoolean.string = 'false';
                break;
        }
        return cookieBoolean;
    }

    public static fromString(value: string) {
        const cookieBoolean = new CookieBoolean()
        cookieBoolean.string = value;

        switch (value) {
            case 'true':
                cookieBoolean.bool = true;
                break;
            case 'false':
                cookieBoolean.bool = false;
                break;
            default:
                cookieBoolean.string = 'false';
                cookieBoolean.bool = false;
        }
        return cookieBoolean;
    }

    public string: string;
    public bool: boolean;

    private constructor() {}
}