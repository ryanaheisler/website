export class Color {

    public hue: number;
    public saturation_L: number;
    public lightness: number;
    public saturation_V: number;
    public value: number

    constructor(public red: number, public green: number, public blue: number) {
        const red0to1 = red / 255;
        const green0to1 = green / 255;
        const blue0to1 = blue / 255;

        const colorMin = Math.min(red0to1, green0to1, blue0to1);
        const colorMax = Math.max(red0to1, green0to1, blue0to1);

        const chroma = colorMax - colorMin;

        this.hue = 0;
        this.saturation_L = 0;
        this.lightness = 0;
        this.saturation_V = 0;
        this.value = colorMax;        

        if (chroma == 0)
            this.hue = 0;

        else if (colorMax == red0to1)
            this.hue = ((green0to1 - blue0to1) / chroma) % 6;
        else if (colorMax == green0to1)
            this.hue = (blue0to1 - red0to1) / chroma + 2;
        else
            this.hue = (red0to1 - green0to1) / chroma + 4;

        this.hue = Math.round(this.hue * 60);

        if (this.hue < 0)
            this.hue += 360;

        this.lightness = (colorMax + colorMin) / 2;

        this.saturation_L = chroma === 0 ? 0 : chroma / (1 - Math.abs(2 * this.lightness - 1));

        this.saturation_V = this.value === 0 ? 0 : chroma / this.value;
    }

    public static fromHexString(hexString: string): Color {
        if (hexString[0] === "#") {
            hexString = hexString.substring(1);
        }

        const red = Number(`0x${hexString.substring(0, 2)}`);
        const green = Number(`0x${hexString.substring(2, 4)}`);
        const blue = Number(`0x${hexString.substring(4, 6)}`);

        return new Color(red, green, blue);
    }

    public static fromHSL(hue: number, saturation: number, lightness: number): Color {
        if (hue >= 360) hue -= 360;
        if (hue < 0) hue += 360;

        const chroma = (1 - Math.abs(2 * lightness - 1)) * saturation;
        const colorWithoutMatchLightness = Color._getColorWithoutMatch(chroma, hue);
        
        const matchLightness = lightness - chroma / 2;

        const r = Math.round((colorWithoutMatchLightness.r + matchLightness) * 255);
        const g = Math.round((colorWithoutMatchLightness.g + matchLightness) * 255);
        const b = Math.round((colorWithoutMatchLightness.b + matchLightness) * 255);

        return new Color(r, g, b);
    }

    public static fromHSV(hue: number, saturation: number, value: number): Color {
        if (hue >= 360) hue -= 360;
        if (hue < 0) hue += 360;

        const chroma = value * saturation;

        const colorWithoutMatchValue = Color._getColorWithoutMatch(chroma, hue);

        const matchValue = value - chroma;

        const r = Math.round((colorWithoutMatchValue.r + matchValue) * 255);
        const g = Math.round((colorWithoutMatchValue.g + matchValue) * 255);
        const b = Math.round((colorWithoutMatchValue.b + matchValue) * 255);

        return new Color(r, g, b);
    }

    private static _getColorWithoutMatch(chroma: number, hue: number): {r: number, g: number, b: number} {
        const medianColorComponent = chroma * (1 - Math.abs((hue / 60) % 2 - 1));

        let r = 0;
        let g = 0;
        let b = 0;

        if (0 <= hue && hue < 60) {
            r = chroma; g = medianColorComponent; b = 0;
        } else if (60 <= hue && hue < 120) {
            r = medianColorComponent; g = chroma; b = 0;
        } else if (120 <= hue && hue < 180) {
            r = 0; g = chroma; b = medianColorComponent;
        } else if (180 <= hue && hue < 240) {
            r = 0; g = medianColorComponent; b = chroma;
        } else if (240 <= hue && hue < 300) {
            r = medianColorComponent; g = 0; b = chroma;
        } else if (300 <= hue && hue < 360) {
            r = chroma; g = 0; b = medianColorComponent;
        }
        return {r, g, b};
    }

    public static getNearestFullySaturatedColor(color: Color): Color {
        return Color.fromHSL(color.hue, 1, 0.5);
    }

    public toHexString(): string {
        const red = Math.round(this.red).toString(16).padStart(2, '0');
        const green = Math.round(this.green).toString(16).padStart(2, '0');
        const blue = Math.round(this.blue).toString(16).padStart(2, '0');
        return `#${red}${green}${blue}`;
    }

    public toRGBString(): string {
        const red = Math.round(this.red);
        const green = Math.round(this.green);
        const blue = Math.round(this.blue);
        return `rgb(${red}, ${green}, ${blue})`;
    }
}