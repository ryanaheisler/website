import { Category } from './category.model';
export class Scorecard {
  public categories: Category[];
  public topSectionBonus: number;
  public topSectionTotal: number;
  public extraYahtzeeBonusTotal: number;
  public bottomSectionTotal: number;
  public total: number;

  constructor() {
    this.categories = [];
  }

  public getTopSection(): Category[] {
    return this.categories.filter(category => {
      return category.section === 0
    });
  }
  public getBottomSection(): Category[] {
    return this.categories.filter(category => {
      return category.section === 1
    });
  }
}