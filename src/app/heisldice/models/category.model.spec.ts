import { Category } from './category.model';
describe('Scorecard Category', () => {
  it('should have description, score, selected boolean, and fitCurrentDice boolean', () => {
    const description = 'Nine of a hind';
    let category = new Category(description);

    expect(category.description).toEqual(description);
    expect(category.score).toBeNull();
    expect(category.selected).toBe(false);
    expect(category.fitsCurrentDice).toBe(false);
    expect(category.section).toBe(0);
    expect(category.inDangerZeroScore).toBe(false);
  });

  it('should return score as string', () => {
    const category = new Category('jjj');
    expect(category.scoreAsString).toEqual('');

    category.score = 0;
    expect(category.scoreAsString).toEqual('0');

    category.score = 42;
    expect(category.scoreAsString).toEqual('42');
  });

  it('should check if score is null', () => {
    const category = new Category('jjj');
    expect(category.scoreIsNull()).toBe(true);

    category.score = 0;
    expect(category.scoreIsNull()).toBe(false);

    category.score = 42;
    expect(category.scoreIsNull()).toBe(false);
  });
});