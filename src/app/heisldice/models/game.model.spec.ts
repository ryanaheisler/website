import { TestHelper } from 'src/test/test-helper';
import { Category } from './category.model';
import { GameObjectFromServer } from './game.model';
import { Scorecard } from './scorecard.model';
describe('Game', () => {
  it('should have correct default fields', () => {
    const id = "gameID";
    const game = new GameObjectFromServer(id);

    expect(game.gameID).toEqual(id);
    expect(game.players).toEqual([]);
  });

  it('should accept a player array', () => {
    const id = "gameID";
    const scorecard = new Scorecard();
    scorecard.categories.push(new Category('knives', 44, false, true, 0))
    const players = [
      TestHelper.constructPlayer("one", 4, 2, [1, 2, 3, 4, 5]),
      TestHelper.constructPlayer("two", 4, 2, [1, 2, 3, 4, 5]),
      TestHelper.constructPlayer("three", 4, 2, [1, 2, 3, 4, 5])
    ];
    players[0].scorecard = scorecard;
    players[1].scorecard = scorecard;
    players[2].scorecard = scorecard;

    const game = new GameObjectFromServer(id, players);

    expect(game.players).toEqual(players);
  });
});