import { DieFilledOneComponent } from './../../components/die-icons/die-filled-one/die-filled-one.component';
import { DieBorderOneComponent } from './../../components/die-icons/die-border-one/die-border-one.component';
import { DieIcon } from './../../components/die-icons/die-icon.interface';
import { DieModel } from './../../models/die.model';
import { Injectable, Type } from '@angular/core';
import { DieBorderTwoComponent } from '../../components/die-icons/die-border-two/die-border-two.component';
import { DieFilledTwoComponent } from '../../components/die-icons/die-filled-two/die-filled-two.component';
import { DieBorderThreeComponent } from '../../components/die-icons/die-border-three/die-border-three.component';
import { DieFilledThreeComponent } from '../../components/die-icons/die-filled-three/die-filled-three.component';
import { DieBorderFourComponent } from '../../components/die-icons/die-border-four/die-border-four.component';
import { DieFilledFourComponent } from '../../components/die-icons/die-filled-four/die-filled-four.component';
import { DieFilledSixComponent } from '../../components/die-icons/die-filled-six/die-filled-six.component';
import { DieBorderSixComponent } from '../../components/die-icons/die-border-six/die-border-six.component';
import { DieFilledFiveComponent } from '../../components/die-icons/die-filled-five/die-filled-five.component';
import { DieBorderFiveComponent } from '../../components/die-icons/die-border-five/die-border-five.component';

@Injectable({
  providedIn: 'root'
})
export class DieSvgLookupService {

  constructor() { }

  public getSvgComponent(die: DieModel): Type<DieIcon> {
    return iconComponents[die.value][die.isReserved];
  }
}

const iconComponents = {
  1: {
    false: DieBorderOneComponent,
    true: DieFilledOneComponent
  },
  2: {
    false: DieBorderTwoComponent,
    true: DieFilledTwoComponent
  },
  3: {
    false: DieBorderThreeComponent,
    true: DieFilledThreeComponent
  },
  4: {
    false: DieBorderFourComponent,
    true: DieFilledFourComponent
  },
  5: {
    false: DieBorderFiveComponent,
    true: DieFilledFiveComponent
  },
  6: {
    false: DieBorderSixComponent,
    true: DieFilledSixComponent
  },
};