import { DieModel } from './../../models/die.model';
import { TestBed } from '@angular/core/testing';
import { DieSvgLookupService } from './die-svg-lookup.service';
import { DieIcon } from '../../components/die-icons/die-icon.interface';
import { Type } from '@angular/core';
import { DieBorderOneComponent } from '../../components/die-icons/die-border-one/die-border-one.component';
import { DieFilledOneComponent } from '../../components/die-icons/die-filled-one/die-filled-one.component';
import { DieBorderTwoComponent } from '../../components/die-icons/die-border-two/die-border-two.component';
import { DieFilledTwoComponent } from '../../components/die-icons/die-filled-two/die-filled-two.component';
import { DieBorderThreeComponent } from '../../components/die-icons/die-border-three/die-border-three.component';
import { DieFilledThreeComponent } from '../../components/die-icons/die-filled-three/die-filled-three.component';
import { DieBorderFourComponent } from '../../components/die-icons/die-border-four/die-border-four.component';
import { DieFilledFourComponent } from '../../components/die-icons/die-filled-four/die-filled-four.component';
import { DieFilledSixComponent } from '../../components/die-icons/die-filled-six/die-filled-six.component';
import { DieBorderSixComponent } from '../../components/die-icons/die-border-six/die-border-six.component';
import { DieFilledFiveComponent } from '../../components/die-icons/die-filled-five/die-filled-five.component';
import { DieBorderFiveComponent } from '../../components/die-icons/die-border-five/die-border-five.component';

describe('DieSvgLookupService', () => {
  let service: DieSvgLookupService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(DieSvgLookupService);
  });

  const testCases = [
    { die: new DieModel(1, false), expectedClass: DieBorderOneComponent },
    { die: new DieModel(1, true), expectedClass: DieFilledOneComponent },
    { die: new DieModel(2, false), expectedClass: DieBorderTwoComponent },
    { die: new DieModel(2, true), expectedClass: DieFilledTwoComponent },
    { die: new DieModel(3, false), expectedClass: DieBorderThreeComponent },
    { die: new DieModel(3, true), expectedClass: DieFilledThreeComponent },
    { die: new DieModel(4, false), expectedClass: DieBorderFourComponent },
    { die: new DieModel(4, true), expectedClass: DieFilledFourComponent },
    { die: new DieModel(5, false), expectedClass: DieBorderFiveComponent },
    { die: new DieModel(5, true), expectedClass: DieFilledFiveComponent },
    { die: new DieModel(6, false), expectedClass: DieBorderSixComponent },
    { die: new DieModel(6, true), expectedClass: DieFilledSixComponent },
  ]
  testCases.forEach((testCase) => {
    const should = 'should return the correct Die SVG component -';
    const pips = `${testCase.die.value} pip${testCase.die.value > 1 ? 's' : ''}`;
    const reserved = `${testCase.die.isReserved ? '' : 'not'} reserved`;

    it(`${should} ${pips} ${reserved}`, () => {
      const iconClass: Type<DieIcon> = service.getSvgComponent(testCase.die);

      expect(iconClass).toBe(testCase.expectedClass);
    });
  });
});
