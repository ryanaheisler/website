import { TestBed } from '@angular/core/testing';

import { LoggerService } from './logger.service';

describe('LoggerService', () => {
  let originalConsoleLog, originalConsoleError;
  let messagePassedToLog, messagePassedToError;

  beforeEach(() => {
    originalConsoleLog = console.log;
    originalConsoleError = console.error;

    messagePassedToLog = undefined;
    messagePassedToError = undefined;

    console.log = (message) => {
      messagePassedToLog = message;
    }

    console.error = (message) => {
      messagePassedToError = message;
    }

    TestBed.configureTestingModule({})
  });

  afterEach(() => {
    console.log = originalConsoleLog;
    console.error = originalConsoleError;
  });

  it('should write to console.log', () => {
    const service: LoggerService = TestBed.get(LoggerService);
    const message = "hello";
    service.log(message);
    expect(messagePassedToLog).toBe(message);
  });

  it('should write to console.error', () => {
    const service: LoggerService = TestBed.get(LoggerService);
    const message = "hello";
    service.error(message);
    expect(messagePassedToError).toBe(message);
  });
});
