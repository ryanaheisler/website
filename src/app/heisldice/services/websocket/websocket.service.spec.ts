import { TestBed } from '@angular/core/testing';
import { WebSocketSubject } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';
import * as td from "testdouble";
import { WebsocketWrapperService } from './../websocket-wrapper/websocket-wrapper.service';
import { HeisldiceWebsocketService } from './websocket.service';

describe('WebsocketService', () => {

  let oldEnvironmentHost = environment.backendHost;
  let oldEnvironmentPort = environment.backendPort;
  let mockWebsocketWrapper: WebsocketWrapperService;

  beforeEach(() => {
    environment.backendHost = 'baroquealmost';
    environment.backendPort = 5923;

    mockWebsocketWrapper = td.object(WebsocketWrapperService.prototype);
    TestBed.configureTestingModule({
      providers: [
        { provide: WebsocketWrapperService, useValue: mockWebsocketWrapper }
      ]
    })
  });

  afterEach(() => {
    environment.backendHost = oldEnvironmentHost;
    environment.backendPort = oldEnvironmentPort;
  });

  it('should return a websocket subject pointing at the backend server with optional callback', () => {
    const optionsCaptor = td.matchers.captor();
    const mockWebsocketSubject = td.object(WebSocketSubject.prototype);
    td.when(mockWebsocketWrapper.websocket(optionsCaptor.capture()))
      .thenReturn(mockWebsocketSubject);

    const openObserverCallback = td.function('openObserver');

    const service: HeisldiceWebsocketService = TestBed.get(HeisldiceWebsocketService);

    const subject = service.getWebSocketForGame('1234', openObserverCallback);

    expect(subject).toBe(mockWebsocketSubject);

    expect(optionsCaptor.value.url).toEqual(`ws://${environment.backendHost}:${environment.backendPort}?gameID=1234`);
    optionsCaptor.value.openObserver.next(1);
    td.verify(openObserverCallback(1));
  });
});