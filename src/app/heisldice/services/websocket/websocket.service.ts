import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { WebSocketSubject } from "rxjs/webSocket";
import { environment } from './../../../../environments/environment';
import { WebsocketWrapperService } from './../websocket-wrapper/websocket-wrapper.service';

@Injectable({
  providedIn: 'root'
})
export class HeisldiceWebsocketService {
  constructor(private websocketWrapperService: WebsocketWrapperService) { }

  getWebSocketForGame(gameID: string, successfulConnectionCallback: Function): WebSocketSubject<any> {
    const closeSubject = new Subject<CloseEvent>();
    closeSubject.subscribe((event: any) => {
      console.log('Underlying WebSocket connection closed', JSON.stringify(event))
    });
    return this.websocketWrapperService.websocket({
      url: `ws://${environment.backendHost}:${environment.backendPort}?gameID=${gameID}`,
      openObserver: new WebsocketOpenObserver(successfulConnectionCallback),
      closeObserver: closeSubject
    })
  }
}

export class WebsocketOpenObserver {
  private callback: Function;
  constructor(callback?: Function) {
    this.callback = callback || function () { };
  }

  next(message) {
    console.log(
      "Successfully connected to websocket at",
      new Date().toISOString().split('T')[1].split('.')[0]
    );

    this.callback(message);
  }
}