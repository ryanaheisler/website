import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ScorecardService {

  constructor(private http: HttpClient) {
  }

  getBlankScorecard(): Observable<any> {
    return this.http.get<any>(`http://${environment.backendHost}:${environment.backendPort}/heisldice/scorecard`);
  }
}
