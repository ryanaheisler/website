import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as td from 'testdouble';
import { ScorecardService } from './scorecard.service';

describe('ScorecardService', () => {
  let mockHttpClient;
  let mockObservable;
  let originalBackendHost = environment.backendHost;
  let originalBackendPort = environment.backendPort;
  beforeEach(() => {

    environment.backendHost = 'URL'
    environment.backendPort = 3214;

    mockObservable = td.object(Observable.prototype);

    mockHttpClient = td.object(HttpClient.prototype);
    td.when(mockHttpClient.get('http://URL:3214/heisldice/scorecard')).thenReturn(mockObservable);

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: mockHttpClient },
        ScorecardService
      ]
    });
  });

  afterEach(() => {
    environment.backendHost = originalBackendHost;
    environment.backendPort = originalBackendPort;
  });

  it('should get scorecard from HttpClient', () => {
    const service: ScorecardService = TestBed.get(ScorecardService);

    expect(service.getBlankScorecard()).toEqual(mockObservable);
  });
});
