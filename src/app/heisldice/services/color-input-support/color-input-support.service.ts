import { Injectable } from '@angular/core';
import { DocumentWrapperService } from 'src/app/services/document-wrapper/document-wrapper.service';

@Injectable({
  providedIn: 'root'
})
export class ColorInputSupportService {

  constructor(private documentWrapperService: DocumentWrapperService) { }

  public isColorInputSupported(): boolean {
    const document: Document = this.documentWrapperService.getDocument();
    const tempInput = document.createElement("input");
    tempInput.setAttribute("type", "color");

    const isSupported = tempInput.type === "color";

    return isSupported;
  }
}
