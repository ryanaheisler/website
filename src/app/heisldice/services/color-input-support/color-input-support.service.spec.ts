import { TestBed } from '@angular/core/testing';
import { DocumentWrapperService } from 'src/app/services/document-wrapper/document-wrapper.service';
import * as td from 'testdouble';
import { ColorInputSupportService } from './color-input-support.service';

describe('DiceColorInputSupportService', () => {
  let mockDocumentWrapper: DocumentWrapperService;
  let mockDocument: Document;
  let mockInputElement;
  let service: ColorInputSupportService;

  beforeEach(() => {
    mockInputElement = {
      setAttribute: td.function("setAttribute"),
      type: null
    }

    mockDocument = td.object(Document.prototype);
    td.when(mockDocument.createElement("input")).thenReturn(mockInputElement);

    mockDocumentWrapper = td.object(DocumentWrapperService.prototype);
    td.when(mockDocumentWrapper.getDocument()).thenReturn(mockDocument);

    TestBed.configureTestingModule({
      providers: [
        { provide: DocumentWrapperService, useValue: mockDocumentWrapper }
      ]
    });

    service = TestBed.get(ColorInputSupportService);
  });

  it('should be return true when color inputs are supported', () => {
    td.when(mockInputElement.setAttribute("type", "color")).thenDo(() => {
      mockInputElement.type = "color"
    });

    const isSupported = service.isColorInputSupported();

    expect(isSupported).toBe(true);
  });

  it('should be return true when color inputs are supported', () => {
    td.when(mockInputElement.setAttribute("type", "color")).thenDo(() => {
      mockInputElement.type = "text"
    });

    const isSupported = service.isColorInputSupported();

    expect(isSupported).toBe(false);
  });
});