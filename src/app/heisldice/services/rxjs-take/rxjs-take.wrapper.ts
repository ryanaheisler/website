import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RxjsTakeWrapper {

  constructor() { }

  take(count: number) {
    return take(count);
  }
}
