import { SettingsStorageService } from './../settings-storage/settings-storage.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { TestHelper } from 'src/test/test-helper';
import * as td from 'testdouble';
import { environment } from './../../../../environments/environment';
import { Player } from './../../models/player.model';
import { PlayerDataFromBackend, PlayerService } from './player.service';


describe('PlayerService', () => {
  let mockHttp: HttpClient;
  let originalBackendHost = environment.backendHost;
  let originalBackendPort = environment.backendPort;
  let expectedUrl: string;
  let player: Player;
  let gameID: string;
  let mockSettingsStorageService: SettingsStorageService;
  let color: string;

  beforeEach(() => {
    environment.backendHost = 'shmoakleghost'
    environment.backendPort = 3214;
    gameID = "4412";

    player = TestHelper.constructPlayer("Jimmy Smits");
    expectedUrl = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games/${gameID}/players`;

    mockHttp = td.object(HttpClient.prototype);

    color = "green";
    mockSettingsStorageService = td.object(SettingsStorageService.prototype);
    td.when(mockSettingsStorageService.getUserDiceColor()).thenReturn(color);

    TestBed.configureTestingModule({
      providers: [
        PlayerService,
        { provide: HttpClient, useValue: mockHttp },
        { provide: SettingsStorageService, useValue: mockSettingsStorageService },
      ]
    })
  });

  afterEach(() => {
    environment.backendHost = originalBackendHost;
    environment.backendPort = originalBackendPort;
  });

  describe('put', () => {
    it('(happy case) should send player information to the server and return Player', (done) => {
      const playerDataFromBackend: PlayerDataFromBackend = {
        name: "Ryan",
        rollsRemaining: 2,
        turnsRemaining: 12,
        previousDice: [6, 6, 6, 6, 6],
        myTurn: true,
        scorecard: {},
        diceColor: color
      }
      const response = {
        body: playerDataFromBackend,
        headers: new HttpHeaders(),
        status: 200,
        statusText: "OK",
        url: ""
      };
      const observableOfRawData = of(new HttpResponse<PlayerDataFromBackend>(response))
      const expectedPlayer = { playerName: player.name, diceColor: color };

      td.when(mockHttp.put(expectedUrl, expectedPlayer, { observe: 'response' })).thenReturn(observableOfRawData);
      const service: PlayerService = TestBed.get(PlayerService);

      const observable: Observable<Player> = service.put(player.name, gameID);

      observable.subscribe((result: Player) => {
        expect(result.name).toEqual(playerDataFromBackend.name);
        expect(result.rollsRemaining).toEqual(playerDataFromBackend.rollsRemaining);
        expect(result.turnsRemaining).toEqual(playerDataFromBackend.turnsRemaining);
        expect(result.previousDice).toEqual(playerDataFromBackend.previousDice);
        expect(result.myTurn).toEqual(playerDataFromBackend.myTurn);
        expect(result.diceColor).toEqual(playerDataFromBackend.diceColor);

        td.verify(mockSettingsStorageService.storeCurrentPlayerName(playerDataFromBackend.name));

        done();
      });

      td.verify(mockSettingsStorageService.storeCurrentPlayerName(player.name), { times: 0 });
    });
    it('(conflicting name case) should rethrow the error', (done) => {
      const response = {
        body: null,
        headers: new HttpHeaders(),
        status: 409,
        statusText: "Conflict",
        url: ""
      };
      const observableOfRawData = of(new HttpResponse<PlayerDataFromBackend>(response))
      const expectedPlayer = { playerName: player.name, diceColor: color };
      td.when(mockHttp.put(expectedUrl, expectedPlayer, { observe: 'response' })).thenReturn(observableOfRawData);
      const service: PlayerService = TestBed.get(PlayerService);

      const observable: Observable<Player> = service.put(player.name, gameID);

      observable.subscribe(
        (result: Player) => {
          expect(result).toBeNull();
          done();
        });
    });
  });
});
