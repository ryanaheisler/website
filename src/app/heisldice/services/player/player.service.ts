import { SettingsStorageService } from './../settings-storage/settings-storage.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Player } from '../../models/player.model';

@Injectable()
export class PlayerService {

  constructor(private httpClient: HttpClient, private settingsStorageService: SettingsStorageService) { }

  put(name: string, gameID: string): Observable<Player> {
    const url = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games/${gameID}/players`;
    const player = {
      playerName: name,
      diceColor: this.settingsStorageService.getUserDiceColor()
    };

    return this.httpClient.put<Player>(url, player, { observe: 'response' })
      .pipe(map((response: HttpResponse<PlayerDataFromBackend>) => {

        this.settingsStorageService.storeCurrentPlayerName(response.body.name);
        return new Player(
          response.body.name,
          response.body.rollsRemaining,
          response.body.turnsRemaining,
          response.body.previousDice,
          response.body.myTurn,
          response.body.scorecard,
          response.body.diceColor
        );
      }),
        catchError((error: any) => {
          return of(null);
        })
      );
  }
}

export interface PlayerDataFromBackend {
  scorecard: any;
  name: string;
  rollsRemaining: number;
  turnsRemaining: number;
  previousDice: number[];
  myTurn: boolean;
  diceColor: string;
}
