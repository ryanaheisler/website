import { STATUS_CODES } from './../../../constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { GameObjectFromServer } from './../../models/game.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private http: HttpClient) { }

  newGame(): Observable<GameObjectFromServer> {
    const backendURL = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games`;

    return this.http.post(backendURL, {}).pipe(
      map((resultData: GameDataFromBackend) => {
        return new GameObjectFromServer(resultData.gameID);
      }));
  }

  getExistingGame(gameID: string): Observable<GameObjectFromServer> {
    const backendURL = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games/${gameID}`;

    return this.http.get(backendURL, { observe: 'response' }).pipe(
      map((response: HttpResponse<GameDataFromBackend>) => {
        return new GameObjectFromServer(response.body.gameID);
      }),
      catchError((error: any) => {
        return of(null);
      }));
  }
}

export interface GameDataFromBackend {
  gameID: string;
}