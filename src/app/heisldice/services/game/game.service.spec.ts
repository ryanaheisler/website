import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as td from 'testdouble';
import { GameObjectFromServer } from './../../models/game.model';
import { GameService, GameDataFromBackend } from './game.service';

describe('GameService', () => {

  let mockHttp: HttpClient;
  let originalBackenPort;
  let originalBackenHost;

  beforeEach(() => {
    originalBackenHost = environment.backendHost
    originalBackenPort = environment.backendPort
    environment.backendHost = 'yokeltoast';
    environment.backendPort = 5745;

    mockHttp = td.object(HttpClient.prototype);

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: mockHttp }
      ]
    })
  });

  afterEach(() => {
    environment.backendHost = originalBackenHost;
    environment.backendPort = originalBackenPort;
  });

  describe('create new game', () => {
    it('should call the game endpoint', (done) => {

      const gameDataFromBackend = {
        gameID: "1234"
      };
      const observableOfRawData = of(gameDataFromBackend)
      td.when(mockHttp.post("http://yokeltoast:5745/heisldice/games", {})).thenReturn(observableOfRawData);

      const service: GameService = TestBed.get(GameService);
      const observable: Observable<GameObjectFromServer> = service.newGame();

      observable.subscribe((result: GameObjectFromServer) => {
        expect(result.gameID).toEqual(gameDataFromBackend.gameID);
        expect(result instanceof GameObjectFromServer).toBe(true);
        done();
      });
    });
  });

  describe('get existing game', () => {
    it('should call the get game endpoint', (done) => {
      const gameID = "1234";
      const gameDataFromBackend = {
        gameID: gameID
      };
      const response = {
        body: gameDataFromBackend,
        headers: new HttpHeaders(),
        status: 200,
        statusText: "OK",
        url: ""
      };
      const observableOfRawData = of(new HttpResponse<GameDataFromBackend>(response))
      td.when(mockHttp.get(`http://yokeltoast:5745/heisldice/games/${gameID}`, { observe: 'response' })).thenReturn(observableOfRawData);

      const service: GameService = TestBed.get(GameService);
      const observable: Observable<GameObjectFromServer> = service.getExistingGame(gameID);

      observable.subscribe((result: GameObjectFromServer) => {
        expect(result.gameID).toEqual(gameDataFromBackend.gameID);
        expect(result instanceof GameObjectFromServer).toBe(true);
        done();
      });
    });
    it('should return null if no game found', (done) => {
      const gameID = "1234";
      const gameDataFromBackend = {
        gameID: gameID
      };
      const response = {
        body: null,
        headers: new HttpHeaders(),
        status: 404,
        statusText: "Not Found",
        url: ""
      };
      const observableOfRawData = of(new HttpResponse<GameDataFromBackend>(response))
      td.when(mockHttp.get(`http://yokeltoast:5745/heisldice/games/${gameID}`, { observe: 'response' })).thenReturn(observableOfRawData);

      const service: GameService = TestBed.get(GameService);
      const observable: Observable<GameObjectFromServer> = service.getExistingGame(gameID);

      observable.subscribe((result: GameObjectFromServer) => {
        expect(result).toBeNull();
        done();
      });
    });
  });
});
