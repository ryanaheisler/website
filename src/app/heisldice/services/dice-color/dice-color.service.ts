import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DiceColorService {
  private _urlRoot: string;

  constructor(private http: HttpClient) {
    this._urlRoot = `http://${environment.backendHost}:${environment.backendPort}`;
  }

  public putDiceColor(gameID: string, playerName: string, diceColor: string) {
    const urlEscapedColor = encodeURIComponent(diceColor);
    const url = `${this._urlRoot}/heisldice/games/${gameID}/players/${playerName}/dice-color/${urlEscapedColor}`;

    this.http.put(url, {}).subscribe({
      next: () => { },
      error: () => { }
    });
  }
}
