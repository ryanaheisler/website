import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import * as td from 'testdouble';
import { DiceColorService } from './dice-color.service';

describe('DiceColorService', () => {
  let mockHttp: HttpClient;
  let originalBackendHost = environment.backendHost;
  let originalBackendPort = environment.backendPort;
  let expectedUrl: string;
  let gameID: string;
  beforeEach(() => {
    environment.backendHost = 'colinjost';
    environment.backendPort = 8417;
    gameID = "9521";

    mockHttp = td.object(HttpClient.prototype);

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: mockHttp }
      ]
    });
  });

  afterEach(() => {
    environment.backendHost = originalBackendHost;
    environment.backendPort = originalBackendPort;
  });

  it('should send a request to change a player\'s dice color and ignore the response', () => {
    const service: DiceColorService = TestBed.get(DiceColorService);
    const diceColor = "#ffffff";
    const urlEscapedDiceColor = "%23ffffff";
    const playerName = "Jammy Jorn";
    const gameID = "G4M3"

    const urlRoot = `http://${environment.backendHost}:${environment.backendPort}`;
    expectedUrl = `${urlRoot}/heisldice/games/${gameID}/players/${playerName}/dice-color/${urlEscapedDiceColor}`

    const observable = td.object(Observable.prototype);
    td.when(mockHttp.put(expectedUrl, td.matchers.anything())).thenReturn(observable);

    service.putDiceColor(gameID, playerName, diceColor);

    const observerCaptor = td.matchers.captor();
    td.verify(observable.subscribe(observerCaptor.capture()));

    expect(observerCaptor.value.next).not.toBeNull();
    expect(observerCaptor.value.error).not.toBeNull();
  });
});
