import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from "rxjs/webSocket";

@Injectable({
  providedIn: 'root'
})
export class WebsocketWrapperService {

  constructor() { }

  websocket(options: any): WebSocketSubject<any> {
    return webSocket(options);
  }
}
