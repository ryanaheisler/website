import { DiceColorService } from './../dice-color/dice-color.service';
import { SessionStorageWrapperService } from './../../../services/session-storage-wrapper/session-storage-wrapper.service';
import { LocalStorageWrapperService } from './../../../services/local-storage-wrapper/local-storage-wrapper.service';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject, ReplaySubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingsStorageService {

  private colorSubject: BehaviorSubject<string>;

  private _gameIDSubject: Subject<string>;
  private get gameIDSubject(): Subject<string> {
    if (!this._gameIDSubject) this._gameIDSubject = new ReplaySubject();
    return this._gameIDSubject;
  };

  private debouncedColorChanged: Subject<string>;
  private _diceColorKey: string = "dice-color";
  private _gameIDKey: string = "game-id";
  private _playerNameKey: string = "player-name";

  constructor(
    private localStorageWrapperService: LocalStorageWrapperService,
    private sessionStorageWrapperService: SessionStorageWrapperService,
    private diceColorService: DiceColorService
  ) {
    this.debouncedColorChanged = new Subject()
    this.debouncedColorChanged.pipe(debounceTime(500)).subscribe((value: string) => {
      this._handleColorChange(value);
    });
  }

  // Dice Color
  public getUserDiceColor(): string {
    return this.localStorageWrapperService.getItem(this._diceColorKey);
  }

  public storeUserDiceColor(value: string) {
    this.debouncedColorChanged.next(value);
  }

  public subscribeToUserDiceColor(): Observable<string> {
    if (!this.colorSubject) {
      const currentColor = this.localStorageWrapperService.getItem(this._diceColorKey);
      this.colorSubject = new BehaviorSubject(currentColor);
    }
    return this.colorSubject;
  }

  private _handleColorChange(value: string) {
    this.localStorageWrapperService.setItem(this._diceColorKey, value);

    const gameID = this.sessionStorageWrapperService.getItem(this._gameIDKey);
    const playerName = this.sessionStorageWrapperService.getItem(this._playerNameKey);

    this.diceColorService.putDiceColor(gameID, playerName, value);

    if (!this.colorSubject) {
      this.colorSubject = new BehaviorSubject(value);
    } else {
      this.colorSubject.next(value);
    }
  }

  // Game ID
  public storeCurrentGameID(gameID: string) {
    this.sessionStorageWrapperService.setItem(this._gameIDKey, gameID);
    this.gameIDSubject.next(gameID);
  }

  public subscribeToCurrentGameID(): Observable<string> {
    const currentGameID = this.sessionStorageWrapperService.getItem(this._gameIDKey);
    this.gameIDSubject.next(currentGameID);
    return this.gameIDSubject;
  }

  public unsetCurrentGameID(): void {
    this.sessionStorageWrapperService.removeItem(this._gameIDKey);
    this.gameIDSubject.next(undefined);
  }

  // Player Name
  public storeCurrentPlayerName(playerName: string) {
    this.sessionStorageWrapperService.setItem(this._playerNameKey, playerName);
  }
  public getCurrentPlayerName(): string {
    return this.sessionStorageWrapperService.getItem(this._playerNameKey);
  }

  public unsetCurrentPlayerName(): void {
    this.sessionStorageWrapperService.removeItem(this._playerNameKey);
  }
}