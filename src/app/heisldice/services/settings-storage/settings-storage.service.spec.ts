import { DiceColorService } from './../dice-color/dice-color.service';
import { SessionStorageWrapperService } from './../../../services/session-storage-wrapper/session-storage-wrapper.service';
import { TestHelper } from './../../../../test/test-helper';
import { LocalStorageWrapperService } from './../../../services/local-storage-wrapper/local-storage-wrapper.service';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';

import { SettingsStorageService } from './settings-storage.service';
import * as td from 'testdouble';

describe('SettingsStorageService', () => {
  let service: SettingsStorageService;
  let diceColor: string;
  let gameID: string;
  let playerName: string;
  let mockLocalStorageWrapper: LocalStorageWrapperService;
  let mockSessionStorageWrapper: SessionStorageWrapperService;
  let mockDiceColorService: DiceColorService;

  beforeEach(() => {
    mockLocalStorageWrapper = td.object(LocalStorageWrapperService.prototype);
    diceColor = "blue";
    td.when(mockLocalStorageWrapper.getItem('dice-color')).thenReturn(diceColor);

    mockSessionStorageWrapper = td.object(SessionStorageWrapperService.prototype);
    gameID = "GAMM";
    playerName = "Krampus";

    mockDiceColorService = td.object(DiceColorService.prototype);

    TestBed.configureTestingModule({
      providers: [
        { provide: LocalStorageWrapperService, useValue: mockLocalStorageWrapper },
        { provide: SessionStorageWrapperService, useValue: mockSessionStorageWrapper },
        { provide: DiceColorService, useValue: mockDiceColorService },
      ]
    });

    service = TestBed.get(SettingsStorageService);
  });

  describe('dice color', () => {
    it('should store a dice color in local storage after debounce time', fakeAsync(() => {
      service.storeUserDiceColor("anything");

      tick(499);

      td.verify(mockLocalStorageWrapper.setItem('dice-color', "anything"), { times: 0 });

      tick(1);

      td.verify(mockLocalStorageWrapper.setItem('dice-color', "anything"));
      TestHelper.expectNothing();
    }));
    it('should send the dice color to the server when it changes after debounce time', fakeAsync(() => {
      const gameID = 'the game';
      const playerName = 'hoosey';
      const diceColor = "anything";

      td.when(mockSessionStorageWrapper.getItem('game-id')).thenReturn(gameID);
      td.when(mockSessionStorageWrapper.getItem('player-name')).thenReturn(playerName);

      service.storeUserDiceColor(diceColor);

      tick(499);

      td.verify(mockDiceColorService.putDiceColor(gameID, playerName, diceColor), { times: 0 });

      tick(1);

      td.verify(mockDiceColorService.putDiceColor(gameID, playerName, diceColor));
      TestHelper.expectNothing();
    }));
    it('should get the dice color in local storage and send to subscribers after debounce time', fakeAsync(() => {
      let color = undefined;
      service.subscribeToUserDiceColor().subscribe(value => color = value);
      expect(color).toEqual(diceColor);

      const newColor = "greeeeeeen";
      service.storeUserDiceColor(newColor);

      tick(500);

      expect(color).toEqual(newColor);
    }));
    it('should get the user\'s dice color once', () => {
      const color = "laller";
      td.when(mockLocalStorageWrapper.getItem("dice-color")).thenReturn(color);

      expect(service.getUserDiceColor()).toEqual(color);
    });
  });
  describe('game ID', () => {
    it('should store game ID in Session Storage', () => {
      service.storeCurrentGameID("anything");
      td.verify(mockSessionStorageWrapper.setItem('game-id', "anything"));
      TestHelper.expectNothing();
    });
    it('should get the game ID in session storage and send to subscribers', () => {
      td.when(mockSessionStorageWrapper.getItem("game-id")).thenReturn(gameID);

      let nextGameID: string;

      service.subscribeToCurrentGameID().subscribe(value => nextGameID = value);
      expect(nextGameID).toEqual(gameID);

      const newGameID = "7772";
      service.storeCurrentGameID(newGameID);

      expect(nextGameID).toEqual(newGameID);
    });
    it('should clear the current game ID and send undefined to subscribers', () => {
      let nextGameID: string = "not undefined";
      td.when(mockSessionStorageWrapper.getItem('game-id')).thenReturn(gameID);
      service.subscribeToCurrentGameID().subscribe(value => nextGameID = value);

      service.unsetCurrentGameID();
      td.verify(mockSessionStorageWrapper.removeItem('game-id'));
      expect(nextGameID).toBeUndefined();
    });
  });
  describe('player name', () => {
    it('should store player name in Session Storage', () => {
      service.storeCurrentPlayerName("anything");
      td.verify(mockSessionStorageWrapper.setItem('player-name', "anything"));
      TestHelper.expectNothing();
    });
    it('should get the player name in session storage ', () => {
      td.when(mockSessionStorageWrapper.getItem("player-name")).thenReturn(playerName);

      expect(service.getCurrentPlayerName()).toEqual(playerName);
    });
    it('should clear the current player name', () => {
      service.unsetCurrentPlayerName();
      td.verify(mockSessionStorageWrapper.removeItem('player-name'));
      TestHelper.expectNothing();
    });
  });
});
