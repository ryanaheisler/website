import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  public getTimeBetweenDiceDisplaysInMillis(): number {
    return 150;
  }
  constructor() { }
}
