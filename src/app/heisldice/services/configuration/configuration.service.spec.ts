import { TestBed } from '@angular/core/testing';
import { ConfigurationService } from './configuration.service';


describe('ConfigurationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should have time between dice displays', () => {
    const service: ConfigurationService = TestBed.get(ConfigurationService);
    expect(service.getTimeBetweenDiceDisplaysInMillis()).toEqual(150);
  });
});
