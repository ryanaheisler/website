import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { TestHelper } from 'src/test/test-helper';
import * as td from 'testdouble';
import { environment } from './../../../../environments/environment';
import { Category } from './../../models/category.model';
import { Player } from './../../models/player.model';
import { ScoreService } from './score.service';

describe('ScoreService', () => {

  let service: ScoreService;
  let playerName: string;
  let category: Category;
  let dice: number[];
  let mockHttp: HttpClient;
  let mockObservable: Observable<Player>;
  let originalBackendHost = environment.backendHost;
  let originalBackendPort = environment.backendPort;
  let expectedURL, expectedPostBody;
  let gameID: string;

  beforeEach(() => {
    environment.backendHost = 'shmoakleghost'
    environment.backendPort = 3214;
    gameID = "0029";

    playerName = "Ryan";
    category = new Category("Fanch");
    dice = [1, 2, 6, 5, 4]

    expectedURL = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games/${gameID}/players/${playerName}/score`;
    mockObservable = td.object(Observable.prototype);
    mockHttp = td.object(HttpClient.prototype);
    expectedPostBody = { category: category.description, dice: dice };
    td.when(mockHttp.post(expectedURL, expectedPostBody)).thenReturn(mockObservable);

    TestBed.configureTestingModule({
      providers: [
        ScoreService,
        { provide: HttpClient, useValue: mockHttp }
      ]
    });
  });

  afterEach(() => {
    environment.backendHost = originalBackendHost;
    environment.backendPort = originalBackendPort;
  });

  it('should send correct request and return player observable', () => {
    service = TestBed.get(ScoreService);

    expect(service.setScore(TestHelper.buildPlayer({name: playerName}), gameID, category, dice)).toBe(mockObservable);
  });
});
