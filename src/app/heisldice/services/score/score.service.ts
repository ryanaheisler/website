import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Player } from '../../models/player.model';
import { environment } from './../../../../environments/environment';
import { Category } from './../../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  constructor(private httpClient: HttpClient) { }

  setScore(player: Player, gameID: string, category: Category, dice: number[]): Observable<Player> {
    const url = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games/${gameID}/players/${player.name}/score`
    return this.httpClient.post<Player>(url, { category: category.description, dice: dice });
  }
}
