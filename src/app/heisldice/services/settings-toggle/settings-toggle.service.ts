import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsToggleService {

  private _subject: Subject<boolean>;

  constructor() {
    this._subject = new Subject();
  }

  public subscribe(observer): Subscription {
    return this._subject.subscribe(observer);
  }

  public settingsOpened() {
    this._subject.next(true);
  }

  public settingsClosed() {
    this._subject.next(false);
  }
}
