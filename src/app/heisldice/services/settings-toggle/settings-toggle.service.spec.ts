import { Subscription } from 'rxjs';
import { TestBed } from '@angular/core/testing';

import { SettingsToggleService } from './settings-toggle.service';

describe('SettingsToggleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should have a method to notify subscribers that settings was toggled', () => {
    const service: SettingsToggleService = TestBed.get(SettingsToggleService);

    let settingsIsOpen = false;
    const subscription = service.subscribe((value: boolean) => {
      settingsIsOpen = value;
    });

    expect(subscription instanceof Subscription).toBe(true);

    service.settingsOpened();
    expect(settingsIsOpen).toBe(true);

    service.settingsOpened();
    expect(settingsIsOpen).toBe(true);

    service.settingsClosed();
    expect(settingsIsOpen).toBe(false);

    service.settingsClosed();
    expect(settingsIsOpen).toBe(false);
  });
});
