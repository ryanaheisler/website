import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IsMobileDeviceService {

  private subject: BehaviorSubject<boolean>;
  constructor() {
    this.subject = new BehaviorSubject(false);
    document.addEventListener('touchstart', this.handler, true);
  }

  private handler = () => {
    document.removeEventListener('touchstart', this.handler, true);
    this.subject.next(true);
  }

  isMobileDevice(): BehaviorSubject<boolean> {
    return this.subject;
  }
}
