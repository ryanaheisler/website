import { TestBed } from '@angular/core/testing';

import { IsMobileDeviceService } from './is-mobile-device.service';
import * as td from 'testdouble';

describe('IsMobileDeviceService', () => {
  let originalAddEventListener;
  let originalRemoveEventListener;
  let service: IsMobileDeviceService;
  beforeEach(() => {
    originalAddEventListener = document.addEventListener;
    originalRemoveEventListener = document.removeEventListener;

    //@ts-ignore
    document.addEventListener = td.function('addEventListener');
    //@ts-ignore
    document.removeEventListener = td.function('removeEventListener');

    TestBed.configureTestingModule({});
    service = TestBed.get(IsMobileDeviceService);
  });

  afterEach(() => {
    document.addEventListener = originalAddEventListener;
    document.removeEventListener = originalRemoveEventListener;
  });

  it('should check if this is a mobile device by listening for the touchstart event', () => {
    const callbackCaptor = td.matchers.captor();
    td.verify(document.addEventListener('touchstart', callbackCaptor.capture(), true));

    let isMobileDevice = false;
    service.isMobileDevice().subscribe((value: boolean) => {
      isMobileDevice = value;
    });

    expect(isMobileDevice).toBe(false);

    callbackCaptor.value();
    td.verify(document.removeEventListener('touchstart', callbackCaptor.value, true));
    expect(isMobileDevice).toBe(true);
  });
});
