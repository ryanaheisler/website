import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as td from 'testdouble';
import { ScorecardFactory } from '../../models/scorecard-factory';
import { Player } from './../../models/player.model';
import { Scorecard } from './../../models/scorecard.model';
import { DiceRollDataFromServer, DiceRollRequestBody, DiceService } from './dice.service';
import { Category } from '../../models/category.model';
import { TestHelper } from 'src/test/test-helper';

describe('DiceService', () => {
  let mockObservable: Observable<Object>;
  let mockHttp: HttpClient;
  let mockScorecardFactory: ScorecardFactory;
  let reservedDice: number[];
  let playerToSend: Player
  let scorecardFromAPI: Scorecard;
  let expectedScorecard: Scorecard;
  let diceFromAPI: number[];
  let playerFromAPI: Player;
  let requestBody: DiceRollRequestBody
  let gameID: string;

  let originalBackendHost = environment.backendHost;
  let originalBackendPort = environment.backendPort;
  beforeEach(() => {
    environment.backendHost = 'shmoakleghost'
    environment.backendPort = 3214;
    scorecardFromAPI = new Scorecard();
    scorecardFromAPI.categories.push(new Category('Aces', 2));
    diceFromAPI = [1, 2, 3, 4, 5];
    playerToSend = TestHelper.constructPlayer("Ryan", 3, 12);
    playerFromAPI = TestHelper.constructPlayer("Ryan", 2, 12);
    gameID = "2232";

    mockObservable = of({
      dice: diceFromAPI,
      scorecard: scorecardFromAPI,
      player: playerFromAPI
    });

    mockHttp = td.object(HttpClient.prototype);
    reservedDice = [3, 0, 1, 1, 0];
    requestBody = {
      reservedDice: reservedDice
    };

    mockScorecardFactory = td.object(ScorecardFactory.prototype);
    expectedScorecard = new Scorecard();
    td.when(mockScorecardFactory.build(scorecardFromAPI)).thenReturn(expectedScorecard);
  });

  afterEach(() => {
    environment.backendHost = originalBackendHost;
    environment.backendPort = originalBackendPort;
  });

  describe('roll', () => {
    it('should call roll endpoint on server', () => {
      const service: DiceService = new DiceService(mockHttp, mockScorecardFactory);

      const response = {
        body: {
          dice: diceFromAPI,
          scorecard: scorecardFromAPI,
          player: playerFromAPI
        },
        headers: new HttpHeaders(),
        status: 200,
        statusText: "OK",
        url: ""
      };
      const observableOfRawData = of(new HttpResponse<DiceRollDataFromServer>(response))
      td.when(mockHttp.post(`http://shmoakleghost:3214/heisldice/games/${gameID}/players/${playerToSend.name}/dice`, requestBody, { observe: 'response' })).thenReturn(observableOfRawData);

      let results: Observable<DiceRollDataFromServer> = service.roll(reservedDice, playerToSend, gameID);
      results.subscribe((result) => {
        expect(result.dice).toEqual(diceFromAPI);
        expect(result.scorecard).toBe(expectedScorecard);
        expect(result.player).toBe(playerFromAPI);
      });
    });
    it('should return null if server could not be reached', (done) => {
      const service: DiceService = new DiceService(mockHttp, mockScorecardFactory);

      const response = {
        body: null,
        headers: new HttpHeaders(),
        status: 500,
        statusText: "ERROR",
        url: ""
      };
      const observableOfRawData = of(new HttpResponse<DiceRollDataFromServer>(response))
      td.when(mockHttp.post(td.matchers.anything(), td.matchers.anything(), { observe: 'response' })).thenReturn(observableOfRawData);

      let results: Observable<DiceRollDataFromServer> = service.roll(reservedDice, playerToSend, gameID);
      results.subscribe((result) => {
        expect(result).toBeNull();
        done()
      });
    });
  });
});
