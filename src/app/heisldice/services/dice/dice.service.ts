import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Player } from '../../models/player.model';
import { Scorecard } from '../../models/scorecard.model';
import { ScorecardFactory } from './../../models/scorecard-factory';

@Injectable()
export class DiceService {

  constructor(private http: HttpClient, private scorecardFactory: ScorecardFactory) { }

  roll(reservedDice: number[], player: Player, gameID: string): Observable<DiceRollDataFromServer> {
    const backendURL = `http://${environment.backendHost}:${environment.backendPort}/heisldice/games/${gameID}/players/${player.name}/dice`;
    const requestBody: DiceRollRequestBody = {
      reservedDice: reservedDice
    };
    return this.http.post<DiceRollDataFromServer>(backendURL, requestBody, { observe: 'response' }).pipe(
      map((response: HttpResponse<DiceRollDataFromServer>) => {
        return {
          dice: response.body.dice,
          scorecard: this.scorecardFactory.build(response.body.scorecard),
          player: response.body.player
        }
      }),
      catchError((error: any) => {
        return of(null);
      }));
  }
}

export interface DiceRollRequestBody {
  reservedDice: number[]
}

export interface DiceRollDataFromServer {
  dice: number[],
  scorecard: Scorecard,
  player: Player
}
