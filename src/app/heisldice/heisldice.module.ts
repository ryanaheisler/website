import { DieFilledSixComponent } from './components/die-icons/die-filled-six/die-filled-six.component';
import { DieFilledFiveComponent } from './components/die-icons/die-filled-five/die-filled-five.component';
import { DieFilledFourComponent } from './components/die-icons/die-filled-four/die-filled-four.component';
import { DieFilledThreeComponent } from './components/die-icons/die-filled-three/die-filled-three.component';
import { DieFilledTwoComponent } from './components/die-icons/die-filled-two/die-filled-two.component';
import { DieFilledOneComponent } from './components/die-icons/die-filled-one/die-filled-one.component';
import { DieBorderSixComponent } from './components/die-icons/die-border-six/die-border-six.component';
import { DieBorderFiveComponent } from './components/die-icons/die-border-five/die-border-five.component';
import { DieBorderFourComponent } from './components/die-icons/die-border-four/die-border-four.component';
import { DieBorderThreeComponent } from './components/die-icons/die-border-three/die-border-three.component';
import { DieBorderTwoComponent } from './components/die-icons/die-border-two/die-border-two.component';
import { DieBorderOneComponent } from './components/die-icons/die-border-one/die-border-one.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { DiceComponent } from './components/dice/dice.component';
import { HeisldiceContainerComponent } from './components/heisldice-container/heisldice-container.component';
import { HeisldiceLandingPageComponent } from './components/heisldice-landing-page/heisldice-landing-page.component';
import { HeisldicePageComponent } from './components/heisldice-page/heisldice-page.component';
import { OpponentDiceComponent } from './components/opponent-dice/opponent-dice.component';
import { AllDiceComponent } from './components/all-dice/all-dice.component';
import { ScorecardComponent } from './components/scorecard/scorecard.component';
import { StartGameModalComponent } from './components/start-game-modal/start-game-modal.component';
import { ScorecardResolver } from './resolvers/scorecard/scorecard-resolver';
import { DiceService } from './services/dice/dice.service';
import { PlayerService } from './services/player/player.service';
import { ScorecardService } from './services/scorecard/scorecard.service';
import { DisconnectedOverlayComponent } from './components/disconnected-overlay/disconnected-overlay.component';
import { DieSvgAnchorDirective } from './directives/die-svg-anchor.directive';
import { DieSvgIconComponent } from './components/die-svg-icon/die-svg-icon.component';
import { HeisldiceHeaderComponent } from './components/heisldice-header/heisldice-header.component';
import { HeisldiceSettingsComponent } from './components/heisldice-settings/heisldice-settings.component';
import { DiceColorSettingsComponent } from './components/dice-color-settings/dice-color-settings.component';
import { FallbackDiceColorSettingsComponent } from './components/dice-color-settings/fallback-dice-color-settings/fallback-dice-color-settings.component';
import { PreviewDiceComponent } from './components/dice-color-settings/preview-dice/preview-dice.component';

export const HEISLDICE_ROUTES = [
  {
    path: 'heisldice',
    component: HeisldiceContainerComponent,
    children: [
      {
        path: '',
        component: HeisldiceLandingPageComponent
      },
      {
        path: ':gameID',
        component: HeisldicePageComponent,
        resolve: { scorecard: ScorecardResolver }
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(HEISLDICE_ROUTES),
    FontAwesomeModule,
    FormsModule,
  ],
  declarations: [
    HeisldicePageComponent,
    DiceComponent,
    ScorecardComponent,
    HeisldiceLandingPageComponent,
    HeisldiceContainerComponent,
    StartGameModalComponent,
    OpponentDiceComponent,
    AllDiceComponent,
    DisconnectedOverlayComponent,
    DieBorderOneComponent,
    DieBorderTwoComponent,
    DieBorderThreeComponent,
    DieBorderFourComponent,
    DieBorderFiveComponent,
    DieBorderSixComponent,
    DieFilledOneComponent,
    DieFilledTwoComponent,
    DieFilledThreeComponent,
    DieFilledFourComponent,
    DieFilledFiveComponent,
    DieFilledSixComponent,
    DieSvgAnchorDirective,
    DieSvgIconComponent,
    HeisldiceHeaderComponent,
    HeisldiceSettingsComponent,
    DiceColorSettingsComponent,
    FallbackDiceColorSettingsComponent,
    PreviewDiceComponent
  ],
  providers: [DiceService, ScorecardService, PlayerService, CookieService, ToastrService],
  entryComponents: [
    StartGameModalComponent,
    DieBorderOneComponent,
    DieBorderTwoComponent,
    DieBorderThreeComponent,
    DieBorderFourComponent,
    DieBorderFiveComponent,
    DieBorderSixComponent,
    DieFilledOneComponent,
    DieFilledTwoComponent,
    DieFilledThreeComponent,
    DieFilledFourComponent,
    DieFilledFiveComponent,
    DieFilledSixComponent,
  ],
})
export class HeisldiceModule { }
