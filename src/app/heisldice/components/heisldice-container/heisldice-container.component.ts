import { Subscription } from 'rxjs';
import { SettingsToggleService } from './../../services/settings-toggle/settings-toggle.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'heisldice-container',
  templateUrl: './heisldice-container.component.html',
  styleUrls: ['./heisldice-container.component.css']
})
export class HeisldiceContainerComponent implements OnInit, OnDestroy {

  private _subscription: Subscription;
  constructor(private settingsToggleService: SettingsToggleService) { }

  ngOnInit() {
    this._subscription = this.settingsToggleService.subscribe((settingsIsOpen: boolean) => {
      const container: HTMLElement = document.querySelector("#router-outlet-container");
      if (settingsIsOpen) {
        container.style.display = "none";
      } else {
        container.style.display = "";
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
