import { SettingsToggleService } from './../../services/settings-toggle/settings-toggle.service';
import { HeisldiceContainerComponent } from './heisldice-container.component';
import { Component } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from '@angular/platform-browser';

describe('HeisldiceContainerComponent', () => {
    let component: HeisldiceContainerComponent;
    let fixture: ComponentFixture<HeisldiceContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HeisldiceContainerComponent, MockHeaderComponent, MockRouterOutlet],
            providers: [
                SettingsToggleService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeisldiceContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should display the header and router outlet', () => {
        expect(fixture.debugElement.query(By.directive(MockHeaderComponent))).not.toBeNull();
        expect(fixture.debugElement.query(By.directive(MockRouterOutlet))).not.toBeNull();
    });
    it('should show/hide the router outlet when the settings are toggled', () => {
        const service: SettingsToggleService = TestBed.get(SettingsToggleService);

        service.settingsOpened();
        fixture.detectChanges();
        let outlet = fixture.nativeElement.querySelector('#router-outlet-container');
        expect(outlet.style.display).toEqual("none");

        service.settingsClosed();
        fixture.detectChanges();
        outlet = fixture.nativeElement.querySelector('#router-outlet-container');
        expect(outlet.style.display).toEqual("");
    });
});

@Component({
    selector: 'router-outlet',
    template: ''
})
class MockRouterOutlet { }

@Component({
    selector: 'heisldice-header',
    template: ''
})
class MockHeaderComponent { }