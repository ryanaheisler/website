import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TestHelper } from 'src/test/test-helper';
import * as td from 'testdouble';
import { DieModel } from '../../models/die.model';
import { MockDieSVGIconComponent } from '../dice/dice.component.spec';
import { ConfigurationService } from './../../services/configuration/configuration.service';
import { OpponentDiceComponent } from './opponent-dice.component';

describe('OpponentDiceComponent', () => {
  let component: OpponentDiceComponent;
  let fixture: ComponentFixture<OpponentDiceComponent>;
  let mockConfigurationService: ConfigurationService;

  beforeEach(async(() => {

    mockConfigurationService = td.object(ConfigurationService.prototype);
    td.when(mockConfigurationService.getTimeBetweenDiceDisplaysInMillis()).thenReturn(0);

    TestBed.configureTestingModule({
      declarations: [OpponentDiceComponent, MockDieSVGIconComponent],
      providers: [
        { provide: ConfigurationService, useValue: mockConfigurationService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpponentDiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Template', () => {
    it('should have the opponent\'s name', () => {
      component.player = TestHelper.constructPlayer('Conrad');
      component.color = "red";
      fixture.detectChanges();

      const name: HTMLSpanElement = fixture.debugElement.nativeElement.querySelector('span');

      expect(name.innerText).toEqual("Conrad");
    });
    it('should have 5 die components, one for each number in dice', () => {
      const dice = [5, 1, 5, 1, 3];
      const player = TestHelper.constructPlayer('Ryan', 1, 2, dice);
      component.player = player;
      component.color = "#c1ffd0"
      fixture.detectChanges();

      const dieSelectors = fixture.debugElement.queryAll(By.css('die-svg-icon'));

      expect(dieSelectors.length).toEqual(5);

      dieSelectors.forEach((selector, index) => {
        let die: MockDieSVGIconComponent = selector.componentInstance;
        expect(die.die).toEqual(new DieModel(dice[index], true));
        expect(die.canReserve).toEqual(false);
        expect(die.color).toEqual(component.color);
      });
    });
    it('should have a colorful border if it is the player\'s turn', () => {
      const diceValues = [5, 1, 5, 1, 3];
      const color = "#1f618d";
      const player = TestHelper.constructPlayer('Ryan', 1, 2, diceValues, true);
      player.diceColor = undefined;
      component.color = color;
      component.player = player;

      fixture.detectChanges();

      const container = fixture.debugElement.query(By.css('.container'));
      expect(container.styles["border-color"]).toEqual(color);
      expect(container.styles["border-width"]).toEqual('2px');
    });
    it('should use the player\'s chosen color for dice and border if it exists', () => {
      const dice = [5, 1, 5, 1, 3];
      const defaultColor = "#c1ffd0";
      const colorFromServer = "#ffffff";
      const player = TestHelper.constructPlayer('Ryan', 1, 2, dice, true, null, colorFromServer);
      component.color = defaultColor
      fixture.detectChanges();

      component.player = player;
      fixture.detectChanges();

      const dieSelectors = fixture.debugElement.queryAll(By.css('die-svg-icon'));

      dieSelectors.forEach((selector, index) => {
        let die: MockDieSVGIconComponent = selector.componentInstance;
        expect(die.color).toEqual(colorFromServer);
      });

      const container = fixture.debugElement.query(By.css('.container'));
      expect(container.styles["border-color"]).toEqual(colorFromServer);
      expect(container.styles["border-width"]).toEqual('2px');
    });
    it('should NOT have a border if it is NOT the player\'s turn', () => {
      const diceValues = [5, 1, 5, 1, 3];
      const player = TestHelper.constructPlayer('Ryan', 1, 2, diceValues, false);
      component.color = "1f618d";
      component.player = player;
      fixture.detectChanges();

      const container = fixture.debugElement.query(By.css('.container'));
      expect(container.styles["border-color"]).toBeNull();
      expect(container.styles["border-width"]).toBeNull();
    });
  });
});