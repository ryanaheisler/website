import { DieModel } from './../../models/die.model';
import { Component, Input, OnInit } from '@angular/core';
import { Player } from './../../models/player.model';
import { ConfigurationService } from './../../services/configuration/configuration.service';

@Component({
  selector: 'opponent-dice',
  templateUrl: './opponent-dice.component.html',
  styleUrls: ['./opponent-dice.component.css']
})
export class OpponentDiceComponent implements OnInit {

  public _player: Player;
  public dieModels: DieModel[];
  @Input()
  public set player(value: Player) {
    this.dieModels = value.previousDice.map(pips => new DieModel(pips, true));
    const dice = value.previousDice.slice(1);
    if (value.myTurn && value.rollsRemaining !== 3) {
      value.previousDice = [value.previousDice[0]]
      dice.forEach((die, index) => {
        setTimeout(() => {
          value.previousDice.push(die);
        }, index * this.configuration.getTimeBetweenDiceDisplaysInMillis());
      });
    }
    this._player = value;
  }
  public get player(): Player {
    return this._player;
  };

  private _color: string;
  @Input()
  public get color(): string {
    return this._player.diceColor || this._color;
  };
  public set color(value: string) {
    this._color = value;
  }

  constructor(private configuration: ConfigurationService) { }

  ngOnInit() {
  }

}
