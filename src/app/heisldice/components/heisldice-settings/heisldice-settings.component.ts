import { Component, OnInit } from '@angular/core';
import { ColorInputSupportService } from '../../services/color-input-support/color-input-support.service';

@Component({
  selector: 'heisldice-settings',
  templateUrl: './heisldice-settings.component.html',
  styleUrls: ['./heisldice-settings.component.css']
})
export class HeisldiceSettingsComponent implements OnInit {
  public colorInputSupported:boolean

  constructor(private colorInputSupportService: ColorInputSupportService) { }

  ngOnInit() {
    this.colorInputSupported = this.colorInputSupportService.isColorInputSupported();
  }
}
