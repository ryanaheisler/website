import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ColorInputSupportService } from '../../services/color-input-support/color-input-support.service';
import * as td from 'testdouble';
import { HeisldiceSettingsComponent } from './heisldice-settings.component';

describe('HeisldiceSettingsComponent', () => {
  let component: HeisldiceSettingsComponent;
  let fixture: ComponentFixture<HeisldiceSettingsComponent>;
  let mockBrowserSupportsColorInputService: ColorInputSupportService

  beforeEach(async(() => {
    mockBrowserSupportsColorInputService = td.object(ColorInputSupportService.prototype);

    TestBed.configureTestingModule({
      declarations: [
        HeisldiceSettingsComponent,
        MockDiceColorSettings,
        MockFallbackDiceColorSettings
      ],
      providers: [
        { provide: ColorInputSupportService, useValue: mockBrowserSupportsColorInputService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeisldiceSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should show a heading', () => {
    const heading = fixture.nativeElement.querySelector('h2');
    expect(heading.innerText).toEqual('Settings');
  });
  it('should show a dice color settings component if the browser supports the color input', () => {
    td.when(mockBrowserSupportsColorInputService.isColorInputSupported()).thenReturn(true);
    component.ngOnInit();
    fixture.detectChanges();
    
    const colorSettings = fixture.debugElement.query(By.directive(MockDiceColorSettings));
    const fallbackcolorSettings = fixture.debugElement.query(By.directive(MockFallbackDiceColorSettings));
    expect(colorSettings).toBeTruthy();
    expect(fallbackcolorSettings).toBeFalsy();
  });
  it('should show the fallback dice color settings component if the browser doesn\'t support the color input', () => {
    td.when(mockBrowserSupportsColorInputService.isColorInputSupported()).thenReturn(false);
    component.ngOnInit();
    fixture.detectChanges();

    const colorSettings = fixture.debugElement.query(By.directive(MockDiceColorSettings));
    const fallbackcolorSettings = fixture.debugElement.query(By.directive(MockFallbackDiceColorSettings));
    expect(colorSettings).toBeFalsy();
    expect(fallbackcolorSettings).toBeTruthy();
  });
});

@Component({
  selector: 'dice-color-settings',
  template: ''
})
class MockDiceColorSettings {
}
@Component({
  selector: 'fallback-dice-color-settings',
  template: ''
})
class MockFallbackDiceColorSettings {
}