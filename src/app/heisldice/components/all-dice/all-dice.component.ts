import { Scorecard } from '../../models/scorecard.model';
import { Player } from '../../models/player.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'all-dice',
  templateUrl: './all-dice.component.html',
  styleUrls: ['./all-dice.component.css']
})
export class AllDiceComponent implements OnInit {

  private opponentColors = [
    '#922B21',
    '#1f618d',
    '#1e8449',
    '#af601a',
    '#b7950b',
    '#148f77',
    '#cb4335',
    '#8e44ad',
    '#2e86c1',
    '#16a085',
    '#28b463',
    '#f39c12',
    '#ba4a00',
  ]

  public leftColors: String[] = [];
  public rightColors: String[] = [];

  public leftColumn: Player[] = [];
  public rightColumn: Player[] = [];
  @Input()
  public set players(value: Player[]) {
    const updatedPlayers = value.filter((player) => {
      return player.name !== this.mainPlayer.name;
    });

    this.leftColumn = [];
    this.leftColors = [];
    this.rightColumn = [];
    this.rightColors = [];
    updatedPlayers.forEach((player, index) => {
      if (index % 2 === 0) {
        this.leftColumn.push(player);
        this.leftColors.push(this.opponentColors[index % this.opponentColors.length]);
      } else {
        this.rightColumn.push(player);
        this.rightColors.push(this.opponentColors[index % this.opponentColors.length]);
      }
    });
  }

  @Input()
  mainPlayer: Player

  @Input()
  gameID: string

  @Output()
  playerUpdated = new EventEmitter<Player>();
  @Output()
  potentialScorecardUpdated = new EventEmitter<Scorecard>();
  @Output()
  diceUpdated = new EventEmitter<number[]>();

  public emitPlayer(player: Player) {
    this.playerUpdated.emit(player);
  }

  public emitPotentialScorecard(scorecard: Scorecard) {
    this.potentialScorecardUpdated.emit(scorecard);
  }

  public emitDice(dice: number[]) {
    this.diceUpdated.emit(dice);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
