import { Scorecard } from '../../models/scorecard.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { OpponentDiceComponent } from '../opponent-dice/opponent-dice.component';
import { Player } from '../../models/player.model';
import { AllDiceComponent } from './all-dice.component';
import { TestHelper } from 'src/test/test-helper';


describe('AllDiceComponent', () => {
  let component: AllDiceComponent;
  let fixture: ComponentFixture<AllDiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AllDiceComponent, MockOpponentDiceComponent, MockDiceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllDiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Component', () => {
    it('should split players into two columns and omit player of this client', () => {
      const players = [
        TestHelper.constructPlayer("Jamila", 2, 12, [1, 5, 5, 5, 5]),
        TestHelper.constructPlayer("Oum Karim", 2, 12, [4, 3, 1, 5, 6]),
        TestHelper.constructPlayer("Hiba", 2, 12, [6, 5, 4, 2, 1]),
        TestHelper.constructPlayer("Amna", 2, 12, [3, 5, 4, 2, 1]),
        TestHelper.constructPlayer("Marwa", 2, 12, [1, 5, 4, 2, 1]),
        TestHelper.constructPlayer("Roula", 2, 12, [6, 4, 4, 2, 1]),
      ];
      component.mainPlayer = players[3]
      component.players = players;

      expect(component.leftColumn).toEqual([players[0], players[2], players[5]]);
      expect(component.rightColumn).toEqual([players[1], players[4]]);
    });

    it('should pass colors to dice components in a loop', () => {
      const expectedColors = [
        '#922B21',
        '#1f618d',
        '#1e8449',
        '#af601a',
        '#b7950b',
        '#148f77',
        '#cb4335',
        '#8e44ad',
        '#2e86c1',
        '#16a085',
        '#28b463',
        '#f39c12',
        '#ba4a00',
      ];

      const players = [
        TestHelper.constructPlayer("Jamila", 2, 12, [1, 5, 5, 5, 5]),
        TestHelper.constructPlayer("Oum Karim", 2, 12, [4, 3, 1, 5, 6]),
        TestHelper.constructPlayer("Hiba", 2, 12, [6, 5, 4, 2, 1]),
        TestHelper.constructPlayer("Amna", 2, 12, [3, 5, 4, 2, 1]),
        TestHelper.constructPlayer("Marwa", 2, 12, [1, 5, 4, 2, 1]),
        TestHelper.constructPlayer("Roula", 2, 12, [6, 4, 4, 2, 1]),
        TestHelper.constructPlayer("Moula", 2, 12, [7, 4, 4, 2, 1]),
        TestHelper.constructPlayer("Loula", 2, 12, [8, 4, 4, 2, 1]),
        TestHelper.constructPlayer("Toula", 2, 12, [9, 4, 4, 2, 1]),
        TestHelper.constructPlayer("Poula", 2, 12, [1, 4, 4, 2, 1]),
        TestHelper.constructPlayer("Woula", 2, 12, [6, 3, 4, 2, 1]),
        TestHelper.constructPlayer("Xoula", 2, 12, [6, 4, 5, 2, 1]),
        TestHelper.constructPlayer("Soula", 2, 12, [6, 4, 6, 2, 1]),
        TestHelper.constructPlayer("Noula", 2, 12, [6, 4, 1, 2, 1]),
        TestHelper.constructPlayer("Goula", 2, 12, [6, 4, 1, 2, 1]),
      ];
      component.mainPlayer = players[4];
      component.players = players;
      fixture.detectChanges();

      const diceElements = fixture.debugElement.queryAll(By.css('opponent-dice'));

      expect(diceElements[0].componentInstance.color).toEqual(expectedColors[0]);
      expect(diceElements[1].componentInstance.color).toEqual(expectedColors[1]);
      expect(diceElements[2].componentInstance.color).toEqual(expectedColors[2]);
      expect(diceElements[3].componentInstance.color).toEqual(expectedColors[3]);
      expect(diceElements[4].componentInstance.color).toEqual(expectedColors[4]);
      expect(diceElements[5].componentInstance.color).toEqual(expectedColors[5]);
      expect(diceElements[6].componentInstance.color).toEqual(expectedColors[6]);
      expect(diceElements[7].componentInstance.color).toEqual(expectedColors[7]);
      expect(diceElements[8].componentInstance.color).toEqual(expectedColors[8]);
      expect(diceElements[9].componentInstance.color).toEqual(expectedColors[9]);
      expect(diceElements[10].componentInstance.color).toEqual(expectedColors[10]);
      expect(diceElements[11].componentInstance.color).toEqual(expectedColors[11]);
      expect(diceElements[12].componentInstance.color).toEqual(expectedColors[12]);
      expect(diceElements[13].componentInstance.color).toEqual(expectedColors[0]);
    });

    it('should pass a player handler to the main player dice', (done) => {
      const player = TestHelper.constructPlayer("Jamila", 2, 12, [1, 5, 5, 5, 5])
      component.playerUpdated.subscribe((emittedPlayer: Player) => {
        expect(emittedPlayer).toBe(player);
        done();
      })

      fixture.detectChanges();

      const playerDice = fixture.debugElement.query(By.css('dice'));

      playerDice.componentInstance.playerUpdated.emit(player);
    });

    it('should pass a potential scorecard handler to the main player dice', (done) => {
      const scorecard = new Scorecard();
      component.potentialScorecardUpdated.subscribe((emittedScorecard: Scorecard) => {
        expect(scorecard).toBe(emittedScorecard);
        done();
      })

      fixture.detectChanges();

      const playerDice = fixture.debugElement.query(By.css('dice'));

      playerDice.componentInstance.potentialScorecardUpdated.emit(scorecard);
    });

    it('should pass a dice handler to the main player dice', (done) => {
      const dice = [1, 2, 3, 4, 5]
      component.diceUpdated.subscribe((emittedDice: number[]) => {
        expect(emittedDice).toBe(dice);
        done();
      })

      fixture.detectChanges();

      const playerDice = fixture.debugElement.query(By.css('dice'));

      playerDice.componentInstance.diceUpdated.emit(dice);
    });
  });

  describe('Template', () => {

    it('should have a player dice component with the correct things passed to it', () => {
      const players = [
        TestHelper.constructPlayer("Jamila", 2, 12, [1, 5, 5, 5, 5]),
        TestHelper.constructPlayer("Oum Karim", 2, 12, [4, 3, 1, 5, 6]),
        TestHelper.constructPlayer("Hiba", 2, 12, [6, 5, 4, 2, 1])
      ];
      component.gameID = "IOPS"
      component.mainPlayer = players[1];
      component.players = players;

      fixture.detectChanges();

      const playerDice = fixture.debugElement.query(By.css('dice'));

      expect(playerDice.componentInstance.player).toBe(component.mainPlayer);
      expect(playerDice.componentInstance.gameID).toBe(component.gameID);
    });

    it('should have an opponent-dice component for each opponent', () => {
      const players = [
        TestHelper.constructPlayer("Jamila", 2, 12, [1, 5, 5, 5, 5]),
        TestHelper.constructPlayer("Oum Karim", 2, 12, [4, 3, 1, 5, 6]),
        TestHelper.constructPlayer("Hiba", 2, 12, [6, 5, 4, 2, 1])
      ];
      component.mainPlayer = players[1];
      component.players = players;

      fixture.detectChanges();

      const diceElements = fixture.debugElement.queryAll(By.css('opponent-dice'));

      expect(diceElements.length).toEqual(2);

      let diceComponent = diceElements[0].componentInstance;
      expect(diceComponent.player).toEqual(players[0]);

      diceComponent = diceElements[1].componentInstance;
      expect(diceComponent.player).toEqual(players[2]);
    });
  });
});

@Component({
  selector: 'opponent-dice',
  template: '',
})
class MockOpponentDiceComponent {
  @Input()
  public player: Player;

  @Input()
  public color;
}
@Component({
  selector: 'dice',
  template: '',
})
class MockDiceComponent {
  @Input()
  public player: Player;
  @Input()
  public gameID: string;

  @Output()
  playerUpdated = new EventEmitter<Player>();
  @Output()
  potentialScorecardUpdated = new EventEmitter<Scorecard>();
  @Output()
  diceUpdated = new EventEmitter<number[]>();
}

