import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-border-four',
  templateUrl: './die-4-border.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieBorderFourComponent {

  @Input()
  public color: string;
}
