import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-filled-five',
  templateUrl: './die-5-filled.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieFilledFiveComponent {

  @Input()
  public color: string;
}
