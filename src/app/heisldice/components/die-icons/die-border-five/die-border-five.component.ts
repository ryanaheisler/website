import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-border-five',
  templateUrl: './die-5-border.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieBorderFiveComponent {

  @Input()
  public color: string;
}
