import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-border-three',
  templateUrl: './die-3-border.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieBorderThreeComponent {

  @Input()
  public color: string;
}
