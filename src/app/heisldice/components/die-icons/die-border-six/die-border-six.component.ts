import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-border-six',
  templateUrl: './die-6-border.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieBorderSixComponent {

  @Input()
  public color: string;
}
