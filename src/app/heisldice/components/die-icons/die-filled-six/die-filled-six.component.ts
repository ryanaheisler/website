import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-filled-six',
  templateUrl: './die-6-filled.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieFilledSixComponent {

  @Input()
  public color: string;
}
