import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-filled-four',
  templateUrl: './die-4-filled.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieFilledFourComponent {

  @Input()
  public color: string;
}
