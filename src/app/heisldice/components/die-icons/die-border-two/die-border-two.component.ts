import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-border-two',
  templateUrl: './die-2-border.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieBorderTwoComponent {

  @Input()
  public color: string;
}
