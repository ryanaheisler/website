import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-filled-two',
  templateUrl: './die-2-filled.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieFilledTwoComponent {

  @Input()
  public color: string;
}
