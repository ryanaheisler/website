import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-border-one',
  templateUrl: './die-1-border.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieBorderOneComponent {

  @Input()
  public color: string;
}
