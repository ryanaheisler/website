import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-filled-three',
  templateUrl: './die-3-filled.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieFilledThreeComponent {

  @Input()
  public color: string;
}
