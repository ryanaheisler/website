import { Component, Input } from '@angular/core';

@Component({
  selector: 'die-filled-one',
  templateUrl: './die-1-filled.svg',
  styleUrls: ['../die-icon.component.css']
})
export class DieFilledOneComponent {

  @Input()
  public color: string;
}
