import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'disconnected-overlay',
  templateUrl: './disconnected-overlay.component.html',
  styleUrls: ['./disconnected-overlay.component.css']
})
export class DisconnectedOverlayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
