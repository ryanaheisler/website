import { TestHelper } from 'src/test/test-helper';
import { SettingsStorageService } from './../../services/settings-storage/settings-storage.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import * as td from 'testdouble';
import { Cookies } from './../../models/cookies';
import { GameObjectFromServer } from './../../models/game.model';
import { GameService } from './../../services/game/game.service';
import { HeisldiceLandingPageComponent } from './heisldice-landing-page.component';

describe('HeisldiceLandingPageComponent', () => {
  let fixture: ComponentFixture<HeisldiceLandingPageComponent>;
  let mockModal: any;
  let mockRouter: any;
  let mockActivatedRoute: any;
  let mockGameService: GameService;
  let mockCookieService: CookieService;
  let mockToastrService: ToastrService;
  let mockSettingsStorageService: SettingsStorageService;

  beforeEach(async(() => {
    mockModal = td.object(['load']);
    mockModal.stateEvents = { subscribe: td.function() };
    mockRouter = td.object(['navigate']);
    mockActivatedRoute = { parent: "parent route" };
    mockGameService = td.object(GameService.prototype);
    mockCookieService = td.object(CookieService.prototype);
    mockToastrService = td.object(ToastrService.prototype);
    mockSettingsStorageService = td.object(SettingsStorageService.prototype);
    TestBed.configureTestingModule({
      declarations: [HeisldiceLandingPageComponent],
      imports: [FormsModule, ToastrModule],
      providers: [
        { provide: GameService, useValue: mockGameService },
        { provide: Router, useValue: mockRouter },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: CookieService, useValue: mockCookieService },
        { provide: ToastrService, useValue: mockToastrService },
        { provide: SettingsStorageService, useValue: mockSettingsStorageService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeisldiceLandingPageComponent);
    fixture.detectChanges();
  });

  describe('component', () => {
    it('should clear the current game ID and player name from session storage', () => {
      td.verify(mockSettingsStorageService.unsetCurrentGameID());
      td.verify(mockSettingsStorageService.unsetCurrentPlayerName());
      TestHelper.expectNothing();
    });
  });

  describe('new game button', () => {
    it('should exist', () => {
      const button: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#new-game-button');

      expect(button.innerText).toEqual('New Game');
    });

    it('should navigate to the game page when new game', () => {
      const button: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#new-game-button');
      const game = new GameObjectFromServer("J3SD");
      td.when(mockGameService.newGame()).thenReturn(of(game));

      td.when(mockCookieService.check(Cookies.hasDismissedIntroModal)).thenReturn(true);
      td.when(mockCookieService.get(Cookies.hasDismissedIntroModal)).thenReturn("true");

      button.click()

      td.verify(mockModal.load(td.matchers.anything()), { times: 0 });
      td.verify(mockModal.stateEvents.subscribe(td.matchers.anything()), { times: 0 });
      td.verify(mockRouter.navigate([`${game.gameID}`], { relativeTo: mockActivatedRoute }));
      expect().nothing();
    });
  });

  describe('join game button', () => {

    let button: HTMLButtonElement;
    let input: HTMLInputElement;

    beforeEach(() => {
      button = fixture.debugElement.nativeElement.querySelector('#join-game-button');
      input = fixture.debugElement.nativeElement.querySelector('#join-game-input');
    });

    it('should exist with a text input', () => {
      expect(input.placeholder).toEqual('Enter a Game ID');
      expect(input.type).toEqual('text');
      expect(button.innerText).toEqual('Join Game');
    });

    it('should navigate to the game page if player enters a valid game ID and clicks Join Game', () => {
      const game = new GameObjectFromServer("j3sd");

      td.when(mockGameService.getExistingGame("J3SD")).thenReturn(of(game));

      input.value = game.gameID;
      input.dispatchEvent(new Event('input'));

      button.click();

      td.verify(mockRouter.navigate([`J3SD`], { relativeTo: mockActivatedRoute }));

      expect().nothing();
    });

    it('should show toast and not navigate if game ID entered is not valid', () => {
      const gameID = "J3SD";

      td.when(mockGameService.getExistingGame(gameID)).thenReturn(of(null));

      input.value = gameID;
      input.dispatchEvent(new Event('input'));

      button.click();

      td.verify(mockRouter.navigate(td.matchers.anything()), { times: 0 });
      td.verify(mockToastrService.error("The game ID you entered does not match a current game.", "GAME NOT FOUND", { positionClass: "toast-center-center" }));
      expect().nothing();
    });
  });

});
