import { SettingsStorageService } from './../../services/settings-storage/settings-storage.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GameObjectFromServer } from './../../models/game.model';
import { GameService } from './../../services/game/game.service';

@Component({
  selector: 'heisldice-landing-page',
  templateUrl: './heisldice-landing-page.component.html',
  styleUrls: ['./heisldice-landing-page.component.css']
})
export class HeisldiceLandingPageComponent implements OnInit {

  constructor(
    private gameService: GameService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toaster: ToastrService,
    private settingsStorageService: SettingsStorageService
  ) { }

  public joinGameID: string;


  ngOnInit() {
    this.settingsStorageService.unsetCurrentGameID();
    this.settingsStorageService.unsetCurrentPlayerName();
  }

  public get joinDisabled(): boolean {
    const input: HTMLInputElement = document.getElementById('join-game-input') as HTMLInputElement;
    return input.value === "";
  }

  newGame() {
    this.gameService.newGame().subscribe((game: GameObjectFromServer) => {
      this.navigate(game.gameID);
    });
  }

  joinGame() {
    this.gameService.getExistingGame(this.joinGameID.toUpperCase()).subscribe((game: GameObjectFromServer) => {
      if (game === null) {
        this.toaster.error("The game ID you entered does not match a current game.", "GAME NOT FOUND", { positionClass: "toast-center-center" });
      } else {
        this.navigate(this.joinGameID.toUpperCase());
      }
    });
  }

  private navigate(gameID: string) {
    this.router.navigate([`${gameID}`], { relativeTo: this.activatedRoute });
  }
}
