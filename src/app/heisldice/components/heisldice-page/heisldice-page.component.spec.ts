import { SettingsToggleService } from './../../services/settings-toggle/settings-toggle.service';
import { DisconnectedOverlayComponent } from './../disconnected-overlay/disconnected-overlay.component';
import { LoggerService } from './../../services/logger/logger.service';
import { HttpClientModule } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as td from 'testdouble';
import { DEFAULT_PLAYER, Player } from '../../models/player.model';
import { Scorecard } from '../../models/scorecard.model';
import { HeisldiceWebsocketService } from '../../services/websocket/websocket.service';
import { Category } from './../../models/category.model';
import { GameObjectFromServer } from './../../models/game.model';
import { DiceService } from './../../services/dice/dice.service';
import { PlayerService } from './../../services/player/player.service';
import { HeisldicePageComponent } from './heisldice-page.component';
import { Subject, Subscription } from 'rxjs';
import { Captor } from 'testdouble';
import { TestHelper } from 'src/test/test-helper';
import { SettingsStorageService } from '../../services/settings-storage/settings-storage.service';

let component: HeisldicePageComponent;
let fixture: ComponentFixture<HeisldicePageComponent>;
let scorecard: Scorecard;
let gameID;
let game: GameObjectFromServer;

let mockWebSocketService: HeisldiceWebsocketService;
let mockLoggerService: LoggerService;
let mockSettingsStorageService: SettingsStorageService;

const makeBeforeEachCallback = (uniqueSetupCallback: Function) => {
  return () => {
    scorecard = new Scorecard();
    gameID = '9188';
    game = new GameObjectFromServer(gameID)

    mockWebSocketService = td.object(HeisldiceWebsocketService.prototype);

    uniqueSetupCallback();

    mockLoggerService = td.object(LoggerService.prototype);

    mockSettingsStorageService = td.object(SettingsStorageService.prototype);

    TestBed.configureTestingModule({
      declarations: [HeisldicePageComponent, MockDiceComponent, MockScorecardComponent, MockAllDiceComponent, DisconnectedOverlayComponent],
      imports: [FontAwesomeModule, HttpClientModule],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            snapshot: {
              data: { scorecard: scorecard },
              params: { gameID: gameID }
            }
          },
        },
        { provide: HeisldiceWebsocketService, useValue: mockWebSocketService },
        PlayerService,
        DiceService,
        { provide: LoggerService, useValue: mockLoggerService },
        SettingsToggleService,
        { provide: SettingsStorageService, useValue: mockSettingsStorageService },
      ]
    })
      .compileComponents();
  }
}

describe('HeisldicePageComponent', () => {
  let mockWebsocketSubject, secondMockWebsocketSubject;
  let mockWebsocketSubscription, secondMockWebsocketSubscription;

  let errorHandlerCaptor: Captor;
  let secondErrorHandlerCaptor: Captor;
  let dataHandlerCaptor: Captor;
  let secondDataHandlerCaptor: Captor;

  let successfulWebsocketConnectionCallbackCaptor: Captor;

  beforeEach(async(makeBeforeEachCallback(() => {
    successfulWebsocketConnectionCallbackCaptor = td.matchers.captor();

    mockWebsocketSubject = td.object(['subscribe', 'pipe']);
    secondMockWebsocketSubject = td.object(['subscribe', 'pipe']);
    td.when(mockWebSocketService.getWebSocketForGame(gameID, successfulWebsocketConnectionCallbackCaptor.capture())).thenReturn(mockWebsocketSubject, secondMockWebsocketSubject);

    td.when(mockWebsocketSubject.pipe(td.matchers.anything())).thenReturn(mockWebsocketSubject);
    td.when(secondMockWebsocketSubject.pipe(td.matchers.anything())).thenReturn(secondMockWebsocketSubject);

    errorHandlerCaptor = td.matchers.captor();
    secondErrorHandlerCaptor = td.matchers.captor();
    dataHandlerCaptor = td.matchers.captor();
    secondDataHandlerCaptor = td.matchers.captor();

    mockWebsocketSubscription = td.object(Subscription.prototype);
    secondMockWebsocketSubscription = td.object(Subscription.prototype);
    td.when(mockWebsocketSubject
      .subscribe(dataHandlerCaptor.capture(), errorHandlerCaptor.capture()))
      .thenReturn(mockWebsocketSubscription);
    td.when(secondMockWebsocketSubject
      .subscribe(secondDataHandlerCaptor.capture(), secondErrorHandlerCaptor.capture()))
      .thenReturn(secondMockWebsocketSubscription);
  })));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeisldicePageComponent);
    component = fixture.componentInstance;
    component.game = game;
    fixture.detectChanges();
  });

  describe('component', () => {
    it('should store the game ID in session storage on construction', () => {
      td.verify(mockSettingsStorageService.storeCurrentGameID(gameID));
      TestHelper.expectNothing();
    });

    it('should add and remove class to prevent overscroll reload in OnInit and OnDestroy', () => {
      expect(document.body.classList).toContain("no-scroll-reload");

      component.ngOnDestroy();

      expect(document.body.classList).not.toContain("no-scroll-reload");

    });

    it('should get scorecard from Activated Route and make a new Player', () => {
      expect(component.player).toEqual(DEFAULT_PLAYER());
      expect(component.scorecard).toBe(scorecard);
      expect(component.potentialScorecard).toBe(scorecard);
    });

    it('should subscribe to websocket subject with correct callback', () => {
      const allDiceComponent = fixture.debugElement.query(By.directive(MockAllDiceComponent)).componentInstance;
      component.player.myTurn = false;
      component.player.name = "Ryan";
      component.player.rollsRemaining = 0

      const game = new GameObjectFromServer(gameID, [TestHelper.constructPlayer('Ryan', 3, 1, [], true), TestHelper.constructPlayer('Etta')]);
      dataHandlerCaptor.value(game);

      fixture.detectChanges();

      expect(allDiceComponent.players).toEqual(game.players);
      expect(component.player.myTurn).toBe(true);
      expect(component.player.rollsRemaining).toEqual(3);
    });

    it('should stop showing disconnected overlay when it receives a message from the websocket', () => {
      component.disconnectedFromWebsocket = true;

      fixture.detectChanges();

      const game = new GameObjectFromServer(gameID, [TestHelper.constructPlayer('Ryan', 3, 1, [], true), TestHelper.constructPlayer('Etta')]);
      dataHandlerCaptor.value(game);

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(DisconnectedOverlayComponent))).toBeNull();
    });

    it('should stop showing disconnected overlay when a connection to the websocket server is made', () => {
      component.disconnectedFromWebsocket = true;

      fixture.detectChanges();

      successfulWebsocketConnectionCallbackCaptor.value("anything");

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(DisconnectedOverlayComponent))).toBeNull();
    });

    it('should unsubscribe and reconnect to the websocket on error', () => {
      errorHandlerCaptor.value("error1");

      td.verify(mockWebsocketSubscription.unsubscribe());
      td.verify(mockLoggerService.log("Error in connection to websocket!"));
      td.verify(mockLoggerService.log("Attempting to reconnect to the websocket server..."));

      const allDiceComponent = fixture.debugElement.query(By.directive(MockAllDiceComponent)).componentInstance;
      component.player.myTurn = false;
      component.player.name = "Ryan";
      component.player.rollsRemaining = 0

      const game = new GameObjectFromServer(gameID, [TestHelper.constructPlayer('Ryan', 3, 1, [], true), TestHelper.constructPlayer('Etta')]);
      dataHandlerCaptor.value(game);

      fixture.detectChanges();

      expect(allDiceComponent.players).toEqual(game.players);
      expect(component.player.myTurn).toBe(true);
      expect(component.player.rollsRemaining).toEqual(3);

      secondErrorHandlerCaptor.value("error2");
      td.verify(secondMockWebsocketSubscription.unsubscribe());
    });

    it('should start showing disconnected overlay when it loses connection to the websocket', () => {
      errorHandlerCaptor.value("Failed!");

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(DisconnectedOverlayComponent))).toBeTruthy();
    });

    it('should unsubscribe from websocket when it is destroyed.', () => {
      component.ngOnDestroy();

      td.verify(mockWebsocketSubscription.unsubscribe());
      expect().nothing();
    });
  });

  describe('template', () => {
    describe('scorecard child component', () => {
      let scorecardComponent: MockScorecardComponent;
      beforeEach(() => {
        scorecardComponent = fixture.debugElement
          .query(By.directive(MockScorecardComponent)).componentInstance;
      });
      it('should have correct inputs', () => {
        const currentDice = [1, 4, 3, 2, 1];
        component.currentDice = currentDice

        fixture.detectChanges();

        expect(scorecardComponent.player).toBe(component.player);
        expect(scorecardComponent.scorecard).toBe(component.scorecard);
        expect(scorecardComponent.potentialScorecard).toBe(component.potentialScorecard);
        expect(scorecardComponent.currentDice).toBe(component.currentDice);
        expect(scorecardComponent.game).toBe(game);
      });

      it('should have correct event handlers', () => {
        const newPlayer = TestHelper.constructPlayer('Klambo', 2, 56);
        scorecardComponent.playerUpdated.emit(newPlayer);
        expect(component.player).toBe(newPlayer);
      });
    });

    describe('all-dice child component', () => {
      let allDiceComponent: MockAllDiceComponent;
      beforeEach(() => {
        allDiceComponent = fixture.debugElement
          .query(By.directive(MockAllDiceComponent)).componentInstance;
      });

      it('should have the players from the game and the current player name from the heisldice page', () => {
        expect(allDiceComponent.players).toEqual(new GameObjectFromServer(gameID).players);

        const updatedPlayers = [TestHelper.constructPlayer('Ryan'), TestHelper.constructPlayer('Jeff')];
        component.player = TestHelper.constructPlayer('Kammy');
        component.game = new GameObjectFromServer(gameID, updatedPlayers);
        fixture.detectChanges();

        expect(allDiceComponent.players).toEqual(updatedPlayers);
        expect(allDiceComponent.gameID).toEqual(component.game.gameID);
        expect(allDiceComponent.mainPlayer).toEqual(component.player);
      });
      it('should have player event handler', () => {
        const newPlayer = TestHelper.constructPlayer('Klambo', 2, 56);
        allDiceComponent.playerUpdated.emit(newPlayer);
        expect(component.player).toBe(newPlayer);
      });

      it('should have potential scorecard event handler', () => {
        const potentialScores = new Scorecard();
        potentialScores.categories.push(new Category("Sevens"));
        allDiceComponent.potentialScorecardUpdated.emit(potentialScores);
        expect(component.potentialScorecard).toBe(potentialScores);
      });

      it('should have dice event handler', () => {
        const dice = [4, 4, 2, 3, 4];
        allDiceComponent.diceUpdated.emit(dice);
        expect(component.currentDice).toBe(dice);
      });
    });
  });
});

describe("HeisldicePageComponent - Real WebsocketSubject", () => {
  let websocketSubject: Subject<any>;

  beforeEach(async(makeBeforeEachCallback(() => {
    websocketSubject = new Subject();
    td.when(mockWebSocketService.getWebSocketForGame(gameID, td.matchers.anything())).thenReturn(websocketSubject);
  })));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeisldicePageComponent);
    component = fixture.componentInstance;
    component.game = game;
    fixture.detectChanges();
  });

  afterEach(() => {
  });

  describe('WebsocketSubject filter', () => {
    it('should ignore messages from websocket that are not a Game object', () => {
      const nonGameMessage = { nonGameField: "non-game value" };
      websocketSubject.next(nonGameMessage);
      expect().nothing();
    });
  });
});

@Component({
  selector: 'dice',
  template: '',
})
class MockDiceComponent {
  @Input()
  player: Player;
  @Input()
  gameID: string;
  @Output()
  playerUpdated = new EventEmitter<Player>();
  @Output()
  potentialScorecardUpdated = new EventEmitter<Scorecard>();
  @Output()
  diceUpdated = new EventEmitter<number[]>();
}

@Component({
  selector: 'scorecard',
  template: '',
})
class MockScorecardComponent {
  @Input()
  player: Player;
  @Input()
  gameID: string;
  @Input()
  game: GameObjectFromServer;
  @Input()
  scorecard: Scorecard;
  @Input()
  potentialScorecard: Scorecard;
  @Input()
  currentDice: number[];
  @Output()
  playerUpdated = new EventEmitter<Player>();
}

@Component({
  selector: 'all-dice',
  template: '',
})
class MockAllDiceComponent {
  @Input()
  public players: Player[];
  @Input()
  mainPlayer: Player;
  @Input()
  gameID: string;
  @Output()
  playerUpdated = new EventEmitter<Player>();
  @Output()
  potentialScorecardUpdated = new EventEmitter<Scorecard>();
  @Output()
  diceUpdated = new EventEmitter<number[]>();
}