import { SettingsStorageService } from './../../services/settings-storage/settings-storage.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { WebSocketSubject } from 'rxjs/webSocket';
import { DEFAULT_PLAYER, Player } from '../../models/player.model';
import { GameObjectFromServer } from './../../models/game.model';
import { Scorecard } from './../../models/scorecard.model';
import { LoggerService } from './../../services/logger/logger.service';
import { HeisldiceWebsocketService } from './../../services/websocket/websocket.service';

@Component({
  selector: 'heisldice-page',
  templateUrl: './heisldice-page.component.html',
  styleUrls: ['./heisldice-page.component.css'],
})
export class HeisldicePageComponent implements OnInit, OnDestroy {

  player: Player;
  scorecard: Scorecard;
  potentialScorecard: Scorecard;
  currentDice: number[];
  game: GameObjectFromServer;
  private websocketSubject: WebSocketSubject<GameObjectFromServer>;
  private websocketSubscription: Subscription;
  public disconnectedFromWebsocket: boolean;

  constructor(
    private route: ActivatedRoute,
    private websocketService: HeisldiceWebsocketService,
    private loggerService: LoggerService,
    private settingsStorageService: SettingsStorageService
  ) {
    this.player = DEFAULT_PLAYER();
    this.scorecard = this.route.snapshot.data.scorecard;
    this.potentialScorecard = this.route.snapshot.data.scorecard;

    const gameID = this.route.snapshot.params.gameID;
    this.game = new GameObjectFromServer(gameID);
    this.settingsStorageService.storeCurrentGameID(gameID)

    this.getNewWebsocketAndSubscribe();
  }

  ngOnInit() {
    document.body.classList.add("no-scroll-reload");
    this.disconnectedFromWebsocket = false;
  }

  ngOnDestroy() {
    document.body.classList.remove("no-scroll-reload");
    this.websocketSubscription.unsubscribe();
  }

  private getNewWebsocketAndSubscribe() {
    this.websocketSubject = this.websocketService.getWebSocketForGame(this.game.gameID, () => {
      this.disconnectedFromWebsocket = false;
    });
    this.websocketSubscription = this.websocketSubject
      .pipe(map((message: any) => {
        console.log("received message:", message);
        return message;
      }))
      .pipe(filter(value => value.gameID != undefined))
      .subscribe(
        this.receiveMessageFromWebsocket(),
        this.handleErrorFromWebsocket());
  }

  private receiveMessageFromWebsocket(): (value: any) => void {
    return (message: GameObjectFromServer) => {
      console.log("received message was Game Information");
      this.disconnectedFromWebsocket = false;
      this.game = message;
      const myPlayer = message.players.find((thePlayer) => {
        return thePlayer.name === this.player.name;
      });
      if (myPlayer && myPlayer.myTurn) {
        this.player.myTurn = myPlayer.myTurn;
        this.player.rollsRemaining = myPlayer.rollsRemaining;
      }
    };
  }

  private handleErrorFromWebsocket(): (error: any) => void {
    return (error: any) => {
      this.disconnectedFromWebsocket = true;
      this.loggerService.log(`Error in connection to websocket!`);
      this.websocketSubscription.unsubscribe();
      this.loggerService.log('Attempting to reconnect to the websocket server...');
      this.getNewWebsocketAndSubscribe();
    };
  }

  playerUpdated(player: Player) {
    this.player = player;
  }

  potentialScorecardUpdated(scorecard: Scorecard) {
    this.potentialScorecard = scorecard;
  }

  diceUpdated(dice: number[]) {
    this.currentDice = dice;
  }
}
