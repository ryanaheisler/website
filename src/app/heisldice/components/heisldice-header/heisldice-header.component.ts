import { SettingsStorageService } from './../../services/settings-storage/settings-storage.service';
import { SettingsToggleService } from './../../services/settings-toggle/settings-toggle.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'heisldice-header',
  templateUrl: './heisldice-header.component.html',
  styleUrls: ['./heisldice-header.component.css']
})
export class HeisldiceHeaderComponent implements OnInit {

  public settingsIsOpen: boolean;
  public gameID: string;

  constructor(
    private settingsToggleService: SettingsToggleService,
    private settingsStorageService: SettingsStorageService
  ) { }

  ngOnInit() {
    this.settingsIsOpen = false;
    this.settingsStorageService.subscribeToCurrentGameID().subscribe((gameID: string) => {
      this.gameID = gameID;
    });
  }

  public toggleSettings() {
    if (this.settingsIsOpen) {
      this.settingsToggleService.settingsClosed();
    } else {
      this.settingsToggleService.settingsOpened();
    }
    this.settingsIsOpen = !this.settingsIsOpen;
  }

  public closeSettings() {
    this.settingsToggleService.settingsClosed();
    this.settingsIsOpen = false;
  }
}
