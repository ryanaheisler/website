import { SettingsStorageService } from './../../services/settings-storage/settings-storage.service';
import { SettingsToggleService } from './../../services/settings-toggle/settings-toggle.service';
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { HeisldiceHeaderComponent } from './heisldice-header.component';
import * as td from 'testdouble';
import { TestHelper } from 'src/test/test-helper';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';

describe('HeisldiceHeaderComponent', () => {
  let component: HeisldiceHeaderComponent;
  let fixture: ComponentFixture<HeisldiceHeaderComponent>;
  let gameID: string;
  let mockSettingsToggleService: SettingsToggleService
  let mockSettingsStorageService: SettingsStorageService
  let gameIDSubject: Subject<string>;

  beforeEach(async(() => {
    gameID = "3L0H";
    mockSettingsToggleService = td.object(SettingsToggleService.prototype);

    gameIDSubject = new Subject();
    mockSettingsStorageService = td.object(SettingsStorageService.prototype);
    td.when(mockSettingsStorageService.subscribeToCurrentGameID()).thenReturn(gameIDSubject);

    TestBed.configureTestingModule({
      declarations: [HeisldiceHeaderComponent, MockSettingsComponent],
      providers: [
        { provide: SettingsToggleService, useValue: mockSettingsToggleService },
        { provide: SettingsStorageService, useValue: mockSettingsStorageService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeisldiceHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have the title', () => {
    const title = fixture.debugElement.nativeElement.querySelector('h1');
    expect(title.innerText).toContain('Heisldice!');
  });

  it('should have the game ID, if present', fakeAsync(() => {
    gameIDSubject.next(gameID);
    fixture.detectChanges();

    const title = fixture.debugElement.nativeElement.querySelector('p');
    expect(title.innerText).toEqual(`Game ID: ${gameID}`);
  }));

  it('should not have the game ID, if not present', () => {
    gameIDSubject.next(undefined);
    fixture.detectChanges();

    const title = fixture.debugElement.nativeElement.querySelector('p');
    expect(title).toBeNull();
  });

  it('should have a button to open the settings', () => {
    const button = fixture.debugElement.nativeElement.querySelector('button');
    expect(button.innerText).toEqual('Settings');
  });

  describe('clicking the settings button', () => {
    it('should call the settings toggle service correctly', () => {
      const button = fixture.debugElement.nativeElement.querySelector('button#settings-button');

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 0 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 0 });

      button.click();

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 1 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 0 });

      button.click();

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 1 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 1 });

      button.click();

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 2 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 1 });

      button.click();

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 2 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 2 });

      TestHelper.expectNothing();
    });
    it('should show/hide the settings when toggled', () => {
      const button = fixture.debugElement.nativeElement.querySelector('button#settings-button');

      let settings = fixture.debugElement.query(By.directive(MockSettingsComponent));
      expect(settings).toBeNull();

      button.click();
      fixture.detectChanges();
      settings = fixture.debugElement.query(By.directive(MockSettingsComponent));
      expect(settings).not.toBeNull();

      button.click();
      fixture.detectChanges();
      settings = fixture.debugElement.query(By.directive(MockSettingsComponent));
      expect(settings).toBeNull();

      button.click();
      fixture.detectChanges();
      settings = fixture.debugElement.query(By.directive(MockSettingsComponent));
      expect(settings).not.toBeNull();
    });
  });

  describe('clicking the done button', () => {
    let openButton;
    beforeEach(() => {
      openButton = fixture.debugElement.nativeElement.querySelector('button#settings-button');
      openButton.click();
      td.reset();
      fixture.detectChanges();
    });
    it('should call the settings toggle service correctly', () => {
      const button = fixture.debugElement.nativeElement.querySelector('button#close-settings-button');

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 0 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 0 });

      button.click();

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 0 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 1 });

      button.click();

      td.verify(mockSettingsToggleService.settingsOpened(), { times: 0 });
      td.verify(mockSettingsToggleService.settingsClosed(), { times: 2 });

      TestHelper.expectNothing();
    });
    it('should hide the settings when clicked', () => {
      const button = fixture.debugElement.nativeElement.querySelector('button#close-settings-button');
      button.click();
      fixture.detectChanges();

      const settings = fixture.debugElement.query(By.directive(MockSettingsComponent));
      expect(settings).toBeNull();
    });
  });
});

@Component({
  selector: 'heisldice-settings',
  template: ''
})
class MockSettingsComponent { }