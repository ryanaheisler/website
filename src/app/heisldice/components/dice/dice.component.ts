import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DEFAULT_PLAYER, Player } from '../../models/player.model';
import { SettingsStorageService } from '../../services/settings-storage/settings-storage.service';
import { DieModel } from './../../models/die.model';
import { Scorecard } from './../../models/scorecard.model';
import { ConfigurationService } from './../../services/configuration/configuration.service';
import { DiceRollDataFromServer, DiceService } from './../../services/dice/dice.service';

@Component({
  selector: 'dice',
  templateUrl: './dice.component.html',
  styleUrls: ['./dice.component.css']
})
export class DiceComponent implements OnInit, OnDestroy {

  public dice: DieModel[];

  private _player: Player;
  public _environment = environment
  private _diceAreRolling: boolean = false;
  private $destroy: Subject<any> = new Subject();

  private _diceColor: string;
  public get diceColor(): string {
    return this._diceColor || "#76448a";
  }
  public set diceColor(value: string) {
    this._diceColor = value;
  }

  public get playerDiceColor(): string {
    if (this.diceColor) return this.diceColor;
    else return "#76448a";

  }

  public get player(): Player {
    return this._player;
  }
  @Input()
  public set player(value: Player) {
    if (this._player.turnsRemaining > value.turnsRemaining) {
      this.dice = this.dice.map((die: DieModel) => new DieModel(die.value, false));
    }

    this._player = value;
  }
  @Input()
  public gameID: string;

  @Output()
  playerUpdated = new EventEmitter<Player>();
  @Output()
  potentialScorecardUpdated = new EventEmitter<Scorecard>();
  @Output()
  diceUpdated = new EventEmitter<number[]>();

  constructor(
    private diceService: DiceService,
    private toaster: ToastrService,
    private configurationService: ConfigurationService,
    private settingsStorageService: SettingsStorageService
  ) {
    this.setDefaultDice();
    this._player = DEFAULT_PLAYER();
  }

  ngOnInit() {
    this.settingsStorageService.subscribeToUserDiceColor().pipe(takeUntil(this.$destroy)).subscribe((value: string) => {
      this.diceColor = value;
    });
  }

  ngOnDestroy() {
    this.$destroy.next();
  }

  private setDefaultDice() {
    this.dice = [
      new DieModel(1, false),
      new DieModel(2, false),
      new DieModel(3, false),
      new DieModel(4, false),
      new DieModel(5, false)
    ];
  }

  rollDice() {
    if (!this.buttonShouldBeHidden()) {
      this._diceAreRolling = true;
      this.diceService.roll(this.getReservedDice(), this.player, this.gameID).subscribe((results: DiceRollDataFromServer) => {
        if (results === null) {
          this.toaster.error("Server could not be reached", "ERROR", { positionClass: "toast-center-center" });
        } else {
          this.setDice(results.dice);
          this.playerUpdated.emit(results.player);
          this.potentialScorecardUpdated.emit(results.scorecard);
          this.diceUpdated.emit(results.dice);
          setTimeout(() => {
            this._diceAreRolling = false;
          }, this.configurationService.getTimeBetweenDiceDisplaysInMillis() * 5 + 100);
        }
      });
    }
  }

  private getReservedDice(): number[] {
    return this.dice.map((die) => {
      return die.isReserved ? die.value : 0;
    })
  }

  private setDice(diceValues: number[]) {

    const originalDice = [...this.dice];
    this.dice = [];
    diceValues = [...diceValues];

    originalDice.forEach((die, index) => {
      if (die.isReserved) {
        this.dice.push(die);
        diceValues.splice(diceValues.findIndex(val => val === die.value), 1);
      };
    });
    this.dice = this.dice.sort((a, b) => {
      return a.value - b.value;
    });

    let diceDisplayTimeoutAdder = 0
    if (this.dice.length === 0) {
      const first = diceValues.shift();
      this.dice.push(new DieModel(first, false));
      diceDisplayTimeoutAdder = 1;
    }

    diceValues.forEach((value, index) => {
      setTimeout(() => {
        this.dice.push(new DieModel(value, false))
      }, (index + diceDisplayTimeoutAdder) * this.configurationService.getTimeBetweenDiceDisplaysInMillis());
    });
  }

  buttonShouldBeHidden() {
    let shouldBeHidden = this.player.rollsRemaining <= 0;
    shouldBeHidden = shouldBeHidden || this.player.turnsRemaining <= 0;
    shouldBeHidden = shouldBeHidden || !this.player.myTurn;
    shouldBeHidden = shouldBeHidden || !this.player.name;

    return shouldBeHidden;
  }

  buttonShouldBeDisabled() {
    let shouldBeDisabled = this.dice.every(die => {
      return die.isReserved;
    });
    shouldBeDisabled = shouldBeDisabled || this._diceAreRolling;
    return shouldBeDisabled;
  }

  private setDiceToYahtzee() {
    if (environment.debug) {
      this.dice = [
        new DieModel(1, true),
        new DieModel(1, true),
        new DieModel(1, true),
        new DieModel(1, true),
        new DieModel(1, true)
      ];
      this.diceUpdated.emit(this.dice.map(die => { return die.value }));
    }
  }

}
