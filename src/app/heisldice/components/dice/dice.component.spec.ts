import { Component, Input } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subject } from 'rxjs';
import { TestHelper } from 'src/test/test-helper';
import * as td from 'testdouble';
import { Category } from '../../models/category.model';
import { Player } from '../../models/player.model';
import { Scorecard } from '../../models/scorecard.model';
import { SettingsStorageService } from '../../services/settings-storage/settings-storage.service';
import { environment } from './../../../../environments/environment';
import { DieModel } from './../../models/die.model';
import { ConfigurationService } from './../../services/configuration/configuration.service';
import { DiceService } from './../../services/dice/dice.service';
import { IsMobileDeviceService } from './../../services/is-mobile-device/is-mobile-device.service';
import { DiceComponent } from './dice.component';

describe('DiceComponent', () => {
  let component: DiceComponent;
  let fixture: ComponentFixture<DiceComponent>;
  let diceObservable: Observable<{ dice: number[], scorecard: Scorecard, player: Player }>;
  let expectedDice: number[];
  let mockDiceService: DiceService;
  let player: Player, playerFromAPI: Player;
  let scorecard: Scorecard;
  let reservedDice;
  let gameID: string;
  let mockToastrService: ToastrService;
  let mockConfigurationService: ConfigurationService;
  let mockSettingsStorageService: SettingsStorageService
  let diceColorSubject: Subject<string>;

  beforeEach(async(() => {
    expectedDice = [3, 1, 1, 5, 6];
    scorecard = new Scorecard();
    scorecard.categories.push(new Category("Shrees"));

    player = TestHelper.constructPlayer("Tony", 2, 13, [1, 1, 1, 1, 1], true);
    playerFromAPI = TestHelper.constructPlayer("Tony", 1, 13);

    gameID = "4431";

    mockToastrService = td.object(ToastrService.prototype);

    diceObservable = of(
      {
        dice: expectedDice,
        scorecard: scorecard,
        player: playerFromAPI
      }
    );

    mockDiceService = td.object(DiceService.prototype);
    reservedDice = [0, 0, 0, 0, 0];
    td.when(mockDiceService.roll(reservedDice, player, gameID)).thenReturn(diceObservable);

    mockConfigurationService = td.object(ConfigurationService.prototype);
    td.when(mockConfigurationService.getTimeBetweenDiceDisplaysInMillis()).thenReturn(0);

    mockSettingsStorageService = td.object(SettingsStorageService.prototype);
    diceColorSubject = new Subject();
    td.when(mockSettingsStorageService.subscribeToUserDiceColor()).thenReturn(diceColorSubject);

    TestBed.configureTestingModule({
      declarations: [DiceComponent, MockDieSVGIconComponent],
      providers: [
        { provide: DiceService, useValue: mockDiceService, },
        { provide: ToastrService, useValue: mockToastrService },
        { provide: ConfigurationService, useValue: mockConfigurationService },
        IsMobileDeviceService,
        { provide: SettingsStorageService, useValue: mockSettingsStorageService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiceComponent);
    component = fixture.componentInstance;
    component.player = player;
    component.gameID = gameID;
    fixture.detectChanges();
  });

  describe('component', () => {
    it('should have default dice values', () => {
      expect(component.dice).toEqual([
        new DieModel(1, false),
        new DieModel(2, false),
        new DieModel(3, false),
        new DieModel(4, false),
        new DieModel(5, false)
      ]);
    });

    it('should unreserve all dice if an updated player is passed in with fewer turns than the current player', () => {
      component.player = TestHelper.constructPlayer('Ryan', 0, 5, [1, 1, 1, 1, 1], false);

      const existingDice = [
        new DieModel(1, true),
        new DieModel(1, true),
        new DieModel(1, true),
        new DieModel(1, true),
        new DieModel(1, true),
      ];

      component.dice = existingDice
      component.player = TestHelper.constructPlayer('Ryan', 3, 4, [1, 1, 1, 1, 1], false);
      expect(component.dice.length).toEqual(5);
      component.dice.forEach((die, index) => {
        expect(die.isReserved).toBe(false);
        expect(die).not.toBe(existingDice[index]);
      });
    });

    describe('rollDice', () => {
      it('should call the dice service and emit event', (done) => {
        component.rollDice();

        setTimeout(() => {
          expect(component.dice.length).toEqual(5);
          component.dice.forEach((die, index) => {
            expect(die.value).toBe(expectedDice[index]);
          });
          done();
        });
      });

      it('should disable button while dice are rolling', (done) => {
        const mockDiceObservable = td.object(['subscribe']);
        td.when(mockDiceService.roll(reservedDice, player, gameID)).thenReturn(mockDiceObservable)
        const subscribeCaptor = td.matchers.captor();

        let rollButton = fixture.debugElement.nativeElement.querySelectorAll('button')[0];
        expect(rollButton.disabled).toBe(false);

        component.rollDice();
        fixture.detectChanges();

        rollButton = fixture.debugElement.nativeElement.querySelectorAll('button')[0];
        expect(rollButton.disabled).toBe(true);

        td.verify(mockDiceObservable.subscribe(subscribeCaptor.capture()));

        subscribeCaptor.value({
          dice: expectedDice,
          scorecard: scorecard,
          player: playerFromAPI
        });

        setTimeout(() => {
          fixture.detectChanges();
          rollButton = fixture.debugElement.nativeElement.querySelectorAll('button')[0];
          expect(rollButton.hidden).toBe(false);
          done();
        }, 110);
      });

      it('should emit the player via its event', (done) => {
        component.playerUpdated.subscribe((data: Player) => {
          expect(data).toEqual(playerFromAPI);
          done()
        });

        component.rollDice();
      });

      it('should emit the potential scorecard via its event', (done) => {
        component.potentialScorecardUpdated.subscribe((data: Scorecard) => {
          expect(data).toEqual(scorecard);
          done()
        });

        component.rollDice();
      });

      it('should emit the dice via its event', (done) => {
        component.diceUpdated.subscribe((data: number[]) => {
          expect(data).toEqual(expectedDice);
          done();
        });

        component.rollDice();
      });

      it('should sort reserved dice and put them at the front of the dice array', (done) => {

        component.dice = [
          new DieModel(5, false),
          new DieModel(4, true),
          new DieModel(3, false),
          new DieModel(2, false),
          new DieModel(1, true),
        ]
        reservedDice = [0, 4, 0, 0, 1];
        expectedDice = [1, 4, 6, 2, 6]

        diceObservable = of(
          {
            dice: [6, 4, 2, 6, 1],
            scorecard: scorecard,
            player: playerFromAPI
          }
        )

        td.when(mockDiceService.roll(reservedDice, player, gameID)).thenReturn(diceObservable);

        component.rollDice();

        setTimeout(() => {
          expect(component.dice.length).toBe(5);
          component.dice.forEach((die, index) => {
            expect(die.value).toBe(expectedDice[index], `INDEX: ${index}`);
          });
          done();
        });
      });

      it('shouldn\'t call the roll service when all dice are reserved', () => {
        component.buttonShouldBeHidden = () => { return true };

        component.rollDice()

        td.verify(mockDiceService.roll(td.matchers.anything(), td.matchers.anything(), td.matchers.anything()), { times: 0 });
        expect().nothing();
      });

      it('should do nothing but show an error toast when server can\'t be reached', (done) => {
        td.when(mockDiceService.roll(reservedDice, player, gameID)).thenReturn(of(null));
        component.rollDice();
        const originalDice = [1, 2, 3, 4, 5];
        setTimeout(() => {
          expect(component.dice.length).toEqual(5);
          component.dice.forEach((die, index) => {
            expect(die.value).toBe(originalDice[index]);
          });
          td.verify(mockToastrService.error("Server could not be reached", "ERROR", { positionClass: "toast-center-center" }));
          done();
        });
      });
    });
  });

  describe('template', () => {
    it('should have a border when it is the player\'s turn', () => {
      component.player.myTurn = true
      const diceColor = "#ffaecc";
      diceColorSubject.next(diceColor);
      fixture.detectChanges();

      let container = fixture.debugElement.query(By.css('.container'));

      expect(container.styles["border-width"]).toEqual('2px');
      expect(container.styles["border-color"]).toEqual(diceColor);

      diceColorSubject.next(undefined);
      fixture.detectChanges();

      container = fixture.debugElement.query(By.css('.container'));

      expect(container.styles["border-width"]).toEqual('2px');
      expect(container.styles["border-color"]).toEqual("#76448a");
    });
    it('should have NO border when it is NOT the player\'s turn', () => {
      component.player.myTurn = false
      fixture.detectChanges();

      const container = fixture.debugElement.query(By.css('.container'));

      expect(container.styles["border-width"]).toEqual('0');
    });
    it('should create 5 die components by passing the correct values', () => {
      const testDice = [
        new DieModel(4, false),
        new DieModel(6, false),
        new DieModel(5, false),
        new DieModel(3, false),
        new DieModel(5, false)
      ];
      component.dice = testDice;

      const storedDiceColor = "#238f88";
      diceColorSubject.next(storedDiceColor);

      fixture.detectChanges();

      const dieSelectors = fixture.debugElement.queryAll(By.css('die-svg-icon'));

      expect(dieSelectors.length).toEqual(5);

      dieSelectors.forEach((selector, index) => {
        let die: MockDieSVGIconComponent = selector.componentInstance;
        expect(die.die).toEqual(testDice[index]);
        expect(die.color).toEqual(storedDiceColor);
        expect(die.canReserve).toEqual(component.player.rollsRemaining !== 3);
      });
    });

    it('should give dice a default color if none is defined', () => {
      const testDice = [
        new DieModel(4, false),
        new DieModel(6, false),
        new DieModel(5, false),
        new DieModel(3, false),
        new DieModel(5, false)
      ];
      component.dice = testDice;

      const defaultDiceColor = "#76448a";
      diceColorSubject.next(undefined);

      fixture.detectChanges();

      const dieSelectors = fixture.debugElement.queryAll(By.css('die-svg-icon'));

      expect(dieSelectors.length).toEqual(5);

      dieSelectors.forEach((selector) => {
        let die: MockDieSVGIconComponent = selector.componentInstance;
        expect(die.color).toEqual(defaultDiceColor);
      });
    });

    describe('Debug', () => {
      let originalEnvironmentDebug;
      beforeEach(() => {
        originalEnvironmentDebug = environment.debug;
      });

      afterEach(() => {
        environment.debug = originalEnvironmentDebug;
      });
      it('should have a button to make dice a yahtzee if dev environment', (done) => {
        component.diceUpdated.subscribe(dice => {
          expect(dice.length).toBe(5);
          dice.forEach(die => {
            expect(die).toEqual(1);
          });
          done();
        });

        environment.debug = true;
        fixture.detectChanges();

        const yahtzeeButton = fixture.debugElement.nativeElement.querySelector('#yahtzee-button');

        yahtzeeButton.click();
      });
      it('should NOT have a button to make dice a yahtzee if prod environment', () => {
        environment.debug = false;
        fixture.detectChanges();

        const yahtzeeButton = fixture.debugElement.nativeElement.querySelector('#yahtzee-button');
        expect(yahtzeeButton).toBeFalsy();
      });
    });

    describe('roll button', () => {
      let button;
      let rollDiceCalls;
      beforeEach(() => {
        button = fixture.debugElement.nativeElement.querySelector('button');

        rollDiceCalls = 0;
        component.rollDice = () => {
          rollDiceCalls += 1;
        }
      });

      it('should call the right function', () => {
        expect(button.innerText).toEqual('Roll \'em!')

        expect(rollDiceCalls).toEqual(0);
        button.click();
        expect(rollDiceCalls).toEqual(1);
      });

      it('should be disabled when all dice are reserved', () => {
        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.disabled).toBe(false);

        component.dice.forEach(die => {
          die.isReserved = true;
        });
        fixture.detectChanges();

        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.disabled).toBe(true);
      });

      it('should be hidden when player has no rolls remaining', () => {
        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(false);

        player.rollsRemaining = 0;
        fixture.detectChanges();

        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(true);
      });

      it('should be hidden when it is not the player\'s turn', () => {
        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(false);

        player.myTurn = false;
        fixture.detectChanges();

        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(true);
      });

      it('should be hidden when the player has no name', () => {
        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(false);

        player.name = '';
        fixture.detectChanges();

        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(true);
      });

      it('should be hidden when the player has no turns remaining', () => {
        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(false);

        player.turnsRemaining = 0;
        fixture.detectChanges();

        button = fixture.debugElement.nativeElement.querySelector('button');
        expect(button.hidden).toBe(true);
      });
    });
  });
});
@Component({
  selector: 'die-svg-icon',
  template: '',
})
export class MockDieSVGIconComponent {
  @Input()
  public color: string;
  @Input()
  public canReserve: boolean;
  @Input()
  public die: DieModel;
}