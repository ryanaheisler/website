import { DieFilledFourComponent } from './../die-icons/die-filled-four/die-filled-four.component';
import { of } from 'rxjs';
import { IsMobileDeviceService } from './../../services/is-mobile-device/is-mobile-device.service';
import { DieFilledThreeComponent } from './../die-icons/die-filled-three/die-filled-three.component';
import { DieBorderThreeComponent } from './../die-icons/die-border-three/die-border-three.component';
import { DieSvgLookupService } from './../../services/die-svg-lookup/die-svg-lookup.service';
import { DieModel } from './../../models/die.model';
import { DieSvgAnchorDirective } from './../../directives/die-svg-anchor.directive';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { DieSvgIconComponent } from './die-svg-icon.component';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';

import { By } from '@angular/platform-browser';
import * as td from "testdouble";

let inputColor: string;
let dieModel: DieModel;
let black: string;
let canReserve: boolean;

@NgModule({
  declarations: [DieBorderThreeComponent, DieFilledThreeComponent, DieFilledFourComponent],
  entryComponents: [DieBorderThreeComponent, DieFilledThreeComponent, DieFilledFourComponent]
})
class TestModule { }

describe('DieSvgIconComponent', () => {
  let component: DieSvgIconComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let mockIsMobileDeviceService: IsMobileDeviceService;

  beforeEach(async(() => {

    inputColor = "#fe68ab";
    black = "#000000";
    dieModel = new DieModel(3, false);

    mockIsMobileDeviceService = td.object(IsMobileDeviceService.prototype);
    td.when(mockIsMobileDeviceService.isMobileDevice()).thenReturn(of(false));

    TestBed.configureTestingModule({
      declarations: [
        DieSvgIconComponent,
        DieSvgAnchorDirective,
        TestHostComponent,
      ],
      providers: [
        DieSvgLookupService,
        { provide: IsMobileDeviceService, useValue: mockIsMobileDeviceService }
      ],
      imports: [TestModule]
    })
      .compileComponents();
  }));

  function setupComponent() {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.query(By.directive(DieSvgIconComponent)).componentInstance;
    fixture.detectChanges();
  }

  it('should display correct die svg with border in black if unreserved', () => {
    setupComponent();

    const svg = fixture.debugElement.query(By.css('svg'));
    const rect: SVGRectElement = svg.query(By.css('rect')).nativeElement;
    const circles: SVGCircleElement[] = svg.queryAll(By.css('circle')).map(debug => debug.nativeElement);

    expect(rect.getAttribute('stroke')).toEqual(black);

    expect(circles.length).toEqual(3);
    circles.forEach((circle: SVGCircleElement) => {
      expect(circle.getAttribute('stroke')).toEqual(black);
      expect(circle.getAttribute('fill')).toEqual(black);
    });
  });

  it('should display correct die svg filled with color if reserved', () => {
    dieModel.isReserved = true;
    setupComponent();

    const svg = fixture.debugElement.query(By.css('svg'));
    const rect: SVGRectElement = svg.query(By.css('rect')).nativeElement;
    const circles: SVGCircleElement[] = svg.queryAll(By.css('circle')).map(debug => debug.nativeElement);

    expect(rect.getAttribute('fill')).toEqual(inputColor);

    expect(circles.length).toEqual(3);
  });
  it('should display correct die svg filled with color when color changes', () => {
    dieModel.isReserved = true;
    setupComponent();

    component.color = "#071b42";
    fixture.detectChanges();

    const svg = fixture.debugElement.query(By.css('svg'));
    const rect: SVGRectElement = svg.query(By.css('rect')).nativeElement;
    const circles: SVGCircleElement[] = svg.queryAll(By.css('circle')).map(debug => debug.nativeElement);

    expect(rect.getAttribute('fill')).toEqual("#071b42");
    expect(circles.length).toEqual(3);
  });
  it('should display correct die svg in black outline when color changes', () => {
    dieModel.isReserved = false;
    setupComponent();

    component.color = "#071b42";
    fixture.detectChanges();

    const svg = fixture.debugElement.query(By.css('svg'));
    const rect: SVGRectElement = svg.query(By.css('rect')).nativeElement;
    const circles: SVGCircleElement[] = svg.queryAll(By.css('circle')).map(debug => debug.nativeElement);

    expect(rect.getAttribute('stroke')).toEqual(black);
    expect(circles.length).toEqual(3);
    circles.forEach((circle: SVGCircleElement) => {
      expect(circle.getAttribute('stroke')).toEqual(black);
      expect(circle.getAttribute('fill')).toEqual(black);
    });
  });

  it('should display correct die and color when die model is set', () => {
    setupComponent();

    component.die = new DieModel(4, true);
    fixture.detectChanges();

    const svg = fixture.debugElement.query(By.css('svg'));
    const rect: SVGRectElement = svg.query(By.css('rect')).nativeElement;
    const circles: SVGCircleElement[] = svg.queryAll(By.css('circle')).map(debug => debug.nativeElement);

    expect(rect.getAttribute('fill')).toEqual(inputColor);

    expect(circles.length).toEqual(4);
  });

  describe('Mouse Click', () => {
    const clickTestCases = [
      {
        name: "should toggle reserved on the die model when clicked - starts unreserved",
        canReserve: true,
        dieModel: new DieModel(3, false),
        expectedReservedState: true,
        dieStrokeOrFill: 'fill',
        dieColorAfterClick: "#fe68ab",
        pipColorAfterClick: "#fe68ab",
      },
      {
        name: "should toggle reserved on the die model when clicked - starts reserved",
        canReserve: true,
        dieModel: new DieModel(3, true),
        expectedReservedState: false,
        dieStrokeOrFill: 'stroke',
        dieColorAfterClick: "#fe68ab",
        pipColorAfterClick: "#fe68ab",
      },
      {
        name: "should NOT toggle reserved on the die model when canReserve is false",
        canReserve: false,
        dieModel: new DieModel(3, false),
        expectedReservedState: false,
        dieStrokeOrFill: 'stroke',
        dieColorAfterClick: "#fe68ab",
        pipColorAfterClick: "#fe68ab",
      },
    ];
    clickTestCases.forEach((testCase) => {
      it(testCase.name, () => {
        canReserve = testCase.canReserve;
        dieModel = testCase.dieModel;
        setupComponent();
        fixture.detectChanges();

        const dieElement: HTMLDivElement = fixture.debugElement.query(By.css("div")).nativeElement;
        dieElement.click();
        fixture.detectChanges();

        expect(dieModel.isReserved).toBe(testCase.expectedReservedState);
        checkColors(fixture, testCase.dieStrokeOrFill, testCase.dieColorAfterClick, testCase.pipColorAfterClick);
      });
    });

    it('should NOT toggle reserved on the die model when 200ms has not yet passed', fakeAsync(() => {
      canReserve = true;
      dieModel = new DieModel(3, false);
      setupComponent();
      fixture.detectChanges();

      const dieElement: HTMLDivElement = fixture.debugElement.query(By.css("div")).nativeElement;
      dieElement.click();
      expect(dieModel.isReserved).toBe(true);

      tick(199);
      dieElement.click();
      expect(dieModel.isReserved).toBe(true);

      tick(1);
      dieElement.click();
      expect(dieModel.isReserved).toBe(false);
    }));
  });

  describe('Mouse Hover', () => {
    const hoverTestCases = [
      {
        name: 'should display correct die svg with border in color if mouse is on die and die is unreserved',
        isReserved: false,
        isMobile: false,
        dieStrokeOrFill: 'stroke',
        dieColorAfterMouseEnter: "#fe68ab",
        pipColorAfterMouseEnter: "#fe68ab",
        dieColorAfterMouseLeave: "#000000",
        pipColorAfterMouseLeave: "#000000"
      },
      {
        name: 'should not change die on hover if die is reserved',
        isReserved: true,
        isMobile: false,
        dieStrokeOrFill: 'fill',
        dieColorAfterMouseEnter: "#fe68ab",
        pipColorAfterMouseEnter: "#ffffff",
        dieColorAfterMouseLeave: "#fe68ab",
        pipColorAfterMouseLeave: "#ffffff"
      },
      {
        name: 'should not change die on hover if user is on a mobile device',
        isReserved: false,
        isMobile: true,
        dieStrokeOrFill: 'stroke',
        dieColorAfterMouseEnter: "#000000",
        pipColorAfterMouseEnter: "#000000",
        dieColorAfterMouseLeave: "#000000",
        pipColorAfterMouseLeave: "#000000"
      },
    ]
    hoverTestCases.forEach((testCase) => {
      it(testCase.name, () => {
        td.when(mockIsMobileDeviceService.isMobileDevice()).thenReturn(of(testCase.isMobile));
        dieModel.isReserved = testCase.isReserved;
        setupComponent();

        const dieElement: HTMLDivElement = fixture.debugElement.query(By.css("div")).nativeElement;

        dieElement.dispatchEvent(new MouseEvent('mouseenter'));
        fixture.detectChanges();

        checkColors(fixture, testCase.dieStrokeOrFill, testCase.dieColorAfterMouseEnter, testCase.pipColorAfterMouseEnter);

        dieElement.dispatchEvent(new MouseEvent('mouseleave'));
        fixture.detectChanges();

        checkColors(fixture, testCase.dieStrokeOrFill, testCase.dieColorAfterMouseLeave, testCase.pipColorAfterMouseLeave);
      });
    });
  });
});

function checkColors(
  fixture: ComponentFixture<TestHostComponent>,
  dieStrokeOrFill: string,
  dieColor: string,
  pipColor: string
) {
  let svg = fixture.debugElement.query(By.css('svg'));
  let rect: SVGRectElement = svg.query(By.css('rect')).nativeElement;
  let circles: SVGCircleElement[] = svg.queryAll(By.css('circle')).map(debug => debug.nativeElement);

  expect(rect.getAttribute(dieStrokeOrFill)).toEqual(dieColor);

  if (dieStrokeOrFill === "stroke") {
    expect(circles.length).toEqual(3);
    circles.forEach((circle: SVGCircleElement) => {
      expect(circle.getAttribute('stroke')).toEqual(pipColor);
      expect(circle.getAttribute('fill')).toEqual(pipColor);
    });
  }
}

@Component({
  selector: '',
  template: '<die-svg-icon [color]="inputColor" [die]=dieModel [canReserve]="canReserve"></die-svg-icon>'
})
class TestHostComponent {
  public inputColor: string = inputColor;
  public dieModel: DieModel = dieModel;
  public canReserve: boolean = canReserve;
}
