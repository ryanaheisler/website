import { Subject, Subscription } from 'rxjs';
import { IsMobileDeviceService } from './../../services/is-mobile-device/is-mobile-device.service';
import { DieSvgLookupService } from './../../services/die-svg-lookup/die-svg-lookup.service';
import { DieModel } from './../../models/die.model';
import { Component, ComponentFactoryResolver, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DieSvgAnchorDirective } from '../../directives/die-svg-anchor.directive';
import { DieIcon } from '../die-icons/die-icon.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'die-svg-icon',
  templateUrl: './die-svg-icon.component.html',
  styleUrls: ['./die-svg-icon.component.css']
})
export class DieSvgIconComponent implements OnInit, OnDestroy {

  @ViewChild(DieSvgAnchorDirective, { static: true }) svgHost: DieSvgAnchorDirective;

  private _color: string;

  public get color(): string {
    return this._color;
  }
  @Input()
  public set color(value: string) {
    this._color = value;
    if (this.die) {
      this.setIconAndColor(this.black);
    }
  }
  private black: string = "#000000";

  @Input()
  public canReserve: boolean;

  private _die: DieModel;
  @Input()
  public set die(value: DieModel) {
    this._die = value;
    this.setIconAndColor(this.black);
  }
  public get die() {
    return this._die;
  }
  private isMobileDevice: boolean = false;

  private timeOfLastClick: number;

  private mouseInside: boolean = false;

  private destroyed: Subject<unknown>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private dieSvgLookupService: DieSvgLookupService,
    private isMobileDeviceService: IsMobileDeviceService
  ) { }

  ngOnInit() {
    this.setIconAndColor(this.black);

    this.destroyed = new Subject();
    this.isMobileDeviceService.isMobileDevice()
      .pipe(takeUntil(this.destroyed))
      .subscribe((value: boolean) => {
        this.isMobileDevice = value;
      });

    this.timeOfLastClick = Date.parse('1970-01-01T00:00:00Z');
  }

  ngOnDestroy() {
    this.destroyed.next();
  }

  toggleReserved() {
    const now = new Date().getTime();
    const enoughTimeHasPassedSinceLastClick = now - this.timeOfLastClick >= 200;

    if (this.canReserve && enoughTimeHasPassedSinceLastClick) {
      this.die.toggleReserved();
      this.timeOfLastClick = now;
    }
    this.setIconAndColor(this.color);
  }

  mouseEnter() {
    if (!this.mouseInside) {
      this.mouseInside = true;
      if (!this.isMobileDevice) {
        this.setIconAndColor(this.color);
      }
    }
  }

  mouseLeave() {
    if (this.mouseInside) {
      this.mouseInside = false;
      if (!this.isMobileDevice) {
        this.setIconAndColor(this.black);
      }
    }
  }

  private setIconAndColor(unreservedColor: string) {
    const svgIconClass = this.dieSvgLookupService.getSvgComponent(this.die);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(svgIconClass);

    const viewContainerRef = this.svgHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<DieIcon>(componentFactory);
    if (!this.die.isReserved) {
      componentRef.instance.color = unreservedColor;
    } else {
      componentRef.instance.color = this.color;
    }
  }

}
