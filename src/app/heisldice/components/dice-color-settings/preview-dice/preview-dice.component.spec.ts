import { Component, DebugElement, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DieModel } from 'src/app/heisldice/models/die.model';

import { PreviewDiceComponent } from './preview-dice.component';

describe('PreviewDiceComponent', () => {
  let component: PreviewDiceComponent;
  let fixture: ComponentFixture<PreviewDiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreviewDiceComponent, MockDieComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewDiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show a row of dice bound to the color input', () => {
    const selectedColor = "#32ffa3";
    component.color = selectedColor;
    fixture.detectChanges();

    const dice: DebugElement[] = fixture.debugElement.queryAll(By.directive(MockDieComponent));

    expect(dice.length).toEqual(6);
    for (let index = 0; index < dice.length; index++) {
      const die: MockDieComponent = dice[index].componentInstance;
      expect(die.color).toEqual(selectedColor);
    }
  });
});



@Component({
  selector: 'die-svg-icon',
  template: ''
})
class MockDieComponent {
  @Input() public die: DieModel;
  @Input() public color: string;
  @Input() public canReserve: boolean;

  constructor() { }
}