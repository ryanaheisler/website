import { Component, Input } from '@angular/core';
import { DieModel } from 'src/app/heisldice/models/die.model';

@Component({
  selector: 'preview-dice',
  templateUrl: './preview-dice.component.html',
  styleUrls: ['./preview-dice.component.css']
})
export class PreviewDiceComponent {
  
  public dice: Array<DieModel>;
  @Input() color: string;

  constructor() {
    this.dice = [1, 2, 3, 4, 5, 6].map((value: number) => {
      return new DieModel(value, true);
    });
  }
}
