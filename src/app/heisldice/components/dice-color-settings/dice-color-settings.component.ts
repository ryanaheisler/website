import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DieModel } from '../../models/die.model';
import { SettingsStorageService } from '../../services/settings-storage/settings-storage.service';

@Component({
  selector: 'dice-color-settings',
  templateUrl: './dice-color-settings.component.html',
  styleUrls: ['./dice-color-settings.component.css']
})
export class DiceColorSettingsComponent implements OnInit, OnDestroy {
  
  private $destroy: Subject<any>;

  private _selectedColor: string;
  public get selectedColor(): string {
    return this._selectedColor || "#76448a";
  }
  public set selectedColor(value: string) {
    this._selectedColor = value;
    this.settingsStorageService.storeUserDiceColor(this.selectedColor);
  }

  constructor(private settingsStorageService: SettingsStorageService) {
  }

  ngOnInit() {
    this.$destroy = new Subject();
    this.settingsStorageService.subscribeToUserDiceColor().pipe(takeUntil(this.$destroy)).subscribe((value: string) => {
      this._selectedColor = value;
    });
  }

  ngOnDestroy() {
    this.$destroy.next();
  }

  public colorSelected(color: string) {
    this.selectedColor = color;
  }

  public previewColor(color: string) {
    this._selectedColor = color;
  }
}
