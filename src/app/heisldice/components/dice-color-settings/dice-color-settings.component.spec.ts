import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { TestHelper } from 'src/test/test-helper';
import * as td from 'testdouble';
import { SettingsStorageService } from '../../services/settings-storage/settings-storage.service';

import { DiceColorSettingsComponent } from './dice-color-settings.component';

describe('DiceColorSettingComponent', () => {
  let component: DiceColorSettingsComponent;
  let fixture: ComponentFixture<TestParentComponent>;
  let colorInput: HTMLInputElement;
  let colorSettingSubject: Subject<string>;
  let mockSettingsStorage: SettingsStorageService;

  beforeEach(async(() => {
    mockSettingsStorage = td.object(SettingsStorageService.prototype);
    colorSettingSubject = new Subject();

    td.when(mockSettingsStorage.subscribeToUserDiceColor()).thenReturn(colorSettingSubject);
    TestBed.configureTestingModule({
      declarations: [
        DiceColorSettingsComponent,
        TestParentComponent,
        MockPreviewDiceComponent
      ],
      providers: [
        { provide: SettingsStorageService, useValue: mockSettingsStorage }
      ],
      imports: [
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestParentComponent);
    component = fixture.debugElement.query(By.directive(DiceColorSettingsComponent)).componentInstance;
    fixture.detectChanges();
    colorInput = fixture.nativeElement.querySelector('input');
  });

  it('should have a label for the color input', () => {
    const label: HTMLLabelElement = fixture.nativeElement.querySelector("label");
    expect(label.getAttribute("for")).toEqual("color-picker");
    expect(label.textContent).toEqual("Click the dice to choose your dice color");
  });
  it('should have a color input bound to the component\'s selected color', async () => {
    const selectedColor = "#32ffa3";
    component.selectedColor = selectedColor;
    fixture.detectChanges();
    await fixture.whenStable();
    expect(colorInput.value).toEqual(selectedColor);
    expect(colorInput.name).toEqual("color-picker");
    expect(colorInput.id).toEqual("color-picker");
  });
  it('should show a row of dice bound to the selected color from the color input', () => {
    const selectedColor = "#32ffa3";
    colorInput.value = selectedColor;
    colorInput.dispatchEvent(new Event("change"));
    fixture.detectChanges();

    const preview: MockPreviewDiceComponent = fixture.debugElement.query(By.directive(MockPreviewDiceComponent)).componentInstance;
    expect(preview.color).toEqual(selectedColor);
  });
  it('should show the dice preview in the default purple if no color is selected', () => {
    colorSettingSubject.next(undefined);
    fixture.detectChanges();

    const preview: MockPreviewDiceComponent = fixture.debugElement.query(By.directive(MockPreviewDiceComponent)).componentInstance;
    expect(preview.color).toEqual('#76448a');
  });
  it('should show the dice preview in the currently saved color from local storage', async () => {
    const savedColor = "#32ffa3";
    colorSettingSubject.next(savedColor);

    fixture.detectChanges();
    await fixture.whenStable();
    expect(colorInput.value).toEqual(savedColor);

    const preview: MockPreviewDiceComponent = fixture.debugElement.query(By.directive(MockPreviewDiceComponent)).componentInstance;
    expect(preview.color).toEqual(savedColor);
  });
  describe('Selecting a color', () => {
    it('should emit the currently selected color to a parent', () => {
      const selectedColor = "#32ffa3";
      colorInput.value = selectedColor;
      colorInput.dispatchEvent(new Event("change"));

      td.verify(mockSettingsStorage.storeUserDiceColor(selectedColor));
      TestHelper.expectNothing();
    });
    it('should change the dice color but not emit color to parent on input event', () => {
      const selectedColor = "#32ffa3";
      colorInput.value = selectedColor;
      colorInput.dispatchEvent(new Event("input"));

      fixture.detectChanges();

      td.verify(mockSettingsStorage.storeUserDiceColor(selectedColor), { times: 0 });

      const preview: MockPreviewDiceComponent = fixture.debugElement.query(By.directive(MockPreviewDiceComponent)).componentInstance;
      expect(preview.color).toEqual(selectedColor);
    });
  });
});

@Component({
  selector: 'preview-dice',
  template: ''
})
class MockPreviewDiceComponent {
  @Input() color: string;
  constructor() { }
}

@Component({
  selector: '',
  template: '<dice-color-settings></dice-color-settings>'
})
class TestParentComponent {
  constructor() { }
}
