import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { Color } from 'src/app/heisldice/models/color.model';
import { SettingsStorageService } from 'src/app/heisldice/services/settings-storage/settings-storage.service';
import * as td from 'testdouble';
import { FallbackDiceColorSettingsComponent } from './fallback-dice-color-settings.component';

describe('FallbackDiceColorSettingsComponent', () => {
  let component: FallbackDiceColorSettingsComponent;
  let fixture: ComponentFixture<FallbackDiceColorSettingsComponent>;
  let mockSettingsStorage: SettingsStorageService;
  let colorSettingSubject: Subject<string>;
  let previewDice: MockPreviewDiceComponent;
  let hexInput: HTMLInputElement;
  let rgbInputRed: HTMLInputElement, rgbInputGreen: HTMLInputElement, rgbInputBlue: HTMLInputElement
  let namedColorInput: HTMLInputElement;
  let savedColor: string

  beforeEach(async(() => {
    colorSettingSubject = new Subject();
    mockSettingsStorage = td.object(SettingsStorageService.prototype);
    td.when(mockSettingsStorage.subscribeToUserDiceColor()).thenReturn(colorSettingSubject);
    savedColor = "#32ffa3";

    TestBed.configureTestingModule({
      declarations: [FallbackDiceColorSettingsComponent, MockPreviewDiceComponent],
      providers: [
        { provide: SettingsStorageService, useValue: mockSettingsStorage }
      ],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FallbackDiceColorSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    previewDice = fixture.debugElement.query(By.directive(MockPreviewDiceComponent)).componentInstance;
    hexInput = fixture.nativeElement.querySelector("input#hex-color-input");

    rgbInputRed = fixture.nativeElement.querySelector("input#rgb-color-input-red");
    rgbInputGreen = fixture.nativeElement.querySelector("input#rgb-color-input-green");
    rgbInputBlue = fixture.nativeElement.querySelector("input#rgb-color-input-blue");

    namedColorInput = fixture.nativeElement.querySelector("input#named-color-input");
  });

  it('should show the dice preview in the default purple if no color is selected', () => {
    colorSettingSubject.next(undefined);
    fixture.detectChanges();

    expect(previewDice.color).toEqual('#76448a');
  });
  it('should show the dice preview in the currently saved color from local storage', async () => {
    colorSettingSubject.next(savedColor);

    fixture.detectChanges();

    expect(previewDice.color).toEqual(savedColor);
  });
  it('should have form instructions before the form with links', () => {
    const formInstructions = fixture.nativeElement.querySelectorAll('p');
    expect(formInstructions[0].textContent).toEqual(
      "Please fill out the form below to choose a color for your dice. You only have to fill out a hex color, or a color name, or red, green, and blue color values.");
    expect(formInstructions[1].textContent).toEqual(
      "You can use Google's Color Picker to choose a color. It will give you the Hex value of your color under \"HEX\" starting with \"#\". You can enter it with or without the \"#\".");
    expect(formInstructions[2].textContent).toEqual(
      "Google's Color picker will also show you your color's RGB values under \"RGB\", which are 3 numbers between 0 and 255. Enter the numbers in the Red, Green, and Blue fields in the order they appear in Google.");
    expect(formInstructions[3].textContent).toEqual(
      "You can find a list of color names on Wikipedia. Enter the color's name with or without spaces or capital letters.");
    expect(formInstructions[4].textContent).toEqual(
      "The dice below show a preview of your selected color.");

    const anchors = fixture.nativeElement.querySelectorAll('a');
    expect(anchors[0].href).toEqual("https://www.google.com/search?q=color+picker");
    expect(anchors[0].textContent).toEqual("Google's Color Picker");
    expect(anchors[0].target).toEqual("_blank");
    expect(anchors[1].href).toEqual("https://en.wikipedia.org/wiki/Web_colors#Extended_colors");
    expect(anchors[1].textContent).toEqual("Wikipedia");
    expect(anchors[1].target).toEqual("_blank");
  });
  describe('form', () => {
    describe('hex input', () => {
      it('should show a text input with a label that has the value from local storage if present', () => {
        const hexLabel: HTMLLabelElement = fixture.nativeElement.querySelector("label#hex-color-input-label");

        expect(hexInput).toBeTruthy('hex input is not present');
        expect(hexInput.placeholder).toEqual('e.g. #7D9BAA');
        expect(hexLabel.getAttribute('for')).toEqual(hexInput.id);
        expect(hexLabel.textContent).toEqual("Hex color:");

        colorSettingSubject.next(savedColor);
        fixture.detectChanges();
        expect(hexInput.value).toEqual(savedColor);
      });
      const hexColorTestCases = [
        { inputColor: "#ff5214", expectedColor: "#ff5214" },
        { inputColor: "0021f2", expectedColor: "#0021f2" },
        { inputColor: "#0", expectedColor: "#ffffff" },
        { inputColor: "#05", expectedColor: "#ffffff" },
        { inputColor: "#05f", expectedColor: "#ffffff" },
        { inputColor: "#05f0", expectedColor: "#ffffff" },
        { inputColor: "#05f09", expectedColor: "#ffffff" },
        { inputColor: "#6632f10", expectedColor: "#ffffff" },
        { inputColor: "0#6632f0", expectedColor: "#ffffff" },
        { inputColor: "710000a", expectedColor: "#ffffff" },
      ];
      hexColorTestCases.forEach((testCase) => {
        it(`should change preview dice color based on value entered - ${testCase.inputColor}`, () => {
          colorSettingSubject.next("#ffffff");
          fixture.detectChanges();

          hexInput.value = testCase.inputColor;
          hexInput.dispatchEvent(new Event("input"));

          fixture.detectChanges();

          expect(component.selectedColor).toEqual(testCase.expectedColor);
          expect(previewDice.color).toEqual(testCase.expectedColor);
        });
      });
      it('should affect the values in the other inputs', () => {
        hexInput.value = "#bc8f8f";
        hexInput.dispatchEvent(new Event("input"));
        fixture.detectChanges();

        expect(rgbInputRed.value).toEqual("188");
        expect(rgbInputGreen.value).toEqual("143");
        expect(rgbInputBlue.value).toEqual("143");
        expect(namedColorInput.value).toEqual("Rosy Brown");
      });
    });
    describe('rgb inputs', () => {
      it('should show 3 text inputs with labels that have the value from local storage if present', () => {
        const rgbLabelRed: HTMLLabelElement = fixture.nativeElement.querySelector("label#rgb-color-input-red-label");
        const rgbLabelGreen: HTMLLabelElement = fixture.nativeElement.querySelector("label#rgb-color-input-green-label");
        const rgbLabelBlue: HTMLLabelElement = fixture.nativeElement.querySelector("label#rgb-color-input-blue-label");

        expect(rgbInputRed).toBeTruthy('red input is not present');
        expect(rgbInputGreen).toBeTruthy('green input is not present');
        expect(rgbInputBlue).toBeTruthy('blue input is not present');

        expect(rgbInputRed.placeholder).toEqual('0-255');
        expect(rgbInputRed.getAttribute('inputmode')).toEqual('numeric');

        expect(rgbInputGreen.placeholder).toEqual('0-255');
        expect(rgbInputGreen.getAttribute('inputmode')).toEqual('numeric');

        expect(rgbInputBlue.placeholder).toEqual('0-255');
        expect(rgbInputBlue.getAttribute('inputmode')).toEqual('numeric');

        expect(rgbLabelRed.getAttribute('for')).toEqual(rgbInputRed.id);
        expect(rgbLabelRed.textContent).toEqual("Red:");

        expect(rgbLabelGreen.getAttribute('for')).toEqual(rgbInputGreen.id);
        expect(rgbLabelGreen.textContent).toEqual("Green:");

        expect(rgbLabelBlue.getAttribute('for')).toEqual(rgbInputBlue.id);
        expect(rgbLabelBlue.textContent).toEqual("Blue:");

        colorSettingSubject.next(savedColor);
        fixture.detectChanges();

        const savedColorModel = Color.fromHexString(savedColor);
        expect(rgbInputRed.value).toEqual(`${savedColorModel.red}`);
        expect(rgbInputGreen.value).toEqual(`${savedColorModel.green}`);
        expect(rgbInputBlue.value).toEqual(`${savedColorModel.blue}`);
      });
      const rgbColorTestCases = [
        { inputRed: "254", inputGreen: "128", inputBlue: "64", expectedColor: "#fe8040" },
        { inputRed: "", inputGreen: "128", inputBlue: "64", expectedColor: "#ff8040" },
        { inputRed: "254", inputGreen: "", inputBlue: "64", expectedColor: "#feff40" },
        { inputRed: "254", inputGreen: "128", inputBlue: "", expectedColor: "#fe80ff" },
        { inputRed: "a", inputGreen: "128", inputBlue: "64", expectedColor: "#ff8040" },
        { inputRed: "254", inputGreen: "b", inputBlue: "64", expectedColor: "#feff40" },
        { inputRed: "254", inputGreen: "128", inputBlue: "c", expectedColor: "#fe80ff" },
        { inputRed: "-1", inputGreen: "128", inputBlue: "64", expectedColor: "#ff8040" },
        { inputRed: "254", inputGreen: "256", inputBlue: "64", expectedColor: "#feff40" },
        { inputRed: "254", inputGreen: "128", inputBlue: "64.9", expectedColor: "#fe8040" },
        { inputRed: "254", inputGreen: "128.1", inputBlue: "64", expectedColor: "#fe8040" },
        { inputRed: "254.5", inputGreen: "128", inputBlue: "64", expectedColor: "#fe8040" },
      ];
      rgbColorTestCases.forEach((testCase) => {
        it(`should change preview dice color based on value entered - ${testCase.inputRed}, ${testCase.inputGreen}, ${testCase.inputBlue}`, () => {
          const previousColor = new Color(255, 255, 255);
          colorSettingSubject.next(previousColor.toHexString());
          fixture.detectChanges();

          rgbInputRed.value = testCase.inputRed.toString();
          rgbInputGreen.value = testCase.inputGreen.toString();
          rgbInputBlue.value = testCase.inputBlue.toString();

          rgbInputRed.dispatchEvent(new Event("input"));
          fixture.detectChanges();
          const redIntermediate = new Color(
            getNumberForRgbTest(testCase.inputRed),
            previousColor.green,
            previousColor.blue
          ).toHexString();
          expect(component.selectedColor).toEqual(redIntermediate);
          expect(previewDice.color).toEqual(redIntermediate);

          rgbInputGreen.dispatchEvent(new Event("input"));
          fixture.detectChanges();
          const greenIntermediate = new Color(
            getNumberForRgbTest(testCase.inputRed),
            getNumberForRgbTest(testCase.inputGreen),
            previousColor.blue).toHexString();
          expect(component.selectedColor).toEqual(greenIntermediate);
          expect(previewDice.color).toEqual(greenIntermediate);

          rgbInputBlue.dispatchEvent(new Event("input"));
          fixture.detectChanges();
          expect(component.selectedColor).toEqual(testCase.expectedColor);
          expect(previewDice.color).toEqual(testCase.expectedColor);
        });
      });
      it('should affect the values in the other inputs', () => {
        rgbInputRed.value = "218";
        rgbInputRed.dispatchEvent(new Event("input"));
        rgbInputGreen.value = "165";
        rgbInputGreen.dispatchEvent(new Event("input"));
        rgbInputBlue.value = "32";
        rgbInputBlue.dispatchEvent(new Event("input"));
        fixture.detectChanges();

        expect(hexInput.value).toEqual("#daa520");
        expect(namedColorInput.value).toEqual("Goldenrod");
      });
    });
    describe('named color input', () => {
      it('should show a text input with a label', () => {
        const namedColorLabel: HTMLLabelElement = fixture.nativeElement.querySelector("label#named-color-input-label");

        expect(namedColorInput).toBeTruthy('named-color input is not present');
        expect(namedColorInput.placeholder).toEqual('e.g. Dark Cyan');
        expect(namedColorLabel.getAttribute('for')).toEqual(namedColorInput.id);
        expect(namedColorLabel.textContent).toEqual("Color Name:");
      });
      const previouslySavedNamedColorTestCases = [
        { colorName: "Pink", colorValue: "#ffc0cb" },
        { colorName: "Dark Cyan", colorValue: "#008b8b" },
        { colorName: "Dark Olive Green", colorValue: "#556b2f" },
        { colorName: "Dark Slate Blue", colorValue: "#483d8b" },
        { colorName: "Antique White", colorValue: "#faebd7" },
        { colorName: "", colorValue: "#ffc0ca" },
        { colorName: "", colorValue: "#008b8a" },
        { colorName: "", colorValue: "#556b2e" },
        { colorName: "", colorValue: "#faebd8" },
      ];
      previouslySavedNamedColorTestCases.forEach(testCase => {
        it(`should show the color name from local storage - ${testCase.colorValue} -> ${testCase.colorName || "None"}`, () => {
          colorSettingSubject.next(testCase.colorValue);
          fixture.detectChanges();
          expect(namedColorInput.value).toEqual(testCase.colorName);
        });
      });
      const typingNamedColorTestCases = [
        { inputColor: "Antique White", expectedColor: "#faebd7" },
        { inputColor: "aNtiQue WHite", expectedColor: "#faebd7" },
        { inputColor: "xAntique White", expectedColor: "#ffffff" },
        { inputColor: "Chartreuse", expectedColor: "#7fff00" },
        { inputColor: "Chartreuse ", expectedColor: "#7fff00" },
        { inputColor: "chartreuse", expectedColor: "#7fff00" },
        { inputColor: "Deep Sky Blue", expectedColor: "#00bfff" },
        { inputColor: " Deep Sky Blue", expectedColor: "#00bfff" },
        { inputColor: "deepsky blue", expectedColor: "#00bfff" },
        { inputColor: "Light Blue", expectedColor: "#add8e6" },
        { inputColor: "Light     Blue", expectedColor: "#add8e6" },
        { inputColor: "lightblue", expectedColor: "#add8e6" },
        { inputColor: "Papaya Whip", expectedColor: "#ffefd5" },
        { inputColor: "papaya whip", expectedColor: "#ffefd5" },
        { inputColor: "Teal", expectedColor: "#008080" },
        { inputColor: "teal", expectedColor: "#008080" },
        { inputColor: "Tea", expectedColor: "#ffffff" },
        { inputColor: "Garbledina", expectedColor: "#ffffff" },
        { inputColor: "#fa092e", expectedColor: "#ffffff" },
        { inputColor: "rgb(125, 22, 98)", expectedColor: "#ffffff" },
      ];
      typingNamedColorTestCases.forEach(testCase => {
        it(`should change preview dice color based on value entered - ${testCase.inputColor}`, () => {
          colorSettingSubject.next("#ffffff");
          fixture.detectChanges();

          namedColorInput.value = testCase.inputColor;
          namedColorInput.dispatchEvent(new Event("input"));

          fixture.detectChanges();

          expect(component.selectedColor).toEqual(testCase.expectedColor);
          expect(previewDice.color).toEqual(testCase.expectedColor);
        });
      });
      it('should affect the values in the other inputs', () => {
        namedColorInput.value = "Medium Aquamarine";
        namedColorInput.dispatchEvent(new Event("input"));
        fixture.detectChanges();

        expect(rgbInputRed.value).toEqual("102");
        expect(rgbInputGreen.value).toEqual("205");
        expect(rgbInputBlue.value).toEqual("170");
        expect(hexInput.value).toEqual("#66cdaa");
      });
    });
    describe('submit', () => {
      it('should save the selected color to local storage', () => {
        const submitButton: HTMLInputElement = fixture.nativeElement.querySelector('input#submit-dice-color');

        expect(submitButton.type).toEqual('submit');
        expect(submitButton.value).toEqual('Save Color');

        const selectedColor = "#fa3189";
        component.selectedColor = selectedColor;

        submitButton.click();

        td.verify(mockSettingsStorage.storeUserDiceColor(selectedColor));
      });
    });
  });
});

@Component({
  selector: 'preview-dice',
  template: ''
})
class MockPreviewDiceComponent {
  @Input() color: string;
  constructor() { }
}

function getNumberForRgbTest(value: string) {
  const numericValue = parseInt(value);

  if (isNaN(numericValue)) return 255;
  if (numericValue < 0) return 255;
  if (numericValue > 255) return 255;

  return numericValue;
}