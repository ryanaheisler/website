import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Color } from 'src/app/heisldice/models/color.model';
import { SettingsStorageService } from 'src/app/heisldice/services/settings-storage/settings-storage.service';
import { object } from 'testdouble';
import webColors from '../../../../../assets/web-colors.json';

@Component({
  selector: 'fallback-dice-color-settings',
  templateUrl: './fallback-dice-color-settings.component.html',
  styleUrls: ['./fallback-dice-color-settings.component.css']
})
export class FallbackDiceColorSettingsComponent implements OnInit, OnDestroy {

  private $destroy: Subject<any>;

  private _selectedColor: string;
  public get selectedColor(): string {
    return this._selectedColor || "#76448a";
  }
  public set selectedColor(value: string) {
    this._selectedColor = value;
  }

  public get selectedColorRed() {
    return Color.fromHexString(this.selectedColor).red;
  }
  public get selectedColorGreen() {
    return Color.fromHexString(this.selectedColor).green;
  }
  public get selectedColorBlue() {
    return Color.fromHexString(this.selectedColor).blue;
  }

  public get selectedColorName(): string {
    const colorNames = Object.keys(webColors);
    const colorValues = Object.values(webColors);

    const index = colorValues.findIndex((value: string) => {
      return value === this._selectedColor;
    });
    
    return colorNames[index] || '';
  }

  constructor(private settingsStorageService: SettingsStorageService) {
  }

  ngOnInit() {
    this.$destroy = new Subject();
    this.settingsStorageService.subscribeToUserDiceColor().pipe(takeUntil(this.$destroy)).subscribe((value: string) => {
      this.selectedColor = value;
    });
  }

  ngOnDestroy() {
    this.$destroy.next();
  }

  public hexInput(value: string) {
    const threeByteHexNumber = /^#?[0-9a-f]{6}$/;
    if (threeByteHexNumber.test(value)) {
      this.selectedColor = Color.fromHexString(value).toHexString();
    }
  }

  public rgbInput(value: string, color: string) {
    let numericValue = parseInt(value);
    let shouldUseNewValue = true;
    if (isNaN(numericValue)) shouldUseNewValue = false;
    if ((numericValue < 0) || (numericValue > 255)) shouldUseNewValue = false;

    let valueToUse: number;
    switch (color) {
      case "red":
        valueToUse = shouldUseNewValue ? numericValue : this.selectedColorRed;
        this.selectedColor = new Color(valueToUse, this.selectedColorGreen, this.selectedColorBlue).toHexString();
        break;
      case "green":
        valueToUse = shouldUseNewValue ? numericValue : this.selectedColorGreen;
        this.selectedColor = new Color(this.selectedColorRed, valueToUse, this.selectedColorBlue).toHexString();
        break;
      case "blue":
        valueToUse = shouldUseNewValue ? numericValue : this.selectedColorBlue;
        this.selectedColor = new Color(this.selectedColorRed, this.selectedColorGreen, valueToUse).toHexString();
        break;

      default:
        break;
    }
  }

  public namedColorInput(value: string) {
    const colorNames = Object.keys(webColors).map((name: string) => {
      return name.toLowerCase().replace(/ /g, "");
    });

    const sanitizedInput = value.toLowerCase().replace(/ /g, "");
    const index = colorNames.findIndex((name: string) => {
      return name === sanitizedInput;
    });
    
    if (index >= 0) {
      this.selectedColor = Object.values(webColors)[index];
    }
  }

  public submitDiceColor() {
    this.settingsStorageService.storeUserDiceColor(this.selectedColor);
  }
}
