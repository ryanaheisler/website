import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'start-game-modal',
  templateUrl: './start-game-modal.component.html',
  styleUrls: ['./start-game-modal.component.css']
})
export class StartGameModalComponent implements OnInit {

  @Input()
  modalData: { gameID: string };
  public gameID: string;
  public LINE1: string = 'Thanks for playing Heisldice! This is your first time starting a game. Your game ID is:'
  public LINE2: string = 'Give this ID to your friends and tell them to enter it in the "Join Game" section of this page.'
  public LINE3: string = 'Your game ID will be displayed in the top-right corner of the next page if you need it later. Have fun!'

  constructor() { }

  ngOnInit() {
    this.gameID = this.modalData.gameID;
  }

}
