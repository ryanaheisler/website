import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StartGameModalComponent } from './start-game-modal.component';


describe('StartGameModalComponent', () => {
  let component: StartGameModalComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StartGameModalComponent, TestHostComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should show a message about the game', () => {
    const lines: HTMLSpanElement[] = fixture.debugElement.nativeElement.querySelectorAll('p');
    const gameIDTitle: HTMLHeadingElement = fixture.debugElement.nativeElement.querySelector('h1');

    expect(lines[0].innerText).toEqual(component.LINE1);
    expect(lines[1].innerText).toEqual(component.LINE2);
    expect(lines[2].innerText).toEqual(component.LINE3);
    expect(gameIDTitle.textContent).toEqual(component.gameID)
  });
});

@Component({
  selector: 'test-host',
  template: '<start-game-modal [modalData]=data></start-game-modal>'
})
class TestHostComponent {
  public data: { gameID: string } = { gameID: "1234" }
}
