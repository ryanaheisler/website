import { SettingsStorageService } from './../../services/settings-storage/settings-storage.service';
import { PlayerDataFromBackend } from './../../services/player/player.service';
import { GameObjectFromServer } from './../../models/game.model';
import { ToastrService } from 'ngx-toastr';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import * as td from 'testdouble';
import { DEFAULT_PLAYER, Player } from '../../models/player.model';
import { Scorecard } from '../../models/scorecard.model';
import { PlayerService } from '../../services/player/player.service';
import { ScoreService } from '../../services/score/score.service';
import { Category } from './../../models/category.model';
import { ScorecardFactory } from './../../models/scorecard-factory';
import { ScorecardComponent } from './scorecard.component';
import { TestHelper } from 'src/test/test-helper';

describe('ScorecardComponent', () => {
  let component: ScorecardComponent;
  let fixture: ComponentFixture<ScorecardComponent>;
  let mockPlayerService: PlayerService;
  let mockScoreService: ScoreService;
  let scorecard: Scorecard;
  let potentialScorecard: Scorecard;
  let player: Player;
  let opponent: PlayerDataFromBackend;
  let game: GameObjectFromServer;
  let dice: number[];
  let category1: Category, category2: Category, category3: Category,
    potentialCategory1: Category, potentialCategory2: Category, potentialCategory3: Category;
  let playerObservable;
  let mockToastrService: ToastrService;
  let mockSettingsStorageService: SettingsStorageService;

  beforeEach(async(() => {

    category1 = new Category('two and a halfs', 6, false, true, 0);
    category2 = new Category('6 of a grind', 4, false, true, 0);
    category3 = new Category('bleebo!', 14, false, false, 1);

    scorecard = new Scorecard();
    scorecard.categories = [category1, category2, category3];
    scorecard.topSectionBonus = 13
    scorecard.topSectionTotal = 55
    scorecard.extraYahtzeeBonusTotal = 2
    scorecard.bottomSectionTotal = 124
    scorecard.total = 555

    potentialCategory1 = new Category('two and a halfs', null, false, true, 0);
    potentialCategory2 = new Category('6 of a grind', null, false, true, 0);
    potentialCategory3 = new Category('bleebo!', null, false, false, 1);

    potentialScorecard = new Scorecard()
    potentialScorecard.categories = [potentialCategory1, potentialCategory2, potentialCategory3];

    player = TestHelper.constructPlayer('Ryan', 2, 9);
    player.scorecard = new Scorecard();
    player.scorecard.categories[0] = new Category('knives', 1);
    playerObservable = of(player);

    opponent = {
      scorecard: {
        categories: {}
      },
      name: "Plokta",
      rollsRemaining: 3,
      turnsRemaining: 2,
      previousDice: [1, 2, 3, 4, 5],
      myTurn: false,
      diceColor: undefined
    }
    opponent.scorecard.categories[category1.description] = category1
    opponent.scorecard.categories[category2.description] = category2
    opponent.scorecard.categories[category3.description] = category3

    opponent.scorecard.topSectionBonus = 14
    opponent.scorecard.topSectionTotal = 56
    opponent.scorecard.extraYahtzeeBonusTotal = 3
    opponent.scorecard.bottomSectionTotal = 125
    opponent.scorecard.total = 556

    game = new GameObjectFromServer('IOSD', [player, opponent]);

    mockPlayerService = td.object(PlayerService.prototype);
    mockScoreService = td.object(ScoreService.prototype);
    mockToastrService = td.object(ToastrService.prototype);
    mockSettingsStorageService = td.object(SettingsStorageService.prototype);

    TestBed.configureTestingModule({
      declarations: [ScorecardComponent],
      providers: [
        { provide: PlayerService, useValue: mockPlayerService },
        { provide: ScoreService, useValue: mockScoreService },
        { provide: ToastrService, useValue: mockToastrService },
        { provide: SettingsStorageService, useValue: mockSettingsStorageService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorecardComponent);
    component = fixture.componentInstance;
    component.scorecard = scorecard;
    component.potentialScorecard = potentialScorecard;
    component.player = player;
    component.game = game;
    fixture.detectChanges();
  });

  it('should have opponents\' names in the table header', () => {
    const rows = fixture.debugElement.nativeElement.querySelectorAll('tr');
    const headers = rows[0].querySelectorAll('th');

    expect(headers.length).toEqual(3);
    expect(headers[0].innerText).toEqual('Categories');
    expect(headers[1].innerText).toEqual(player.name);
    expect(headers[2].innerText).toEqual(opponent.name);
  });

  it('should have correct text for each category on scorecard with scores', () => {
    const rows = fixture.debugElement.nativeElement.querySelectorAll('tr');

    expect(rows.length).toBe(9);

    checkRowForActualScore(rows[1], 0);
    checkRowForActualScore(rows[2], 1);

    let columns = rows[3].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual("Bonus for Top Section (Score 63 or over)");

    columns = rows[4].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual("Total for Top Section");

    checkRowForActualScore(rows[5], 2);

    columns = rows[6].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual("Extra Yahtzee Bonus (100 each)");

    columns = rows[7].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual("Total for Bottom Section");

    columns = rows[8].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual("Grand Total");
  });

  it('should have warning text for each category that is in danger', () => {
    category2.inDangerZeroScore = true;
    fixture.detectChanges();

    const rows = fixture.debugElement.nativeElement.querySelectorAll('tr');

    const columns = rows[2].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual("Are you sure?");
    expect(columns[1].innerText).toEqual("0");
    expect(columns[2].innerText).toEqual(opponent.scorecard.categories[1].scoreAsString);
  });

  it('should show totals when the player has a name', () => {
    fixture.detectChanges();

    const rows = fixture.debugElement.nativeElement.querySelectorAll('tr');

    let columns = rows[3].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[1].innerText).toEqual(scorecard.topSectionBonus.toString());
    expect(columns[2].innerText).toEqual(opponent.scorecard.topSectionBonus.toString());

    columns = rows[4].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[1].innerText).toEqual(scorecard.topSectionTotal.toString());
    expect(columns[2].innerText).toEqual(opponent.scorecard.topSectionTotal.toString());

    columns = rows[6].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[1].innerText).toEqual(scorecard.extraYahtzeeBonusTotal.toString());
    expect(columns[2].innerText).toEqual(opponent.scorecard.extraYahtzeeBonusTotal.toString());

    columns = rows[7].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[1].innerText).toEqual(scorecard.bottomSectionTotal.toString());
    expect(columns[2].innerText).toEqual(opponent.scorecard.bottomSectionTotal.toString());

    columns = rows[8].querySelectorAll('td');
    expect(columns.length).toBe(3);
    expect(columns[1].innerText).toEqual(scorecard.total.toString());
    expect(columns[2].innerText).toEqual(opponent.scorecard.total.toString());
  });

  describe('dice input', () => {
    it('should come from the parent', () => {
      expect(component.currentDice).toEqual(dice);
    });
    it('should reset danger to false on all categories', () => {
      component.scorecard.categories.forEach((category) => {
        category.inDangerZeroScore = true;
      });

      component.currentDice = [4, 4, 4, 4, 4];

      component.scorecard.categories.forEach((category) => {
        expect(category.inDangerZeroScore).toBe(false);
      });
    });
  });

  it('should display 0 for category that has a score of 0', () => {
    category1.score = 0;
    fixture.detectChanges();
    const rows = fixture.debugElement.nativeElement.querySelectorAll('tr');
    checkRowForActualScore(rows[1], 0);
  });

  describe('name input (player UNdefined)', () => {
    let newPlayerName: string;
    let gameID: string;
    beforeEach(() => {
      newPlayerName = 'Ryan';
      gameID = "4231"
      td.when(mockPlayerService.put(newPlayerName, gameID)).thenReturn(playerObservable)

      component.player = DEFAULT_PLAYER();
      fixture.detectChanges();
    });

    it('should check session storage for a player name and automatically submit it if there is one', (done) => {
      const playerName = "Greta Gumbo";
      td.when(mockPlayerService.put(playerName, game.gameID)).thenReturn(playerObservable)
      td.when(mockSettingsStorageService.getCurrentPlayerName()).thenReturn(playerName);

      component.playerUpdated.subscribe((data: Player) => {
        expect(component.scorecard.categories[0]).toBeDefined();
        expect(component.scorecard.categories[0].score).toEqual(1);
        expect(data).toEqual(player);
        done()
      });

      component.ngOnInit();

      expect(component.player.name).toEqual(playerName);
    });

    it('should have an input for player name if player name is undefined', () => {
      const nameLabel = fixture.debugElement.nativeElement.querySelector('label');
      expect(nameLabel.innerText).toContain('Enter name:');
      const nameInput = fixture.debugElement.nativeElement.querySelector('#name-input');
      expect(nameInput.placeholder).toEqual('Anonymous Heisler');
      const nameSpan = fixture.debugElement.nativeElement.querySelector('span');
      expect(nameSpan).toBeFalsy();
      const submitButton = fixture.debugElement.nativeElement.querySelector('#submitButton');
      expect(submitButton).toBeTruthy();
      const startButton = fixture.debugElement.nativeElement.querySelector('#startButton');
      expect(startButton).toBeFalsy();
    });

    it('should have a button that posts player to backend and emits player', (done) => {
      component.playerUpdated.subscribe((data: Player) => {
        expect(data).toEqual(player);
        done()
      });

      const nameInput = fixture.debugElement.nativeElement.querySelector('#name-input');
      let submitButton = fixture.debugElement.nativeElement.querySelector('#submitButton');
      expect(submitButton.innerText).toEqual('Submit');

      nameInput.value = '';
      fixture.detectChanges();
      submitButton = fixture.debugElement.nativeElement.querySelector('button');
      expect(submitButton.disabled).toBe(true);

      nameInput.value = newPlayerName;
      fixture.detectChanges();
      nameInput.dispatchEvent(new Event('input'));
      submitButton = fixture.debugElement.nativeElement.querySelector('button');
      expect(submitButton.disabled).toBe(false);

      component.game.gameID = gameID;

      submitButton.click();

      expect(component.scorecard.categories[0]).toBeDefined();
      expect(component.scorecard.categories[0].score).toEqual(1);
    });

    it('should have an input that posts player to backend and emits player on ENTER key press', (done) => {
      component.playerUpdated.subscribe((data: Player) => {
        expect(data).toEqual(player);
        done()
      });

      const nameInput = fixture.debugElement.nativeElement.querySelector('#name-input');

      nameInput.value = newPlayerName;
      fixture.detectChanges();
      nameInput.dispatchEvent(new Event('input'));

      component.game.gameID = gameID;
      nameInput.dispatchEvent(new KeyboardEvent('keyup', { key: "enter" }))
    });

    it('should show an informational toast if the player could not be posted', (done) => {
      component.playerUpdated.subscribe((data: Player) => {
        fail("this should not be called");
      });
      td.when(mockPlayerService.put(newPlayerName, gameID)).thenReturn(of(null));

      const nameInput = fixture.debugElement.nativeElement.querySelector('#name-input');

      nameInput.value = newPlayerName;
      fixture.detectChanges();
      nameInput.dispatchEvent(new Event('input'));
      const submitButton = fixture.debugElement.nativeElement.querySelector('button');

      component.game.gameID = gameID;

      submitButton.click();

      setTimeout(() => {
        td.verify(mockToastrService.error("That name cannot be used. Please choose another.", "INVALID NAME", { positionClass: "toast-center-center" }));
        done();
      });
      expect().nothing();
    });

    it('should only allow letters and space', () => {
      const nameInput = fixture.debugElement.nativeElement.querySelector('#name-input');
      nameInput.value = 'Ryan';
      nameInput.dispatchEvent(new Event('input'));
      expect(nameInput.value).toEqual('Ryan');

      nameInput.value = 'R3y 7a1n4*201$@*(!';
      nameInput.dispatchEvent(new Event('input'));
      expect(nameInput.value).toEqual('Ry an');
    });
  });

  describe('name input (player defined)', () => {
    it('should have text for player name if player name is defined', () => {
      const nameLabel = fixture.debugElement.nativeElement.querySelector('label');
      expect(nameLabel).toBeFalsy();
      const nameInput = fixture.debugElement.nativeElement.querySelector('#name-input');
      expect(nameInput).toBeFalsy();
      const nameSpan = fixture.debugElement.nativeElement.querySelector('span');
      expect(nameSpan.innerText).toEqual(player.name);
      const submitButton = fixture.debugElement.nativeElement.querySelector('#submitButton');
      expect(submitButton).toBeFalsy();
    });
  });

  describe('clicking a row in the scorecard', () => {

    let rows: HTMLTableRowElement[];
    let gameID: string;

    beforeEach(() => {
      gameID = "9103"
      component.game.gameID = gameID

      scorecard.categories[1].score = null;
      potentialScorecard.categories[1].score = 15;

      fixture.detectChanges();
      rows = fixture.debugElement.nativeElement.querySelectorAll('tr');
    });

    it('clear storage when the player submits their final score', (done) => {
      player.turnsRemaining = 0;
      td.when(mockScoreService.setScore(player, gameID, scorecard.categories[1], dice)).thenReturn(playerObservable);

      component.playerUpdated.subscribe((data: Player) => {
        expect(data).toEqual(player);
        expect(component.scorecard).toEqual(new ScorecardFactory().build(player.scorecard));
        td.verify(mockSettingsStorageService.unsetCurrentGameID());
        td.verify(mockSettingsStorageService.unsetCurrentPlayerName());
        done();
      });

      rows[2].click();
    });

    it('should call the score service and emit returned player - score null', (done) => {
      td.when(mockScoreService.setScore(player, gameID, scorecard.categories[1], dice)).thenReturn(playerObservable);

      component.playerUpdated.subscribe((data: Player) => {
        expect(data).toEqual(player);
        expect(component.scorecard).toEqual(new ScorecardFactory().build(player.scorecard));
        td.verify(mockSettingsStorageService.unsetCurrentGameID(), { times: 0 });
        td.verify(mockSettingsStorageService.unsetCurrentPlayerName(), { times: 0 });
        done();
      });

      rows[2].click();
    });

    it('should call the score service and emit returned player - score undefined', (done) => {
      td.when(mockScoreService.setScore(player, gameID, scorecard.categories[1], dice)).thenReturn(playerObservable);
      scorecard.categories[1].score = undefined;

      component.playerUpdated.subscribe((data: Player) => {
        expect(data).toEqual(player);
        expect(component.scorecard).toEqual(new ScorecardFactory().build(player.scorecard));
        done();
      });

      rows[2].click();
    });

    it('should call the score service and emit returned player - potential score 0, category in danger', (done) => {
      td.when(mockScoreService.setScore(player, gameID, scorecard.categories[1], dice)).thenReturn(playerObservable);
      scorecard.categories[1].score = null;
      scorecard.categories[1].inDangerZeroScore = true;
      scorecard.categories[0].inDangerZeroScore = true;
      scorecard.categories[2].inDangerZeroScore = true;
      potentialScorecard.categories[1].score = 0;

      component.playerUpdated.subscribe((data: Player) => {
        expect(data).toEqual(player);
        expect(component.scorecard).toEqual(new ScorecardFactory().build(player.scorecard));
        done();
      });

      rows[2].click();
    });

    it('should NOT call the score service if that category has a potential score of 0 and is not in danger', () => {
      scorecard.categories[1].inDangerZeroScore = false;
      scorecard.categories[2].inDangerZeroScore = true;
      potentialScorecard.categories[1].score = 0;
      component.playerUpdated.subscribe((data: Player) => {
        fail('player should not have been updated')
      });

      rows[2].click();

      td.verify(mockScoreService.setScore(td.matchers.anything(), td.matchers.anything(), td.matchers.anything(), td.matchers.anything()), { times: 0 });
      expect(scorecard.categories[1].inDangerZeroScore).toBe(true);
      expect(scorecard.categories[2].inDangerZeroScore).toBe(false);
    });

    it('should NOT call the score service if that category already has a score', () => {
      component.playerUpdated.subscribe((data: Player) => {
        fail('player should not have been updated')
      });

      rows[1].click();

      td.verify(mockScoreService.setScore(td.matchers.anything(), td.matchers.anything(), td.matchers.anything(), td.matchers.anything()), { times: 0 });
      expect().nothing();
    });

    it('should NOT call the score service if that category already has a score of 0', () => {
      component.playerUpdated.subscribe((data: Player) => {
        fail('player should not have been updated')
      });

      scorecard.categories[0].score = 0;
      rows[1].click();

      td.verify(mockScoreService.setScore(td.matchers.anything(), td.matchers.anything(), td.matchers.anything(), td.matchers.anything()), { times: 0 });
      expect().nothing();
    });

    it('should display an alert on error', () => {
      const mockObservable1 = td.object(['pipe']);
      const mockObservable2 = td.object(['subscribe']);

      td.when(mockObservable1.pipe(td.matchers.anything())).thenReturn(mockObservable2);
      td.when(mockScoreService.setScore(player, gameID, scorecard.categories[1], dice))
        .thenReturn(mockObservable1);
      const errorCaptor = td.matchers.captor();
      td.when(mockObservable2.subscribe(td.matchers.anything(), errorCaptor.capture())).thenReturn();

      rows[2].click();

      const errorMessage = 'NOP, it not werk';
      errorCaptor.value(new Error(errorMessage));

      td.verify(mockToastrService.error("Server could not be reached.", "ERROR", { positionClass: "toast-center-center" }));
      expect().nothing();
    });

    it('should NOT call the score service if the player has not rolled yet this turn', () => {
      component.player.rollsRemaining = 3;

      component.playerUpdated.subscribe((data: Player) => {
        fail('player should not have been updated')
      });

      rows[2].click();

      td.verify(mockScoreService.setScore(td.matchers.anything(), td.matchers.anything(), td.matchers.anything(), td.matchers.anything()), { times: 0 });
      expect().nothing();
    });
  });

  describe('hover over row', () => {
    const existingScores = [0, 42];
    const nonExistingScores = [null, undefined]

    existingScores.forEach((score) => {
      it(`should NOT highlight row and add red 0 when score exists - score ${score}`, () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  add: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: `${score}`,
                classList: {
                  add: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const existingCategory = new Category("Shives");
        existingCategory.score = score;

        const potentialScores = new Scorecard();
        potentialScores.categories = [new Category("Shives")];
        component.potentialScorecard = potentialScores;

        component.setHoveredScore(event, existingCategory);

        expect(event.target.children[1].textContent).toEqual(`${score}`);
        expect(_classListDescriptionColumn.length).toEqual(0);
        expect(_classListScoreColumn.length).toEqual(0);
      });

      it(`should NOT highlight row and display potential score when score exists - score ${score}`, () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  add: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: `${score}`,
                classList: {
                  add: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const potentialCategory = new Category("Shives", score);
        potentialCategory.score = 42;

        const potentialScores = new Scorecard();
        potentialScores.categories = [potentialCategory];
        component.potentialScorecard = potentialScores;

        component.setHoveredScore(event, new Category("Shives", score));

        expect(event.target.children[1].textContent).toEqual(`${score}`);
        expect(_classListDescriptionColumn.length).toEqual(0);
        expect(_classListScoreColumn.length).toEqual(0);
      });
    });

    nonExistingScores.forEach((score) => {
      it(`should highlight row and add red 0 - score ${score}`, () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  add: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: null,
                classList: {
                  add: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const exisitingCategory = new Category("Shives");
        exisitingCategory.score = score;

        const potentialCategory = new Category("Shives");
        potentialCategory.score = null;

        const potentialScores = new Scorecard();
        potentialScores.categories = [potentialCategory];
        component.potentialScorecard = potentialScores;

        component.setHoveredScore(event, exisitingCategory);

        expect(_classListDescriptionColumn[0]).toEqual("bold");

        expect(event.target.children[1].textContent).toEqual("0");
        expect(_classListScoreColumn[0]).toEqual("bold");
        expect(_classListScoreColumn[1]).toEqual("red");
      });

      it('should highlight row and display potential score when one exists', () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  add: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: "42",
                classList: {
                  add: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const potentialCategory = new Category("Shives", score);
        potentialCategory.score = 42;

        const potentialScores = new Scorecard();
        potentialScores.categories = [potentialCategory];
        component.potentialScorecard = potentialScores;

        component.setHoveredScore(event, new Category("Shives"));

        expect(event.target.children[1].textContent).toEqual("42");
        expect(_classListDescriptionColumn[0]).toEqual("bold");
        expect(_classListScoreColumn[0]).toEqual("bold");
        expect(_classListScoreColumn[1]).toEqual("potentialScore");
      });
    });

    it('should NOT highlight row and add red 0 when player has not rolled yet this turn', () => {
      const _classListDescriptionColumn = []
      const _classListScoreColumn = []

      const event = {
        target: {
          children: [
            {
              textContent: null,
              classList: {
                add: (_class: string) => {
                  _classListDescriptionColumn.push(_class);
                }
              }
            },
            {
              textContent: null,
              classList: {
                add: (_class: string) => {
                  _classListScoreColumn.push(_class);
                }
              }
            }
          ]
        }
      };

      const category = new Category("Shives");
      category.score = null;

      const potentialScores = new Scorecard();
      potentialScores.categories = [category];
      component.potentialScorecard = potentialScores;

      component.player.rollsRemaining = 3;

      component.setHoveredScore(event, new Category("Shives"));

      expect(event.target.children[1].textContent).toBeNull();
      expect(_classListDescriptionColumn.length).toEqual(0);
      expect(_classListScoreColumn.length).toEqual(0);
    });
  });

  describe('unhover from row', () => {
    const existingScores = [0, 42];
    const nonExistingScores = [null, undefined]

    existingScores.forEach((score) => {
      it(`should NOT unhighlight row and remove red 0 when score is present - score ${score}`, () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  remove: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: `${score}`,
                classList: {
                  remove: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const category = new Category("Shives");
        category.score = score;

        const potentialScores = new Scorecard();
        potentialScores.categories = [];
        component.potentialScorecard = potentialScores;

        component.resetHoveredScore(event, category);

        expect(event.target.children[1].textContent).toEqual(`${score}`);
        expect(_classListDescriptionColumn.length).toEqual(0);
        expect(_classListScoreColumn.length).toEqual(0);
      });

      it(`should NOT unhighlight row and remove potential score when score is present - score ${score}`, () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  remove: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: `${score}`,
                classList: {
                  remove: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const category = new Category("Shives");
        category.score = score;

        const potentialScores = new Scorecard();
        potentialScores.categories = [category];
        component.potentialScorecard = potentialScores;

        component.resetHoveredScore(event, category);

        expect(event.target.children[1].textContent).toEqual(`${score}`);
        expect(_classListDescriptionColumn.length).toEqual(0);
        expect(_classListScoreColumn.length).toEqual(0);
      });
    });
    nonExistingScores.forEach((score) => {
      it(`should unhighlight row and remove red 0 - score ${score}`, () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  remove: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: null,
                classList: {
                  remove: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const category = new Category("Shives", score);

        const potentialScores = new Scorecard();
        potentialScores.categories = [category];
        component.potentialScorecard = potentialScores;

        component.resetHoveredScore(event, category);

        expect(_classListDescriptionColumn[0]).toEqual("bold");

        expect(event.target.children[1].textContent).toEqual("");
        expect(_classListScoreColumn[0]).toEqual("bold");
        expect(_classListScoreColumn[1]).toEqual("red");
        expect(_classListScoreColumn[2]).toEqual("potentialScore");
      });

      it('should unhighlight row when potential score is present', () => {

        const _classListDescriptionColumn = []
        const _classListScoreColumn = []

        const event = {
          target: {
            children: [
              {
                textContent: null,
                classList: {
                  remove: (_class: string) => {
                    _classListDescriptionColumn.push(_class);
                  }
                }
              },
              {
                textContent: null,
                classList: {
                  remove: (_class: string) => {
                    _classListScoreColumn.push(_class);
                  }
                }
              }
            ]
          }
        };

        const potentialCategory = new Category("Shives");
        potentialCategory.score = 33;

        const potentialScores = new Scorecard();
        potentialScores.categories = [potentialCategory];
        component.potentialScorecard = potentialScores;

        component.resetHoveredScore(event, new Category("Shives", score));

        expect(event.target.children[1].textContent).toEqual("");
        expect(_classListDescriptionColumn[0]).toEqual("bold");
        expect(_classListScoreColumn[0]).toEqual("bold");
        expect(_classListScoreColumn[1]).toEqual("red");
        expect(_classListScoreColumn[2]).toEqual("potentialScore");
      });
    });

    it('should NOT unhighlight row and remove red 0 when player has not rolled yet this turn', () => {
      const _classListDescriptionColumn = []
      const _classListScoreColumn = []

      const event = {
        target: {
          children: [
            {
              textContent: null,
              classList: {
                remove: (_class: string) => {
                  _classListDescriptionColumn.push(_class);
                }
              }
            },
            {
              textContent: null,
              classList: {
                remove: (_class: string) => {
                  _classListScoreColumn.push(_class);
                }
              }
            }
          ]
        }
      };

      const category = new Category("Shives");
      category.score = null;

      const potentialScores = new Scorecard();
      potentialScores.categories = [category];
      component.potentialScorecard = potentialScores;

      component.player.rollsRemaining = 3;

      component.resetHoveredScore(event, new Category("Shives"));

      expect(event.target.children[1].textContent).toBeNull();
      expect(_classListDescriptionColumn.length).toEqual(0);
      expect(_classListScoreColumn.length).toEqual(0);
    });
  });

  function checkRowForActualScore(row: any, indexInScorecard: number) {
    const columns = row.querySelectorAll('td');

    expect(columns.length).toBe(3);
    expect(columns[0].innerText).toEqual(scorecard.categories[indexInScorecard].description);

    const score = scorecard.categories[indexInScorecard].score;
    let expectedScore = '';

    if (score !== null) expectedScore = score.toString();

    expect(columns[1].innerText).toEqual(expectedScore);
    expect(columns[2].innerText).toEqual(opponent.scorecard.categories[indexInScorecard].scoreAsString);
  }
});


