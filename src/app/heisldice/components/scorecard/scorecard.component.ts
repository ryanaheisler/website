import { Category } from './../../models/category.model';
import { GameObjectFromServer } from './../../models/game.model';
import { ToastrService } from 'ngx-toastr';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ScorecardFactory } from '../../models/scorecard-factory';
import { Scorecard } from '../../models/scorecard.model';
import { PlayerService } from '../../services/player/player.service';
import { RxjsTakeWrapper } from '../../services/rxjs-take/rxjs-take.wrapper';
import { ScoreService } from '../../services/score/score.service';
import { Player } from './../../models/player.model';
import { SettingsStorageService } from '../../services/settings-storage/settings-storage.service';

@Component({
  selector: 'scorecard',
  templateUrl: './scorecard.component.html',
  styleUrls: ['./scorecard.component.css']
})
export class ScorecardComponent implements OnInit {

  @Input()
  scorecard: Scorecard;
  @Input()
  potentialScorecard: Scorecard;
  @Input()
  player: Player;
  @Input()
  public set game(value: GameObjectFromServer) {
    this.opponents = []
    value.players.map((player: Player) => {
      if (player.name !== this.player.name) {
        player.scorecard = new ScorecardFactory().build(player.scorecard);
        this.opponents.push(player);
      }
    });
    this._game = value;
  }
  public get game() {
    return this._game;
  }
  private _game: GameObjectFromServer;
  public opponents: Player[]

  @Input()
  public set currentDice(value: number[]) {
    this.unsetDangerOnAllCategories();
    this._currentDice = value;
  }
  public get currentDice() {
    return this._currentDice
  }
  private _currentDice: number[];

  @Output()
  playerUpdated = new EventEmitter<Player>();

  constructor(
    private playerService: PlayerService,
    private scoreService: ScoreService,
    private takeWrapper: RxjsTakeWrapper,
    private toaster: ToastrService,
    private settingsStorageService: SettingsStorageService
  ) {

    this.scorecard = new Scorecard();
  }

  ngOnInit() {
    const existingPlayerName = this.settingsStorageService.getCurrentPlayerName();
    if (existingPlayerName) {
      this.player.name = existingPlayerName;
      this.postNewPlayer(existingPlayerName);
    }
  }

  getScorecard(): Scorecard {
    return this.scorecard;
  }

  getPotentialScore(category: Category): string {
    if (this.player.rollsRemaining !== 3) {
      const categories = this.potentialScorecard.categories;
      const matchingCategory = categories.find((potentialCategory) => {
        return potentialCategory.description === category.description;
      })
      if (matchingCategory.scoreIsNull() || matchingCategory.score === 0) {
        return ''
      } else {
        return matchingCategory.scoreAsString;
      }
    } else {
      return '';
    }
  }

  setScore(category: Category) {
    let scoreCanBeSet = (category.score === null || category.score === undefined);
    scoreCanBeSet = scoreCanBeSet && this.player.rollsRemaining !== 3

    const potentialScore = this.getPotentialScore(category);
    if (scoreCanBeSet && potentialScore === '' && !category.inDangerZeroScore) {
      scoreCanBeSet = false;
      this.unsetDangerOnAllCategories();
      category.inDangerZeroScore = true;
    }
    if (scoreCanBeSet) {
      this.scoreService.setScore(this.player, this.game.gameID, category, this.currentDice).pipe(this.takeWrapper.take(1))
        .subscribe(
          (returnedPlayer: Player) => {
            this.scorecard = new ScorecardFactory().build(returnedPlayer.scorecard);

            if (returnedPlayer.turnsRemaining === 0) {
              this.settingsStorageService.unsetCurrentGameID();
              this.settingsStorageService.unsetCurrentPlayerName();
            }

            this.playerUpdated.emit(returnedPlayer);
          },
          (error: Error) => {
            this.toaster.error("Server could not be reached.", "ERROR", { positionClass: "toast-center-center" });
          });
    }
  }

  public getOpponentScore(opponent: Player, category: Category): string {
    const matchingCategory = opponent.scorecard.categories.find((cat) => {
      return cat.description === category.description;
    });

    return matchingCategory.scoreAsString;
  }

  private unsetDangerOnAllCategories() {
    this.scorecard.categories.forEach((category) => {
      category.inDangerZeroScore = false;
    })
  }

  postNewPlayer(name) {
    this.playerService.put(name, this.game.gameID).subscribe((data: Player) => {
      if (data !== null) {
        this.scorecard = new ScorecardFactory().build(data.scorecard);
        this.playerUpdated.emit(data);
      } else {
        this.toaster.error("That name cannot be used. Please choose another.", "INVALID NAME", { positionClass: "toast-center-center" });
      }
    });
  }

  limitToAlphaCharacters(event) {
    event.target.value = event.target.value.replace(/[^A-Za-z" "]/g, '');
  }

  shouldButtonBeDisabled(nameText): boolean {
    return nameText !== '';
  }

  public noop(event) {

  }

  public setHoveredScore(event, category: Category) {
    const categoryHasNoScore = (category.score === null || category.score === undefined);
    if (this.player.rollsRemaining != 3 && categoryHasNoScore) {
      const target: HTMLTableRowElement = event.target

      target.children[0].classList.add("bold");
      target.children[1].classList.add("bold");

      const potentialScore = this.getPotentialScore(category);
      if (potentialScore) {
        target.children[1].classList.add("potentialScore");
      } else {
        target.children[1].classList.add("red");
      }
      target.children[1].textContent = potentialScore || "0";
    }
  };

  public resetHoveredScore(event, category: Category) {
    const categoryHasNoScore = (category.score === null || category.score === undefined);
    if (this.player.rollsRemaining != 3 && categoryHasNoScore) {
      const target: HTMLTableRowElement = event.target

      target.children[0].classList.remove("bold");
      target.children[1].classList.remove("bold");

      if (!category.scoreAsString) {
        target.children[1].textContent = "";
        target.children[1].classList.remove("red");
        target.children[1].classList.remove("potentialScore");
      }
    }
  };

  public get shouldShowTotals() {
    return this.player.name;
  }
}
