import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { map } from 'rxjs/operators';
import { ScorecardFactory } from './../../models/scorecard-factory';
import { ScorecardService } from './../../services/scorecard/scorecard.service';

@Injectable({
  providedIn: 'root'
})
export class ScorecardResolver implements Resolve<any> {
  constructor(private scorecardService: ScorecardService, private scorecardFactory: ScorecardFactory) { }

  resolve(route: ActivatedRouteSnapshot) {

    return this.scorecardService.getBlankScorecard().pipe(map((json) => {
      return this.scorecardFactory.build(json);
    }));
  }
}