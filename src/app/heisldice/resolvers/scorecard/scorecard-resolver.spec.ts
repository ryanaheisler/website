import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import * as td from 'testdouble';
import { ScorecardFactory } from '../../models/scorecard-factory';
import { Scorecard } from '../../models/scorecard.model';
import { ScorecardService } from '../../services/scorecard/scorecard.service';
import { ScorecardResolver } from './scorecard-resolver';


describe('ScorecardResolver', () => {

  let expectedScorecard;
  beforeEach(() => {

    const mockScorecardService = td.object(ScorecardService.prototype);
    const scorecardJson = { scorecard: { 'blee': 'bo' } };
    td.when(mockScorecardService.getBlankScorecard()).thenReturn(of(scorecardJson));

    const mockScorecardFactory = td.object(ScorecardFactory.prototype);
    expectedScorecard = new Scorecard();
    td.when(mockScorecardFactory.build(scorecardJson)).thenReturn(expectedScorecard)

    TestBed.configureTestingModule({
      providers: [
        { provide: ScorecardService, useValue: mockScorecardService },
        { provide: ScorecardFactory, useValue: mockScorecardFactory }
      ]
    });
  });

  it('should call the ScorecardService to get a blank scorecard', () => {
    const resolver = TestBed.get(ScorecardResolver);

    const resolvedValue = resolver.resolve({});

    resolvedValue.subscribe((value) => {
      expect(value).toBe(expectedScorecard);
    })
  });
});