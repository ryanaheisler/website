export const environment = {
  production: true,
  backendHost: 'server.ryanheisler.com',
  backendPort: 80,
  debug: false
};
